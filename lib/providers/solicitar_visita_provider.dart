import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';

class SolicitarVisitaProvider {
  final prefs = PreferenciasUsuario();
  Future solicitar() async {
    Uri _url =
        Uri.parse('http://157.245.82.111/api/solicitarvisita');
    final data = {
      'id': prefs.id,
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    print(result);
  }

  Future validacionVisita() async {
    Uri _url =
        Uri.parse('http://157.245.82.111/api/validacionvisita');
    final data = {
      'id': prefs.id,
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    print(result);
    return result;
  }

  Future visitaCompletada() async {
    Uri _url =
        Uri.parse('http://10.0.2.2/Laravel/Yego/public/api/visitacompletada');
    final data = {
      'id': prefs.id,
      //'id_visita': ,
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    //print(result);
    return result;
  }
}
