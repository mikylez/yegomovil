import 'dart:convert';
//import 'dart:html';
import 'package:http/http.dart' as http;
import 'package:yegoapp/carrito_preferences/carrito_preferences.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';
//import 'package:shared_preferences/shared_preferences.dart';

class UsuarioProvider {

  final _pref = new PreferenciasUsuario();
  final _prefcar = new CarritoPreferences();

  Future<Map<String, dynamic>> login( String email, String password) async {

    Uri _url = Uri.parse('http://157.245.82.111/api/auth/login');
    //Uri _url = Uri.parse('http://127.0.0.1:80/Yego/public/api/auth/login');//para pruebas me da ladilla 
                                                                                  //estar actualizando el server a cada rato
                                                                                  //puto negro
    //final _url = "http://localhost/netmuse/netsell2/public/api/log";
    //
    //print(email);
    //print(password);

    final authData = {
      'email' : email,
      'password' : password,
      //'returnSrecureToken' : true,
    };

    final resp = await http.post( _url,
    headers: {
      'Content-Type': 'application/json',
      'X-Requested-With':'XMLHttpRequest',
    },
    body: json.encode( authData ) );

    //print(resp.body);

    Map<String, dynamic> decodeResp = json.decode (resp.body);

    //print(decodeResp);

    if(decodeResp.containsKey('access_token')){
      _prefcar.eliminarPrefs();
      _pref.token = decodeResp['access_token'];
      _pref.id = decodeResp['id_usuario'];
      _pref.idTipo=decodeResp['id_tipo'];
      return { 'ok': true, 'token':decodeResp['access_token'], 'id_tipo':decodeResp['id_tipo']};

    }else{

      return { 'ok': false, 'mensaje':decodeResp['message']};
    
    }

  } 

  Future logout() async {
      _pref.token = '';
      _pref.id = 0;
      _pref.idTipo= 0; 
    /*Uri _url = Uri.parse('http://157.245.82.111/api/auth/logout');
    final resp = await http.get( _url,
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With':'XMLHttpRequest',
      });
    Map<String, dynamic> decodeResp = json.decode (resp.body);
    print(decodeResp);*/
    return;
  }
}