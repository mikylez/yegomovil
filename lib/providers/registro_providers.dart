import 'dart:convert';

import 'package:http/http.dart' as http;

class Registro {
  Future registro(_imputUsuario, _imputEmail, _imputPassword,
      _imputConfirmarPassword) async {
    // Uri _url = Uri.parse('http://10.0.2.2/yego/public/api/registro_app');
    Uri _url = Uri.parse('http://157.245.82.111/api/registro_app');
    final data = {
      'usuario': _imputUsuario,
      'email': _imputEmail,
      'password': _imputPassword,
      'confirmarPassword': _imputConfirmarPassword,
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = (response.body);
    print(result);
    return result;
  }
}
