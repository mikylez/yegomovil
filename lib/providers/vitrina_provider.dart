import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:yegoapp/carrito_preferences/carrito_preferences.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';

class VitrinaProvider {
  final prefs = PreferenciasUsuario();
  final cart = CarritoPreferences();

  Future productos(page) async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/inventory');
    //Uri _url = Uri.parse('http://159.65.221.75/api/inventory');//pruebas
    Uri _url = Uri.parse('http://157.245.82.111/api/inventory');
    final data = {'page': page, 'id': prefs.id, 'id_tipo': prefs.idTipo};
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    return result;
  }

  Future ofertas(page) async {
    //Uri _url = Uri.parse('http://159.65.221.75/api/ofertas');//pruebas
    //Uri _url = Uri.parse('http://10.0.2.2/yego/public/api/ofertas');
    Uri _url = Uri.parse('http://157.245.82.111/api/ofertas');
    final data = {
      'page': page,
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    return result;
  }

  Future buscar(String querry) async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/searchbar');
    Uri _url = Uri.parse('http://157.245.82.111/api/searchbar');
    //Uri _url = Uri.parse('http://159.65.221.75/api/searchbar');//prueba

    final data = {'id': prefs.id, 'id_tipo': prefs.idTipo, 'querry': querry};

    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    return result["Inventories"];
  }

  Future cuenta(
      _inputnombre,
      _inputapellido,
      _inputdocumento,
      _inputcorreo,
      _inputtelefono,
      _inputfecha,
      _inputdireccionfiscal,
      _inputdirecciondespacho,
      _radioValue,
      _opcionSelecionadaNacionalidad,
      _radioValueContribuyente,
      _estadoFiscal,
      _municipiosFiscal,
      _parroquiasFiscal,
      _estadoDespacho,
      _municipiosDespacho,
      _parroquiasDespacho) async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/cuenta');
    Uri _url = Uri.parse('http://157.245.82.111/api/cuenta');
    final data = {
      'id_usuario': prefs.id,
      'nombre': _inputnombre,
      'apellido': _inputapellido,
      'nacionalidad': _opcionSelecionadaNacionalidad,
      'documento': _inputdocumento,
      'correo': _inputcorreo,
      'telefono': _inputtelefono,
      'fecha': _inputfecha,
      'direccion_fiscal': _inputdireccionfiscal,
      'direccion_despacho': _inputdirecciondespacho,
      'sexo': _radioValue,
      'contribuyente': _radioValueContribuyente,
      'estado_fiscal': _estadoFiscal,
      'municipio_fiscal': _municipiosFiscal,
      'parroquia_fiscal': _parroquiasFiscal,
      'estado_despacho': _estadoDespacho,
      'municipio_despacho': _municipiosDespacho,
      'parroquia_despacho': _parroquiasDespacho,
    };

    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    print(result);
  }

  Future informacionUsuarioVender(_inputBuscar) async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/informacionUsuarioVender');
    //Uri _url = Uri.parse('http://159.65.221.75/api/informacionUsuarioVender'); //pruebas
    Uri _url = Uri.parse(
        'http://157.245.82.111/api/informacionUsuarioVender'); //oficial

    final data = {
      'documento': _inputBuscar,
      'id_vendedor': prefs.id,
    };

    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    return result;
  }

  Future direccion() async {
    // _url = Uri.parse('http://10.0.2.2/Yego/public/api/estados');
    Uri _url = Uri.parse('http://157.245.82.111/api/estados');
    final response = await http.get(
      _url,
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      },
    );
    var result = jsonDecode(response.body);

    List<String> _estadosLista = [];
    //List<String> _municipioLista = [];
    //List<String> _parroquiaLista = [];

    var lengthEstados = result["estados"].length;
    //var lengthMunicipios = result["municipio"].length;
    //var lengthParroquia = result["parroquia"].length;

    for (var i = 0; i < lengthEstados; i++) {
      _estadosLista.add(result["estados"][i]["nombre"]);
    }

    // for (var i = 0; i < lengthMunicipios; i++) {
    //   _municipioLista.add(result["municipio"][i]["nombre"]);
    // }

    // for (var i = 0; i < lengthParroquia; i++) {
    //   _parroquiaLista.add(result["parroquia"][i]["nombre"]);
    // }
    return [_estadosLista, /*_municipioLista, _parroquiaLista*/];
  }

  Future municipios(_estadoFiscal)async{
    //Uri _url = Uri.parse('http://10.0.2.2/yego/public/api/municipios');
    Uri _url = Uri.parse('http://157.245.82.111/api/municipios');
    final data = {
      'estado':_estadoFiscal,
    };
    final response = await http.post(_url,
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      },
    body: json.encode(data));
    var result = jsonDecode(response.body);
    //print(result);
    return result;
  }

  Future parroquia(_municipiosFiscal)async{
    //Uri _url = Uri.parse('http://10.0.2.2/yego/public/api/parroquia');
    Uri _url = Uri.parse('http://157.245.82.111/api/parroquia');
    final data = {
      'municipio':_municipiosFiscal['nombre'],
    };
    final response = await http.post(_url,
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      },
    body: json.encode(data));
    var result = jsonDecode(response.body);
    //print(result);
    return result;
  }

  Future info() async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/infoUusario');
    Uri _url = Uri.parse('http://157.245.82.111/api/infoUusario');

    final data = {'id_usuario': prefs.id};

    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    return result;
  }

  Future<int> getCarritoVitrina() async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/carritoVitrina');
    Uri _url = Uri.parse('http://157.245.82.111/api/carritoVitrina');

    final data = {'id_usuario': prefs.id};

    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    return result['cart'];
  }

  Future getCarritoComprar() async {
    //Uri _url = Uri.parse('http://10.0.2.2/yego/public/api/getCarritoComprar');
    Uri _url = Uri.parse('http://157.245.82.111/api/getCarritoComprar');
    // Uri _url = Uri.parse('http://159.65.221.75/api/getCarritoComprar');

    final data = {'id_usuario': prefs.id};

    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    //print(result);
    return result;
  }

  Future getCarritoVender() async {
    //Uri _url = Uri.parse('http://10.0.2.2/yego1/public/api/getcarritovender');
    Uri _url = Uri.parse('http://157.245.82.111/api/getcarritovender');

    final data = {'id_usuario': prefs.id};

    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    return result;
  }

  Future postProductoDetalle(args, _numeroProductos, unidad) async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/agregarcarrito');
    Uri _url = Uri.parse('http://157.245.82.111/api/agregarcarrito');

    final data = {
      'id_usuario': prefs.id,
      'producto': args,
      'quantity': _numeroProductos.toString(),
      'unidad': unidad.toString(),
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    print(result);
    //return result;
  }

  Future postProductoDetalleVender(args, _numeroProductos, unidad) async {
    //cart.removetPrefs();//por si se muere el cache
    await cart.initPrefs();
    var a = [];
    var dataSumada;
    if (prefs.idTipo == 9) {
      final data = {
        'id': args['id'],
        'quantity': _numeroProductos,
        'name': args['name'],
        'codigo_producto': args['codigo_producto'],
        'descripcion': args['descripcion'],
        'price_unidad': args['price_unidad_credito'],
        'price_unidad_credito': args['price_unidad_credito'],
        'detal': args['detal'],
        'price_caja': args['price_caja'],
        //'cuarto':args['cuartocaja'],
        'numero': args['cantidad'],
        'photo': args['photo'],
        'unidad': unidad, //cambiar dependiendo de lo q sea
        'id_empresa': args['id_empresas'],
        'codigo_barra': args['codigo_de_barra']
      };
      if (cart.carrito == 0) {
        a.add(data);
        cart.carrito = jsonEncode(a);
      } else {
        var length = jsonDecode(cart.carrito).length;
        var b = jsonDecode(cart.carrito);
        for (var i = 0; i < length; i++) {
          if (b[i]['id'] == args['id']) {
            dataSumada = {
              'id': b[i]['id'],
              'quantity': b[i]['quantity'] + _numeroProductos,
              'name': b[i]['name'],
              'codigo_producto': b[i]['codigo_producto'],
              'descripcion': b[i]['descripcion'],
              'price_unidad': b[i]['price_unidad_credito'],
              'price_unidad_credito': b[i]['price_unidad_credito'],
              'photo': b[i]['photo'],
              'detal': b[i]['detal'],
              'price_caja': b[i]['price_caja'],
              //'cuarto':b[i]['cuarto'],
              'numero': b[i]['numero'],
              'unidad': b[i]['unidad'], //cambiar dependiendo de lo q sea
              'id_empresa': b[i]['id_empresa'],
              'codigo_barra': b[i]['codigo_barra']
            };
            a.add(dataSumada);
          } else {
            a.add(b[i]);
          }
        }
        await cart.removetPrefs();
        if (dataSumada == null) {
          jsonEncode(data);
          a.add(data);
          cart.carrito = jsonEncode(a);
        } else {
          jsonEncode(a);
          cart.carrito = jsonEncode(a);
        }
      }
    } else {
      final data = {
        'id': args['id'],
        'quantity': _numeroProductos,
        'name': args['name'],
        'price_unidad': args['price_unidad'],
        'price_unidad_credito': args['price_unidad_credito'],
        'codigo_producto': args['codigo_producto'],
        'descripcion': args['descripcion'],
        'detal': args['detal'],
        'price_caja': args['price_caja'],
        //'cuarto':args['cuartocaja'],
        'numero': args['cantidad'],
        'photo': args['photo'],
        'unidad': unidad, //cambiar dependiendo de lo q sea
        'id_empresa': args['id_empresas'],
        'codigo_barra': args['codigo_de_barra']
      };
      if (cart.carrito == 0) {
        a.add(data);
        cart.carrito = jsonEncode(a);
      } else {
        var length = jsonDecode(cart.carrito).length;
        var b = jsonDecode(cart.carrito);
        for (var i = 0; i < length; i++) {
          if (b[i]['id'] == args['id']) {
            dataSumada = {
              'id': b[i]['id'],
              'quantity': b[i]['quantity'] + _numeroProductos,
              'name': b[i]['name'],
              'price_unidad': b[i]['price_unidad'],
              'price_unidad_credito': b[i]['price_unidad_credito'],
              'codigo_producto': b[i]['codigo_producto'],
              'descripcion': b[i]['descripcion'],
              'photo': b[i]['photo'],
              'detal': b[i]['detal'],
              'price_caja': b[i]['price_caja'],
              //'cuarto':b[i]['cuarto'],
              'numero': b[i]['numero'],
              'unidad': b[i]['unidad'], //cambiar dependiendo de lo q sea
              'id_empresa': b[i]['id_empresa'],
              'codigo_barra': b[i]['codigo_barra']
            };
            a.add(dataSumada);
          } else {
            a.add(b[i]);
          }
        }
        await cart.removetPrefs();
        if (dataSumada == null) {
          jsonEncode(data);
          a.add(data);
          cart.carrito = jsonEncode(a);
        } else {
          jsonEncode(a);
          cart.carrito = jsonEncode(a);
        }
      }
    }
  }

  Future elimarProducto(id) async {
    //await cart.initPrefs();
    var b = jsonDecode(cart.carrito);
    var length = jsonDecode(cart.carrito).length;
    var data;
    for (var i = 0; i < length; i++) {
      if (b[i]['id'] != id) {
        data = [
          {
            'id': b[i]['id'],
            'quantity': b[i]['quantity'],
            'name': b[i]['name'],
            'price_unidad': b[i]['price_unidad'],
            'price_unidad_credito': b[i]['price_unidad_credito'],
            'photo': b[i]['photo'],
            'detal': b[i]['detal'],
            'codigo_producto': b[i]['codigo_producto'],
            'descripcion': b[i]['descripcion'],
            'price_caja': b[i]['price_caja'],
            'cuarto': b[i]['cuarto'],
            'numero': b[i]['numero'],
            'unidad': b[i]['unidad'], //cambiar dependiendo de lo q sea
            'id_empresa': b[i]['id_empresa'],
            'codigo_barra': b[i]['codigo_barra']
          }
        ];
      }
    }
    await cart.removetPrefs();
    if (cart.carrito == 0) {
      cart.carrito = jsonEncode(data);
    }
  }

  Future eliminarcomprar(id) async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/eliminarcomprar');
    Uri _url = Uri.parse('http://157.245.82.111/api/eliminarcomprar');

    final data = {'id_producto': id, 'id_usuario': prefs.id};

    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));

    var result = jsonDecode(response.body);
    print(result);
  }

  Future restarProducto(id) async {
    var b = jsonDecode(cart.carrito);
    var length = jsonDecode(cart.carrito).length;
    var data;
    var dataFinal = [];
    for (var i = 0; i < length; i++) {
      if (b[i]['id'] == id) {
        if (b[i]['unidad'] == 1) {
          if (b[i]['quantity'] == 1) {
            return 1;
          }
          data = {
            'id': b[i]['id'],
            'quantity': b[i]['quantity'] - 1,
            'name': b[i]['name'],
            'price_unidad': b[i]['price_unidad'],
            'price_unidad_credito': b[i]['price_unidad_credito'],
            'photo': b[i]['photo'],
            'detal': b[i]['detal'],
            'codigo_producto': b[i]['codigo_producto'],
            'descripcion': b[i]['descripcion'],
            'price_caja': b[i]['price_caja'],
            'cuarto': b[i]['cuarto'],
            'numero': b[i]['numero'],
            'unidad': b[i]['unidad'], //cambiar dependiendo de lo q sea
            'id_empresa': b[i]['id_empresa'],
            'codigo_barra': b[i]['codigo_barra']
          };

          if (dataFinal == null) {
            dataFinal = data;
          } else {
            dataFinal.add(data);
          }
        } else {
          if (b[i]['quantity'] - int.parse(b[i]['cuarto']) == 0) {
            return 1;
          }
          data = {
            'id': b[i]['id'],
            'quantity': b[i]['quantity'] - int.parse(b[i]['cuarto']),
            'name': b[i]['name'],
            'price_unidad': b[i]['price_unidad'],
            'price_unidad_credito': b[i]['price_unidad_credito'],
            'photo': b[i]['photo'],
            'codigo_producto': b[i]['codigo_producto'],
            'descripcion': b[i]['descripcion'],
            'detal': b[i]['detal'],
            'price_caja': b[i]['price_caja'],
            'cuarto': b[i]['cuarto'],
            'numero': b[i]['numero'],
            'unidad': b[i]['unidad'], //cambiar dependiendo de lo q sea
            'id_empresa': b[i]['id_empresa'],
            'codigo_barra': b[i]['codigo_barra']
          };

          if (dataFinal == null) {
            dataFinal = data;
          } else {
            dataFinal.add(data);
          }
        }
      } else {
        data = {
          'id': b[i]['id'],
          'quantity': b[i]['quantity'],
          'name': b[i]['name'],
          'price_unidad': b[i]['price_unidad'],
          'price_unidad_credito': b[i]['price_unidad_credito'],
          'photo': b[i]['photo'],
          'detal': b[i]['detal'],
          'codigo_producto': b[i]['codigo_producto'],
          'descripcion': b[i]['descripcion'],
          'price_caja': b[i]['price_caja'],
          'cuarto': b[i]['cuarto'],
          'numero': b[i]['numero'],
          'unidad': b[i]['unidad'], //cambiar dependiendo de lo q sea
          'id_empresa': b[i]['id_empresa'],
          'codigo_barra': b[i]['codigo_barra']
        };
        if (dataFinal == null) {
          dataFinal = data;
        } else {
          dataFinal.add(data);
        }
      }
    }
    await cart.removetPrefs();
    if (cart.carrito == 0) {
      cart.carrito = jsonEncode(dataFinal);
    }
    return 2;
  }

  Future sumarProducto(id) async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/sumarvender');
    Uri _url = Uri.parse('http://157.245.82.111/api/sumarvender');
    //Uri _url = Uri.parse('http://159.65.221.75/api/sumarvender');//pruebas

    final info = {
      'id_producto': id,
    };

    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(info));

    var result = jsonDecode(response.body);
    var b = jsonDecode(cart.carrito);
    var length = jsonDecode(cart.carrito).length;
    var data;
    var dataFinal = [];
    for (var i = 0; i < length; i++) {
      if (b[i]['id'] == id) {
        if (b[i]['unidad'] == 1) {
          if (result < b[i]['quantity'] + 1) {
            return 1;
          }
          data = {
            'id': b[i]['id'],
            'quantity': b[i]['quantity'] + 1,
            'name': b[i]['name'],
            'price_unidad': b[i]['price_unidad'],
            'price_unidad_credito': b[i]['price_unidad_credito'],
            'photo': b[i]['photo'],
            'detal': b[i]['detal'],
            'codigo_producto': b[i]['codigo_producto'],
            'descripcion': b[i]['descripcion'],
            'price_caja': b[i]['price_caja'],
            'cuarto': b[i]['cuarto'],
            'numero': b[i]['numero'],
            'unidad': b[i]['unidad'], //cambiar dependiendo de lo q sea
            'id_empresa': b[i]['id_empresa'],
            'codigo_barra': b[i]['codigo_barra']
          };

          if (dataFinal == null) {
            dataFinal = data;
          } else {
            dataFinal.add(data);
          }
        } else {
          if (result < b[i]['quantity'] + int.parse(b[i]['cuarto'])) {
            return 1;
          }
          data = {
            'id': b[i]['id'],
            'quantity': b[i]['quantity'] + int.parse(b[i]['cuarto']),
            'name': b[i]['name'],
            'price_unidad': b[i]['price_unidad'],
            'price_unidad_credito': b[i]['price_unidad_credito'],
            'photo': b[i]['photo'],
            'detal': b[i]['detal'],
            'codigo_producto': b[i]['codigo_producto'],
            'descripcion': b[i]['descripcion'],
            'price_caja': b[i]['price_caja'],
            'cuarto': b[i]['cuarto'],
            'numero': b[i]['numero'],
            'unidad': b[i]['unidad'], //cambiar dependiendo de lo q sea
            'id_empresa': b[i]['id_empresa'],
            'codigo_barra': b[i]['codigo_barra']
          };

          if (dataFinal == null) {
            dataFinal = data;
          } else {
            dataFinal.add(data);
          }
        }
      } else {
        data = {
          'id': b[i]['id'],
          'quantity': b[i]['quantity'],
          'name': b[i]['name'],
          'price_unidad': b[i]['price_unidad'],
          'price_unidad_credito': b[i]['price_unidad_credito'],
          'photo': b[i]['photo'],
          'detal': b[i]['detal'],
          'codigo_producto': b[i]['codigo_producto'],
          'descripcion': b[i]['descripcion'],
          'price_caja': b[i]['price_caja'],
          'cuarto': b[i]['cuarto'],
          'numero': b[i]['numero'],
          'unidad': b[i]['unidad'], //cambiar dependiendo de lo q sea
          'id_empresa': b[i]['id_empresa'],
          'codigo_barra': b[i]['codigo_barra']
        };
        if (dataFinal == null) {
          dataFinal = data;
        } else {
          dataFinal.add(data);
        }
      }
    }
    await cart.removetPrefs();
    if (cart.carrito == 0) {
      cart.carrito = jsonEncode(dataFinal);
    }
    return 2;
  }

  Future pedidovender(
      args,
      _inputBuscar,
      _inputcorreo,
      _inputnombre,
      _inputtelefono,
      _opcionSelecionadaNacionalidad,
      _radioValueContribuyente,
      _estadoFiscal,
      _municipiosFiscal,
      _parroquiasFiscal,
      _estadoDespacho,
      _municipiosDespacho,
      _parroquiasDespacho,
      _inputdocumentocomercio,
      _inputnombrecomercio,
      _inputtelefonocomercio,
      _opcionSelecionadaNacionalidadcomercio,
      _negocioelegido,
      _inputdireccionfiscal,
      _inputdirecciondespacho,
      _inputdocumento,
      _imputCredito) async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/verificar_productos');
    //Uri _url = Uri.parse('http://159.65.221.75/api/verificar_productos');//pruebas
    Uri _url = Uri.parse('http://157.245.82.111/api/verificar_productos');

    final info = {
      'carrito': jsonDecode(cart.carrito),
    };

    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(info));

    var result = jsonDecode(response.body);
    result = 0;
    if (result != 0) {
      return 1;
    } else {
      // Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/pedidovender');
      //Uri _url = Uri.parse('http://159.65.221.75/api/pedidovender');//prueba
      Uri _url = Uri.parse('http://157.245.82.111/api/pedidovender');

      final data = {
        'flete': args,
        'correo': _inputcorreo,
        'nelegido': _negocioelegido,
        'dcomercio': _inputdocumentocomercio,
        'nombrecomercio': _inputnombrecomercio,
        'telefonocomercio': _inputtelefonocomercio,
        'nacionalidadcomercio': _opcionSelecionadaNacionalidadcomercio,
        'idtipo': prefs.idTipo,
        'id_usuario': prefs.id,
        'carrito': jsonDecode(cart.carrito),
        'documento': _inputBuscar,
        'documento_formulario': _inputdocumento,
        //'correo'    :_inputcorreo,
        'nombre': _inputnombre, //esta es la razon social selva y sus webonadas
        //'apellido'  :_inputapellido,
        'telefono': _inputtelefono,
        'credito': _imputCredito,
        'nacionalidad': _opcionSelecionadaNacionalidad,
        'contribuyente': _radioValueContribuyente,
        'estado_fiscal': _estadoFiscal,
        'municipio_fiscal': _municipiosFiscal,
        'parroquia_fiscal': _parroquiasFiscal,
        'estado_despacho': _estadoDespacho,
        'municipio_despacho': _municipiosDespacho,
        'parroquia_despacho': _parroquiasDespacho,
        'direccion_fiscal': _inputdireccionfiscal,
        'direccion_despacho': _inputdirecciondespacho,
      };
      // print(data);
      final response = await http.post(_url,
          headers: {
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
          },
          body: json.encode(data));
      var result = jsonDecode(response.body);
      if (result != null) {
        await cart.removetPrefs();
      }
      print(result);
      return result;
    }
  }

  Future registrarCliente(
    _inputBuscar,
    _inputcorreo,
    _inputnombre,
    _inputapellido,
    _inputtelefono,
    _opcionSelecionadaNacionalidad,
    _radioValueContribuyente,
    _estadoFiscal,
    _municipiosFiscal,
    _parroquiasFiscal,
    _estadoDespacho,
    _municipiosDespacho,
    _parroquiasDespacho,
    _inputdireccionfiscal,
    _inputdirecciondespacho,
    _inputdocumento,
    _imputCredito,
    _inputdocumentocomercio,
    _inputnombrecomercio,
    _inputtelefonocomercio,
    _opcionSelecionadaNacionalidadcomercio,
    _negocioelegido,
  ) async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/verificar_productos');
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/pedidovender');
    Uri _url = Uri.parse('http://157.245.82.111/api/crearcliente');

    final data = {
      'nelegido': _negocioelegido,
      'dcomercio': _inputdocumentocomercio,
      'nombrecomercio': _inputnombrecomercio,
      'telefonocomercio': _inputtelefonocomercio,
      'nacionalidadcomercio': _opcionSelecionadaNacionalidadcomercio,
      'idtipo': prefs.idTipo,
      'id_usuario': prefs.id,
      //'carrito': jsonDecode(cart.carrito),
      'documento': _inputBuscar,
      'documento_formulario': _inputdocumento,
      'correo': _inputcorreo,
      'nombre': _inputnombre,
      'apellido': _inputapellido,
      'telefono': _inputtelefono,
      'credito': _imputCredito,
      'nacionalidad': _opcionSelecionadaNacionalidad,
      'contribuyente': _radioValueContribuyente,
      'estado_fiscal': _estadoFiscal,
      'municipio_fiscal': _municipiosFiscal,
      'parroquia_fiscal': _parroquiasFiscal,
      'estado_despacho': _estadoDespacho,
      'municipio_despacho': _municipiosDespacho,
      'parroquia_despacho': _parroquiasDespacho,
      'direccion_fiscal': _inputdireccionfiscal,
      'direccion_despacho': _inputdirecciondespacho,
    };
    print(data);
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    if (result != null) {
      //await cart.removetPrefs();
    }
    print(result);
    return result;
  }

  Future pedidocomprar(flete) async {
    // Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/pedidocomprar');
     Uri _url = Uri.parse('http://157.245.82.111/api/pedidocomprar');

    final data = {
      'flete': flete,
      'id_usuario': prefs.id,
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    print(result);
    return result;
  }

  Future sumarComprar(id) async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/sumarcomprar');
    Uri _url = Uri.parse('http://157.245.82.111/api/sumarcomprar');

    final data = {
      'id_producto': id,
      'id_usuario': prefs.id,
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    print(result);
    return result;
  }

  Future restarComprar(id) async {
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/restarcomprar');
    Uri _url = Uri.parse('http://157.245.82.111/api/restarcomprar');

    final data = {
      'id_producto': id,
      'id_usuario': prefs.id,
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    //Future.delayed(const Duration(seconds: 5),(){});
    print(result);
    //return result;
  }
}
