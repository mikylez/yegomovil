import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:yegoapp/models/directions.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Directionrepository {

  static const String _baseUrl =
  'https://maps.googleapis.com/maps/api/directions/json?';

  final Dio _dio;

  Directionrepository({Dio dio}) : _dio = dio ?? Dio();

  Future<Directions> getDirections({
    @required LatLng origin,
    @required LatLng destination
  }) async {
    final response = await _dio.get(
      _baseUrl,
      queryParameters: {
        'origin' : '${origin.latitude}, ${origin.longitude}',
        'destination' :'${destination.latitude}, ${destination.longitude}',
        'key': 'AIzaSyDRvkaXveb2dnKxRHjryGwjQWULUgXXDig', 

      }
    );

    if (response.statusCode==200){
      return Directions.fromMap(response.data);
    }

    return null;
  }

  Future getDestination(id) async{
    Uri _url = Uri.parse('http://157.245.82.111/api/getDirecciones');
    final data ={
      'id':id 
    };
    final response = await http.post(_url,
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With':'XMLHttpRequest',
      },
      body: json.encode( data )
    );
    var result = jsonDecode(response.body);
    //print(result);
    return result;
  }

  Future negocios() async{
    //Uri _url = Uri.parse('http://10.0.2.2/Yego/public/api/estados'); 
    Uri _url = Uri.parse('http://157.245.82.111/api/negocios');
    final response = await http.get(_url,
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With':'XMLHttpRequest',
      },
    );
    var result = jsonDecode(response.body);
    
    List _negocios=[];
    print(result);
    var lengthNegocios = result["negocios"].length;

    for (var i = 0; i < lengthNegocios; i++) {
      _negocios.add(result["negocios"][i]["nombre"]);
    }
    //print(_negocios);
    return [_negocios];
  }
}