import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';

class EntregasProvider {
  
  final prefs = PreferenciasUsuario();
  //final cart = CarritoPreferences();
  
  Future despachos() async {
    //Uri _url = Uri.parse('http://10.0.2.2/yego1/public/api/inventory');
    Uri _url = Uri.parse('http://157.245.82.111/api/getDespachosYego');
    final data ={
      'id':prefs.id 
    };
    final response = await http.post(_url,
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With':'XMLHttpRequest',
      },
      body: json.encode( data )
    );
    var result = jsonDecode(response.body);
    print(result);
    return result;
  }

  Future direccion(args) async {
  Uri _url = Uri.parse('http://157.245.82.111/api/getDir');
  final data ={
      'id': args,
    };
    final response = await http.post(_url,
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With':'XMLHttpRequest',
      },
      body: json.encode( data )
    );
    var result = jsonDecode(response.body);
    print(result);
    return result;
  }

    Future verificarCod(args,_inputcodigo,id) async {
      print(_inputcodigo);
    //Uri _url = Uri.parse('http://10.0.2.2/yego1/public/api/inventory');
    Uri _url = Uri.parse('http://157.245.82.111/api/entregarPedido');
    final data ={
      'id': args,
      'cod': _inputcodigo,
      'idu': id,
    };
    final response = await http.post(_url,
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With':'XMLHttpRequest',
      },
      body: json.encode( data )
    );
    var result = jsonDecode(response.body);
    print(result);
    /*if(result['respuesta'] == false){
    return false;
    }else{
    return true;
    }*/
    return result;
  }
}