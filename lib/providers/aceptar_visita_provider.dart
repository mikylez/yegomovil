import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';

class AceptarVisitaProvider {
  final prefs = PreferenciasUsuario();
  Future visitaporaceptar() async {
    Uri _url =
        Uri.parse('http://157.245.82.111/api/visitaporaceptar');
    final data = {
      'id': prefs.id,
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    //print(result);
    return result;
  }

  Future verDireccionVisita(id) async {
    Uri _url =
        Uri.parse('http://157.245.82.111/api/verdireccionvisita');
    final data = {
      'id': id,
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    print(result);
    return result;
  }

  Future aceptarvisita(idv) async {
    Uri _url =
        Uri.parse('http://157.245.82.111/api/aceptarvisita');
    final data = {
      'id_vendedor': prefs.id,
      'id_visita': idv,
      ///'id_visita': id_visita
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    //print(result);
    return result;
  }

  Future visitaEnProceso(id) async {
    Uri _url =
        Uri.parse('http://157.245.82.111/api/visitaenproceso');
    final data = {
      'id_vendedor': prefs.id,
      'id_visita': id,
      ///'id_visita': id_visita
    };
    //print(data);
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    //print(result);
    return result;
  }

  Future visitasPendientes() async {
    Uri _url =
        Uri.parse('http://157.245.82.111/api/visitaspendientes');
    final data = {
      'id_vendedor': prefs.id,
    };
    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    print(result);
    return result;
  }
}
