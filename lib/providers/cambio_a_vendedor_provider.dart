//import 'dart:convert';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';

class CambioAvendedorProvider {
  final prefs = PreferenciasUsuario();
  Future enviar(
    _radioValue1,
    _radioValue2,
    _radioValue3,
    _radioValue4,
    _radioValue5,
    _radioValue6,
    _radioValue7,
    _radioValue8,
    _radioValue9,
    _radioValue10,
    _radioValue11,
    _radioValue12,
    _radioValue13,
    _radioValue14,
    _radioValue15,
    _radioValue16,
    _radioValue17,
    _radioValue18,
    _radioValue19,
    _radioValue20,
    _radioValue21,
    _radioValue22,
    _radioValue23,
    _radioValue24,
    _radioValue25,
    _radioValue26,
    _radioValue27,
    _radioValue28,
    _radioValue29,
    _radioValue30,
  ) async {
    Uri _url =
        Uri.parse('http://10.0.2.2/Laravel/Yego/public/api/guardarencuesta');

    //Uri _url = Uri.parse('http://157.245.82.111/api/guardarencuesta');

    final data = {
      'id': prefs.id,
      'pregunta_1': _radioValue1,
      'pregunta_2': _radioValue2,
      'pregunta_3': _radioValue3,
      'pregunta_4': _radioValue4,
      'pregunta_5': _radioValue5,
      'pregunta_6': _radioValue6,
      'pregunta_7': _radioValue7,
      'pregunta_8': _radioValue8,
      'pregunta_9': _radioValue9,
      'pregunta_10': _radioValue10,
      'pregunta_11': _radioValue11,
      'pregunta_12': _radioValue12,
      'pregunta_13': _radioValue13,
      'pregunta_14': _radioValue14,
      'pregunta_15': _radioValue15,
      'pregunta_16': _radioValue16,
      'pregunta_17': _radioValue17,
      'pregunta_18': _radioValue18,
      'pregunta_19': _radioValue19,
      'pregunta_20': _radioValue20,
      'pregunta_21': _radioValue21,
      'pregunta_22': _radioValue22,
      'pregunta_23': _radioValue23,
      'pregunta_24': _radioValue24,
      'pregunta_25': _radioValue25,
      'pregunta_26': _radioValue26,
      'pregunta_27': _radioValue27,
      'pregunta_28': _radioValue28,
      'pregunta_29': _radioValue29,
      'pregunta_30': _radioValue30,

      //'id_tipo': prefs.idTipo
    };
    //print(data);

    final response = await http.post(_url,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: json.encode(data));
    var result = jsonDecode(response.body);
    print(result);
    //return result;

    //return _radioValue1;*/
  }
}
