import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';

class SubirDocumentosProvider {

  final prefs = PreferenciasUsuario();
  
  Future subirDocumentos(_perfil,_carta,_rif,_documento)async {
    Uri _url = Uri.parse('http://10.0.2.2/yego/public/api/subirimagenes');
    //Uri _url = Uri.parse('http://157.245.82.111/api/subirimagenes');
    final data = {
      'id':prefs.id,
      'perfil':_perfil,
      'documento':_documento,
      'carta':_carta,
      'rif':_rif,
    };
    
    final response = await http.post(_url,
      headers: {
        'Content-Type': 'application/json',
        'X-Requested-With':'XMLHttpRequest',
      },
      body: json.encode( data )
    );
    var result = jsonDecode(response.body);
    print(result);
    //return result;
  }
}