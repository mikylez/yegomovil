class ValidacionFormulario {
  String validateName(String value) {
    String pattern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "El Nombre es obligatorio";
    } else if (!regExp.hasMatch(value)) {
      return "El Nombre debe de ser a-z y A-Z";
    }
    return null;
  }

  String validateApellido(String value) {
    String pattern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "El Apellido es obligatorio";
    } else if (!regExp.hasMatch(value)) {
      return "El Apellido debe de ser a-z y A-Z";
    }
    return null;
  }

  String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "El Correo es obligatorio";
    } else if (!regExp.hasMatch(value)) {
      return "El Correo debe de ser @";
    }
    return null;
  }

  String validateDireccionFiscal(String value) {
    String pattern = r'(^[a-zA-Z0-9.,_+-]*$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "La Direccion es obligatoria";
    } else if (!regExp.hasMatch(value)) {
      return "El Direccion debe de ser a-z y A-Z";
    }
    return null;
  }

  String validateDireccionDespacho(String value) {
    String pattern = r'(^[a-zA-Z0-9.,_+-]*$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "La Dirección es obligatoria";
    } else if (!regExp.hasMatch(value)) {
      return "La Dirección debe de ser a-z y A-Z";
    }
    return null;
  }

   String validatorconfirmarpassword(String value) {
    if (value.length < 8) {
      return "La contraseña debe contener ocho caracteres";
    }
    return null;
  }

}
