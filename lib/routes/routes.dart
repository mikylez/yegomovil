//import 'dart:js';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:yegoapp/pages/aceptar_visita_page.dart';
import 'package:yegoapp/pages/buscar_usuario_vender_page.dart';
import 'package:yegoapp/pages/cart_vender_page.dart';
import 'package:yegoapp/pages/categorias_page.dart';
import 'package:yegoapp/pages/categorias_vender_page.dart';
import 'package:yegoapp/pages/cuenta_page.dart';
import 'package:yegoapp/pages/cuenta_vendedor_page.dart';
import 'package:yegoapp/pages/datos_usuarios_page.dart';
import 'package:yegoapp/pages/detalle_producto_page.dart';
import 'package:yegoapp/pages/detalle_vender_page.dart';
import 'package:yegoapp/pages/direccion_visita_page.dart';
import 'package:yegoapp/pages/entrega_codigo_page.dart';
import 'package:yegoapp/pages/entrega_inicio_page.dart';
import 'package:yegoapp/pages/entrega_mapa_page.dart';
import 'package:yegoapp/pages/ofertas_page.dart';
import 'package:yegoapp/pages/registrar_clientes_page.dart';
import 'package:yegoapp/pages/registrar_page.dart';
import 'package:yegoapp/pages/registro_de_visitas_page.dart';
import 'package:yegoapp/pages/solicitar_visita.page.dart';
import 'package:yegoapp/pages/subir_documentos.dart';
import 'package:yegoapp/pages/vender_page.dart';
import 'package:yegoapp/pages/vitrina_distribuidor.dart';
import 'package:yegoapp/pages/vitrina_page.dart';
import 'package:yegoapp/pages/login_page.dart';
import 'package:yegoapp/pages/cart_page.dart';
import 'package:yegoapp/pages/recibo_page.dart';
import 'package:yegoapp/pages/vitrina_vendedor_page.dart';
import 'package:yegoapp/pages/cambio_a_vendedor_page.dart';

Map<String, WidgetBuilder> routes() {
  return <String, WidgetBuilder>{
    'vitrina': (BuildContext context) => Vitrina(),
    'login': (BuildContext context) => LoginPage(),
    'detalle_producto': (BuildContext context) => DetalleProductoPage(),
    'cart': (BuildContext context) => CartPage(),
    'recibo': (BuildContext context) => ReciboPage(),
    'cuenta': (BuildContext context) => Cuenta(),
    'categorias': (BuildContext context) => Categorias(),
    'vender': (BuildContext context) => Vender(),
    'cart_vender': (BuildContext context) => CartVender(),
    'categorias_vender': (BuildContext context) => CategoriasVender(),
    'vitrina_vendedor': (BuildContext context) => VitrinaVendedor(),
    'vitrina_distribuidor': (BuildContext context) => VitrinaDistribuidor(),
    'detalle_vender': (BuildContext context) => DetalleVender(),
    'cuenta_vender': (BuildContext context) => CuentaVendedor(),
    'buscar_usuario_vender': (BuildContext context) => BuscarUsuarioVender(),
    'entrega_inicio': (BuildContext context) => EntregaInicio(),
    'entrega_mapa': (BuildContext context) => EntregaMapa(),
    'entrega_codigo': (BuildContext context) => EntregaCodigo(),
    'registar': (BuildContext context) => Registrar(),
    'datos_usuarios': (BuildContext context) => DatosUsuarios(),
    'cambio_a_vendedor': (BuildContext context) => CambioAVendedor(),
    'Direccion_Cliente': (BuildContext context) => CambioAVendedor(),
    'aceptar_visita': (BuildContext context) => AceptarVisita(),
    'solicitar_visita': (BuildContext context) => SolicitarVisita(),
    'registro_de_visita': (BuildContext context) => RegistroDeVisitas(),
    'direccion_de_visita': (BuildContext context) => DireccionVisita(),
    'registrar_clientes': (BuildContext context) => RegistrarUsuario(),
    'subir_documentos': (BuildContext context) => SubirDocumentos(),
    'ofertas':(BuildContext context) =>Ofertas(),
  };
}
