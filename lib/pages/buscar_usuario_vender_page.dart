//import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:show_up_animation/show_up_animation.dart';
import 'package:yegoapp/models/validacion_formularios.dart';
import 'package:yegoapp/pages/vender_page.dart';
import 'package:yegoapp/pages/vitrina_distribuidor.dart';
import 'package:yegoapp/providers/direcciones_provider.dart';
//import 'package:yegoapp/pages/vitrina_vendedor_page.dart';
import 'package:yegoapp/providers/vitrina_provider.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';
import 'cart_vender_page.dart';

class BuscarUsuarioVender extends StatefulWidget {
  BuscarUsuarioVender({Key key}) : super(key: key);

  @override
  _BuscarUsuarioVenderState createState() => _BuscarUsuarioVenderState();
}

class _BuscarUsuarioVenderState extends State<BuscarUsuarioVender> {
  final prefs = PreferenciasUsuario();
  final vitrinaprovider = VitrinaProvider();
  final negociosProvider = Directionrepository();
  final validator = ValidacionFormulario();
  final _formulario = GlobalKey<FormState>();
  TextEditingController _inputnombrecomercio = new TextEditingController();
  TextEditingController _inputtelefonocomercio = new TextEditingController();
  TextEditingController _inputdocumentocomercio = new TextEditingController();
  TextEditingController _inputnombre = new TextEditingController();
  TextEditingController _inputBuscar = new TextEditingController();
  //TextEditingController _inputapellido =new TextEditingController();
  TextEditingController _inputdocumento = new TextEditingController();
  TextEditingController _inputcorreo = new TextEditingController();
  TextEditingController _inputtelefono = new TextEditingController();
  TextEditingController _imputCredito = new TextEditingController();
  //TextEditingController _inputfecha =new TextEditingController();
  TextEditingController _inputdireccionfiscal = new TextEditingController();
  TextEditingController _inputdirecciondespacho = new TextEditingController();
  //int _radioValue = 2;
  int _radioValueContribuyente = 2;
  String _opcionSelecionadaNacionalidad = 'V';
  String _opcionSelecionadaNacionalidadcomercio = 'J';
  List<String> _nacionalidad = ['V', 'E', 'J'];

  //var value;
  var municipio;
  var parroquia;
  var municipiodespacho;
  var parroquiadespacho;
  var ndocumento;
  var sdocumento;
  var comercio;
  var snegocio;
  var _tiponegocio;
  var _estadoFiscal;
  var _municipiosFiscal;
  var _parroquiasFiscal;
  var _estadoDespacho;
  var _municipiosDespacho;
  var _parroquiasDespacho;
  var _info;
  var datosDireccion;
  var datosNegocio;
  var _negocioelegido = 0;
  var lengthh;
  var dVisible = false;
  var flete;
  /*void _inputsexo(int value) {
    setState(() {
      _radioValue = value;
    });
  }*/
  void _inputcontribuyente(int value) {
    setState(() {
      _radioValueContribuyente = value;
    });
  }

  _loadEstadosMUnicipiosParroquia() async {
    var datosEstadosMunicipiosParroquia;
    datosEstadosMunicipiosParroquia = await vitrinaprovider.direccion();
    setState(() {
      datosDireccion = datosEstadosMunicipiosParroquia;
    });
  }

  _loadNegocios() async {
    var listaNegocios;
    listaNegocios = await negociosProvider.negocios();
    setState(() {
      datosNegocio = listaNegocios;
    });
  }

  void initState() {
    super.initState();
    _loadEstadosMUnicipiosParroquia();
    _loadNegocios();
  }

  _spinkit() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return SpinKitRotatingCircle(
          color: Colors.white,
          size: 50.0,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments;
    // print(args);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              child: Text(
                'Informacion del Usuario',
                style: TextStyle(
                  fontSize: 25,
                ),
              ),
            ),
          ],
        ),
        backgroundColor: HexColor('#0067A2'),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formulario,
          child: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                ShowUpAnimation(
                  //delayStart: Duration(seconds:1 ),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: _crearImputBuscar(),
                ),
                ShowUpAnimation(
                  animationDuration: Duration(seconds: 2),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: -0.5,
                  child: TextButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blue)),
                    child: Text('Buscar Usuario',
                        style: TextStyle(color: Colors.white)),
                    onPressed: () {
                      if (_inputBuscar.text.length != 0) {
                        vitrinaprovider
                            .informacionUsuarioVender(_inputBuscar.text)
                            .then((value) {
                          if (value["info"] != null) {
                            if (value["registrado"] == 'si') {
                              setState(() {
                                _info = value;
                              });
                              var arr = _info["info"]["rif_comercio_asociado"]
                                  .split('-');
                              var numero = arr.length;
                              if (numero > 2) {
                                ndocumento = "";
                                setState(() {
                                  sdocumento = arr[0];
                                });
                                for (var i = 1; i < numero; i++) {
                                  setState(() {
                                    ndocumento = ndocumento + arr[i];
                                  });
                                }
                                //print(ndocumento);
                                //var ndocumento
                              } else {
                                setState(() {
                                  sdocumento = arr[0];
                                  ndocumento = arr[1];
                                });
                              }
                              //print(arr);
                              switch (_info["info"]["tipo_cliente"]) {
                                case 1:
                                  setState(() {
                                    comercio = "No Aplica";
                                    snegocio = 1;
                                  });
                                  break;
                                case 2:
                                  setState(() {
                                    comercio = "Bodegon";
                                    snegocio = 2;
                                  });
                                  break;
                                case 3:
                                  setState(() {
                                    comercio = "Panaderia";
                                    snegocio = 3;
                                  });

                                  break;
                                case 4:
                                  setState(() {
                                    comercio = "Abasto";
                                    snegocio = 4;
                                  });

                                  break;
                                case 5:
                                  setState(() {
                                    comercio = "Sup. Independiente";
                                    snegocio = 5;
                                  });

                                  break;
                                case 6:
                                  setState(() {
                                    comercio = "kiosko";
                                    snegocio = 6;
                                  });

                                  break;
                                case 7:
                                  setState(() {
                                    comercio = "Food Servise";
                                    snegocio = 7;
                                  });

                                  break;
                                default:
                                  setState(() {
                                    comercio = "No Aplica";
                                    snegocio = 1;
                                  });
                                  break;
                              }
                              if (snegocio != 1) {
                                setState(() {
                                  dVisible = true;
                                });
                              }
                              setState(() {
                                //_info =value;
                                //_inputdocumentocomercio.text=_info["info"]["nombre_usuario"];
                                _inputnombrecomercio.text =
                                    _info["info"]["nombre_comercio_asociado"];
                                _inputtelefonocomercio.text =
                                    _info["info"]["telefono_comercio_asociado"];
                                _tiponegocio = comercio;
                                _negocioelegido = snegocio;
                                _inputnombre.text =
                                    _info["info"]["razon_social"];
                                //_inputapellido.text =_info["info"]["apellido_usuario"];
                                _inputdocumento.text =
                                    _info["info"]["documento"];
                                _inputcorreo.text = _info["correo"];
                                _inputtelefono.text = _info["info"]["telefono"];
                                // _inputfecha.text=_info["info"]["fecha_nacimiento"];
                                _inputdireccionfiscal.text =
                                    _info["direccion_fiscal"]
                                        ["direccion_usuario"];
                                _inputdirecciondespacho.text =
                                    _info["direccion_despacho"]
                                        ["direccion_usuario"];
                                _opcionSelecionadaNacionalidad =
                                    _info["info"]["nacionalidad"];
                                _estadoFiscal =
                                    _info["estado_fiscal"]["nombre"];
                                _municipiosFiscal =
                                    _info["municipio_fiscal"]["nombre"];
                                _parroquiasFiscal =
                                    _info["parroquia_fiscal"]["nombre"];
                                _estadoDespacho =
                                    _info["estado_despacho"]["nombre"];
                                _municipiosDespacho =
                                    _info["municipio_despacho"]["nombre"];
                                _parroquiasDespacho =
                                    _info["parroquia_despacho"]["nombre"];
                                /* if (_info["info"]["sexo"] == "M") {
                                    _radioValue = 0;
                                  }else{
                                    _radioValue = 1;
                                  }*/
                                if (_info["info"]["contribuyente"] == 0) {
                                  _radioValueContribuyente = 0;
                                } else {
                                  _radioValueContribuyente = 1;
                                }
                              });
                            } else {
                              setState(() {
                                _info = value;
                              });
                              var arr = _info["info"]["rif_comercio_asociado"]
                                  .split('-');
                              var numero = arr.length;
                              if (numero > 2) {
                                ndocumento = "";
                                setState(() {
                                  sdocumento = arr[0];
                                });
                                for (var i = 1; i < numero; i++) {
                                  setState(() {
                                    ndocumento = ndocumento + arr[i];
                                  });
                                }
                                //print(ndocumento);
                                //var ndocumento
                              } else {
                                setState(() {
                                  sdocumento = arr[0];
                                  ndocumento = arr[1];
                                });
                              }
                              //print(numero);
                              switch (_info["info"]["tipo_cliente"]) {
                                case 1:
                                  setState(() {
                                    comercio = "No Aplica";
                                    snegocio = 1;
                                  });
                                  break;
                                case 2:
                                  setState(() {
                                    comercio = "Bodegon";
                                    snegocio = 2;
                                  });
                                  break;
                                case 3:
                                  setState(() {
                                    comercio = "Panaderia";
                                    snegocio = 3;
                                  });

                                  break;
                                case 4:
                                  setState(() {
                                    comercio = "Abasto";
                                    snegocio = 4;
                                  });

                                  break;
                                case 5:
                                  setState(() {
                                    comercio = "Sup. Independiente";
                                    snegocio = 5;
                                  });

                                  break;
                                case 6:
                                  setState(() {
                                    comercio = "kiosko";
                                    snegocio = 6;
                                  });

                                  break;
                                case 7:
                                  setState(() {
                                    comercio = "Food Servise";
                                    snegocio = 7;
                                  });

                                  break;
                                default:
                                  setState(() {
                                    comercio = "No Aplica";
                                    snegocio = 1;
                                  });
                                  break;
                              }
                              if (snegocio != 1) {
                                setState(() {
                                  dVisible = true;
                                });
                              }
                              setState(() {
                                //_info =value;
                                _inputnombrecomercio.text =
                                    _info["info"]["nombre_comercio_asociado"];
                                _inputtelefonocomercio.text =
                                    _info["info"]["telefono_comercio_asociado"];
                                _tiponegocio = comercio;
                                _negocioelegido = snegocio;
                                _opcionSelecionadaNacionalidadcomercio =
                                    sdocumento;
                                _inputdocumentocomercio.text = ndocumento;
                                _inputnombre.text =
                                    _info["info"]["razon_social"];
                                //_inputapellido.text =_info["info"]["apellido_cliente"];
                                _inputdocumento.text =
                                    _info["info"]["documento"];
                                _inputcorreo.text =
                                    _info["info"]["correo_cliente"];
                                _inputtelefono.text = _info["info"]["telefono"];
                                _imputCredito.text = _info["info"]["credito"];
                                //_inputfecha.text=_info["info"]["fecha_nacimiento"];
                                _inputdireccionfiscal.text =
                                    _info["direccion_fiscal"];
                                _inputdirecciondespacho.text =
                                    _info["direccion_despacho"];
                                _opcionSelecionadaNacionalidad =
                                    _info["info"]["nacionalidad"];
                                _estadoFiscal =
                                    _info["estado_fiscal"]["nombre"];
                                _municipiosFiscal =
                                    _info["municipio_fiscal"]["nombre"];
                                _parroquiasFiscal =
                                    _info["parroquia_fiscal"]["nombre"];
                                _estadoDespacho =
                                    _info["estado_despacho"]["nombre"];
                                _municipiosDespacho =
                                    _info["municipio_despacho"]["nombre"];
                                _parroquiasDespacho =
                                    _info["parroquia_despacho"]["nombre"];
                                /*if (_info["info"]["sexo"] == "M") {
                                    _radioValue = 0;
                                  }else{
                                    _radioValue = 1;
                                  }*/
                                if (_info["info"]["contribuyente"] == 0) {
                                  _radioValueContribuyente = 0;
                                } else {
                                  _radioValueContribuyente = 1;
                                }
                              });
                            }
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text('Documento no Registrado'),
                                backgroundColor: Colors.red,
                              ),
                            );
                          }
                        });
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text('Introducir Documento de Usuario'),
                            backgroundColor: Colors.red,
                          ),
                        );
                      }
                    },
                  ),
                ),
                //AGREGANDO TITULO
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds:1 ),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: Text(
                    'Datos Negocio',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                //HASTA AQUI
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 6),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: _negocios(),
                ),
                Visibility(
                    //esto es para unidades
                    visible: dVisible,
                    child: Column(
                      children: [
                        SizedBox(height: 15),
                        ShowUpAnimation(
                          //delayStart: Duration(seconds: 6),
                          animationDuration: Duration(seconds: 1),
                          curve: Curves.bounceInOut,
                          direction: Direction.horizontal,
                          offset: 0.5,
                          child: _crearImputNombreComercio(),
                        ),
                        SizedBox(height: 15),
                        ShowUpAnimation(
                          //delayStart: Duration(seconds: 6),
                          animationDuration: Duration(seconds: 1),
                          curve: Curves.bounceInOut,
                          direction: Direction.horizontal,
                          offset: 0.5,
                          child: _crearImputTefonoComercio(),
                        ),
                        SizedBox(height: 15),
                        ShowUpAnimation(
                          //delayStart: Duration(seconds: 6),
                          animationDuration: Duration(seconds: 1),
                          curve: Curves.bounceInOut,
                          direction: Direction.horizontal,
                          offset: 0.5,
                          child: _crearImputDocumentoComercio(),
                        ),
                      ],
                    )),
                //AGREGANDO TITULO
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds:1 ),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: Text(
                    'Datos Personales',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                //HASTA AQUI
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds:1 ),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: _crearImputNombre(),
                ),
                SizedBox(height: 15),
                /*ShowUpAnimation(
                    //delayStart: Duration(seconds: 2),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child: _crearImputApellido(),
                  ),
                  SizedBox( height: 15),*/
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 2),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: _crearImputDocumento(),
                ),
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 3),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: -0.5,
                  child: _crearImputCorreo(),
                ),
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 4),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: _crearImputTefono(),
                ),

                //AGREGANDO TITULO
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds:1 ),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: Text(
                    'Direccion Fiscal',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                //HASTA AQUI

                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 4),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: _crearImputCredito(),
                ),
                SizedBox(height: 15),
                /* ShowUpAnimation(
                    //delayStart: Duration(seconds: 5),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:  _crearImputFecha(),
                  ),*/
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 6),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: _estados(),
                ),
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 6),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: -0.5,
                  child: _municipio(),
                ),
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 6),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: _parroquia(),
                ),
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 6),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: -0.5,
                  child: _crearImputDireccionfiscal(),
                ),

                //AGREGANDO TITULO
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds:1 ),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: Text(
                    'Direccion Despacho',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                //HASTA AQUI

                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 6),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: _estadosDespacho(),
                ),
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 6),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: -0.5,
                  child: _municipioDespacho(),
                ),
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 6),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: 0.5,
                  child: _parroquiaDespacho(),
                ),
                SizedBox(height: 15),
                ShowUpAnimation(
                  //delayStart: Duration(seconds: 7),
                  animationDuration: Duration(seconds: 1),
                  curve: Curves.bounceInOut,
                  direction: Direction.horizontal,
                  offset: -0.5,
                  child: _crearImputDirecciondespacho(),
                ),
                SizedBox(height: 15),
                /* ShowUpAnimation(
                    //delayStart: Duration(seconds: 8),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: Row(
                      children: [
                        Icon(Icons.person,size: 40,color: Colors.grey,),
                        Radio(
                          value: 0,
                          groupValue: _radioValue,
                          activeColor: Colors.blue,
                          onChanged: (value){
                           _inputsexo(value);
                          },
                        ),
                        Text('Hombre'),
                        Radio(
                          value: 1,
                          groupValue: _radioValue,
                          activeColor: Colors.blue,
                          onChanged: (value){
                            _inputsexo(value);
                          },
                        ),
                        Text('Mujer'),
                      ],
                    ),
                  ),*/

                Visibility(
                  //esto es para unidades
                  visible: false,
                  child: ShowUpAnimation(
                    //delayStart: Duration(seconds: 8),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: Row(
                      children: [
                        Icon(
                          Icons.person,
                          size: 40,
                          color: Colors.grey,
                        ),
                        Text('Contribuyente Especial'),
                        Radio(
                          value: 1,
                          groupValue: _radioValueContribuyente,
                          activeColor: Colors.blue,
                          onChanged: (value) {
                            _inputcontribuyente(value);
                          },
                        ),
                        Text('Si'),
                        Radio(
                          value: 0,
                          groupValue: _radioValueContribuyente,
                          activeColor: Colors.blue,
                          onChanged: (value) {
                            _inputcontribuyente(value);
                          },
                        ),
                        Text('No'),
                      ],
                    ),
                  ),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ShowUpAnimation(
                      animationDuration: Duration(seconds: 2),
                      curve: Curves.bounceInOut,
                      direction: Direction.horizontal,
                      offset: -0.5,
                      child: TextButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.blue)),
                        child: Text('Procesar Pedido',
                            style: TextStyle(color: Colors.white)),
                        onPressed: () async {
                          if (_formulario.currentState.validate()) {
                            _radioValueContribuyente = 0;
                            if (_radioValueContribuyente == 2) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  content: Text(
                                      'Selecccionar Si es Contribuyente Especial'),
                                  backgroundColor: Colors.red,
                                ),
                              );
                            } else {
                              _spinkit();
                              await vitrinaprovider
                                  .pedidovender(
                                      args,
                                      _inputBuscar.text,
                                      _inputcorreo.text,
                                      _inputnombre.text,
                                      _inputtelefono.text,
                                      _opcionSelecionadaNacionalidad,
                                      _radioValueContribuyente,
                                      _estadoFiscal,
                                      _municipiosFiscal,
                                      _parroquiasFiscal,
                                      _estadoDespacho,
                                      _municipiosDespacho,
                                      _parroquiasDespacho,
                                      _inputdocumentocomercio.text,
                                      _inputnombrecomercio.text,
                                      _inputtelefonocomercio.text,
                                      _opcionSelecionadaNacionalidadcomercio,
                                      _negocioelegido,
                                      _inputdireccionfiscal.text,
                                      _inputdirecciondespacho.text,
                                      _inputdocumento.text,
                                      _imputCredito.text)
                                  .then((value) {
                                if (value == 1) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => CartVender()),
                                  );
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text(
                                          'Alguno de tu Productos no puede ser Procesado'),
                                      backgroundColor: Colors.red,
                                    ),
                                  );
                                } else {
                                  if (prefs.idTipo == 9) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              VitrinaDistribuidor()),
                                    );
                                  } else {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Vender()),
                                    );
                                  }
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text('Pedido Procesado'),
                                      backgroundColor: Colors.green,
                                    ),
                                  );
                                }
                              });
                            }
                          }
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _crearImputBuscar() {
    return TextField(
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      autofocus: false,
      controller: _inputBuscar,
      textCapitalization: TextCapitalization.sentences,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: 'Buscar Documento del Usuario',
        labelText: 'Buscar Documento del Usuario',
        suffixIcon: Icon(
          Icons.search,
          size: 30,
        ),
        icon: Icon(
          Icons.search,
          size: 40,
        ),
      ),
    );
  }

  _crearImputNombre() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: _inputnombre,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: 'Razón Social',
        labelText: 'Razón Social',
        suffixIcon: Icon(
          Icons.account_circle,
          size: 30,
        ),
        icon: Icon(
          Icons.account_circle,
          size: 40,
        ),
      ),
      validator: validator.validateName,
    );
  }

  /*_crearImputApellido() {
    return TextFormField(
       autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: _inputapellido,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Apellido',
       labelText: 'Apellido',
       suffixIcon: Icon(Icons.account_circle,size: 30,),
       icon: Icon(Icons.account_circle,size:40,),
      ),
         validator: validator.validateApellido,
    );
  }*/
  _crearImputCorreo() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.emailAddress,
      controller: _inputcorreo,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: 'Correo',
        labelText: 'Correo',
        suffixIcon: Icon(
          Icons.email,
          size: 30,
        ),
        icon: Icon(
          Icons.email,
          size: 40,
        ),
      ),
      validator: validator.validateEmail,
    );
  }

  _crearImputTefono() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.number,
      controller: _inputtelefono,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: 'Telefono',
        labelText: ' Telefono',
        suffixIcon: Icon(
          Icons.phone,
          size: 30,
        ),
        icon: Icon(
          Icons.phone,
          size: 40,
        ),
      ),
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      validator: (value) {
        if (value.isEmpty) {
          return 'El Telefono es Obligatorio';
        }
        return null;
      },
    );
  }

  _crearImputCredito() {
    if (prefs.idTipo == 9) {
      return TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        autofocus: false,
        keyboardType: TextInputType.number,
        controller: _imputCredito,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          hintText: 'Dias de Credito',
          labelText: 'Dias de Credito',
          suffixIcon: Icon(
            Icons.credit_card,
            size: 30,
          ),
          icon: Icon(
            Icons.credit_card,
            size: 40,
          ),
        ),
        /*inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
            validator: (value){
          if (value.isEmpty) {
            return 'El Campo es Obligatorio';
          }
          return null;
        },*/
      );
    }
  }

  List<DropdownMenuItem<String>> getOpcionesDropdown() {
    List<DropdownMenuItem<String>> _lista = [];
    _nacionalidad.forEach((nacionalida) {
      _lista.add(DropdownMenuItem(
        child: Text(nacionalida),
        value: nacionalida,
      ));
    });
    return _lista;
  }

  _crearImputDocumento() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.number,
      controller: _inputdocumento,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: 'Documento',
        labelText: ' Documento',
        prefixIcon: Container(
          padding: EdgeInsets.only(left: 15),
          child: DropdownButton(
            underline: Container(
                decoration:
                    BoxDecoration(border: Border(bottom: BorderSide.none))),
            value: _opcionSelecionadaNacionalidad,
            items: getOpcionesDropdown(),
            onChanged: (opt) {
              setState(() {
                _opcionSelecionadaNacionalidad = opt;
              });
            },
          ),
        ),
        icon: Icon(
          Icons.account_box,
          size: 40,
        ),
        suffixIcon: Icon(
          Icons.account_box,
          size: 30,
        ),
      ),
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      validator: (value) {
        if (value.isEmpty) {
          return 'El Documento es Obligatorio';
        }
        return null;
      },
    );
  }

  _crearImputNombreComercio() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: _inputnombrecomercio,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: 'Nombre Comercio',
        labelText: 'Nombre Comercio',
        suffixIcon: Icon(
          Icons.account_circle,
          size: 30,
        ),
        icon: Icon(
          Icons.account_circle,
          size: 40,
        ),
      ),
      validator: validator.validateName,
    );
  }

  _crearImputTefonoComercio() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.number,
      controller: _inputtelefonocomercio,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: 'Telefono Comercio',
        labelText: ' Telefono Comercio',
        suffixIcon: Icon(
          Icons.phone,
          size: 30,
        ),
        icon: Icon(
          Icons.phone,
          size: 40,
        ),
      ),
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      /*validator: (value){
        if (value.isEmpty) {
          return 'El Telefono es Obligatorio';
        }
        return null;
      },*/
    );
  }

  _crearImputDocumentoComercio() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.number,
      controller: _inputdocumentocomercio,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: 'Documento Comercio',
        labelText: 'Documento Comercio',
        prefixIcon: Container(
          padding: EdgeInsets.only(left: 15),
          child: DropdownButton(
            underline: Container(
                decoration:
                    BoxDecoration(border: Border(bottom: BorderSide.none))),
            value: _opcionSelecionadaNacionalidadcomercio,
            items: getOpcionesDropdown(),
            onChanged: (opt) {
              setState(() {
                _opcionSelecionadaNacionalidadcomercio = opt;
              });
            },
          ),
        ),
        icon: Icon(
          Icons.account_box,
          size: 40,
        ),
        suffixIcon: Icon(
          Icons.account_box,
          size: 30,
        ),
      ),
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      /*validator: (value){
        if (value.isEmpty) {
          return 'El Documento es Obligatorio';
        }
        return null;
      },*/
    );
  }

  /*_crearImputFecha() {
    return TextFormField(
    autofocus: false,
    controller: _inputfecha,
    decoration: InputDecoration(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      hintText: 'Fecha de Nacimiento',
      labelText: 'Fecha de Nacimiento',
      suffixIcon: Icon(Icons.calendar_today,size: 30),
      icon: Icon(Icons.calendar_today,size:40),
    ),
     onTap: (){
       FocusScope.of(context).requestFocus(new FocusNode());
       _selectDate(context);
     },
        validator: (value){
        if (value.isEmpty) {
          return 'La Fecha es Obligatoria';
        }
        return null;
      },
    );
  }*/

  /* _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(1920),
      lastDate:  new DateTime(2025),
    );
    if (picked!=null) {
      setState(() {
        String fecha = DateFormat('dd-MM-yyyy').format(picked);
        _inputfecha.text = fecha;
      });
    }
  }*/

  //para tipo de negocio
  _negocios() {
    if (datosDireccion != null) {
      return Container(
        child: DropdownButtonFormField(
          autofocus: false,
          value: _tiponegocio,
          decoration: InputDecoration(
            labelText: 'Tipo Negocio',
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items: datosNegocio[0]?.map<DropdownMenuItem>((map) {
                return DropdownMenuItem(
                  child: Text(map),
                  value: map,
                );
              })?.toList() ??
              [],
          onChanged: (value) {
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _tiponegocio = value;
              _negocioelegido = datosNegocio[0].indexOf(value) + 1;
              if (_negocioelegido != 1) {
                dVisible = true;
              } else {
                dVisible = false;
              }
              print(_negocioelegido);
            });
          },
        ),
      );
    } else {
      return CircularProgressIndicator();
    }
  }
  //hasta aca

  _estados() {
    if (datosDireccion != null) {
      return Container(
        child: DropdownButtonFormField(
          autofocus: false,
          value: _estadoFiscal,
          decoration: InputDecoration(
            labelText: 'Estado Direccion Fiscal',
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items: datosDireccion[0]?.map<DropdownMenuItem>((map) {
                return DropdownMenuItem(
                  child: Text(map),
                  value: map,
                );
              })?.toList() ??
              [],
          onChanged: (value) {
            if (_estadoFiscal==null) {
              setState(() {
                _estadoFiscal = value;
              });    
            } else {
              setState(() {
                _estadoFiscal =null;
                _municipiosFiscal =null;
                _parroquiasFiscal =null;
                _estadoFiscal = value;

              });
            }
            FocusScope.of(context).requestFocus(new FocusNode());
            vitrinaprovider.municipios(_estadoFiscal).then((value) {
              setState(() {
                municipio= value; 
              });
             
            });
          },
        ),
      );
    } else {
      return CircularProgressIndicator();
    }
  }

  _municipio() {
    if (municipio != null) {
      return Container(
        child: DropdownButtonFormField(
          autofocus: false,
          value: _municipiosFiscal,
          decoration: InputDecoration(
            labelText: 'Municipio Direccion Fiscal',
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items: municipio['municipio']?.map<DropdownMenuItem>((map){
                return DropdownMenuItem(
                  child: Text(map['nombre'].toString()),
                  value: map,
                );
              })?.toList() ??
              [],
          onChanged: (value) {
            if (_municipiosFiscal==null) {
              setState(() {
              _municipiosFiscal = value;
            });
            } else {
              setState(() {
                _municipiosFiscal = value;
                _parroquiasFiscal =null;
              });
            }
            FocusScope.of(context).requestFocus(new FocusNode());
            vitrinaprovider.parroquia(_municipiosFiscal).then((value) {
              setState(() {
                parroquia=value;
              });
            });
          },
        ),
      );
    } else {
      return Container(
        child: DropdownButtonFormField(
          autofocus: false,
          value: _municipiosFiscal,
          decoration: InputDecoration(
            labelText: 'Municipio Direccion Fiscal',
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items: [],
          onChanged: (value) {
          },
        ),
      );
    }
  }

  _parroquia() {
    if (parroquia != null) {
      return Container(
        child: DropdownButtonFormField(
          autofocus: false,
          value: _parroquiasFiscal,
          decoration: InputDecoration(
            labelText: 'Parroquia',
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items: parroquia['parroquia']?.map<DropdownMenuItem>((map) {
                return DropdownMenuItem(
                  child: Text(map['nombre'].toString()),
                  value: map,
                );
              })?.toList() ??
              [],
          onChanged: (value) {
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _parroquiasFiscal = value;
            });
          },
        ),
      );
    } else {
      return Container(
        child: DropdownButtonFormField(
          autofocus: false,
          value: _parroquiasFiscal,
          decoration: InputDecoration(
            labelText: 'Parroquia',
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items: [],
          onChanged: (value) {

          },
        ),
      );
    }
  }

  _crearImputDireccionfiscal() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      maxLines: null,
      keyboardType: TextInputType.multiline,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      controller: _inputdireccionfiscal,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: 'Direccion Fiscal',
        labelText: 'Direccion Fiscal',
        icon: Icon(
          Icons.location_on,
          size: 40,
        ),
        suffixIcon: Icon(
          Icons.location_on,
          size: 30,
        ),
      ),
      //validator: validator.validateDireccionFiscal,
    );
  }

  _crearImputDirecciondespacho() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      maxLines: null,
      keyboardType: TextInputType.multiline,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      controller: _inputdirecciondespacho,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: 'Direccion de Despacho',
        labelText: 'Direccion de Despacho',
        icon: Icon(
          Icons.location_on,
          size: 40,
        ),
        suffixIcon: Icon(
          Icons.location_on,
          size: 30,
        ),
      ),
      //validator: validator.validateDireccionDespacho,
    );
  }

  _estadosDespacho() {
    if (datosDireccion != null) {
      return Container(
        child: DropdownButtonFormField(
          autofocus: false,
          value: _estadoDespacho,
          decoration: InputDecoration(
            labelText: 'Estado Direccion de Despacho',
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items: datosDireccion[0]?.map<DropdownMenuItem>((map) {
                return DropdownMenuItem(
                  child: Text(map),
                  value: map,
                );
              })?.toList() ??
              [],
          onChanged: (value) {
            if (_estadoDespacho==null) {
              setState(() {
                _estadoDespacho = value;
              });    
            } else {
              setState(() {
                _estadoDespacho =null;
                _municipiosDespacho =null;
                _parroquiasDespacho =null;
                _estadoDespacho = value;

              });
            }
            FocusScope.of(context).requestFocus(new FocusNode());
            vitrinaprovider.municipios(_estadoDespacho).then((value) {
              setState(() {
                municipiodespacho= value; 
              });
             
            });
          },
        ),
      );
    } else {
      return CircularProgressIndicator();
    }
  }

  _municipioDespacho() {
    if (municipiodespacho != null) {
      return Container(
        child: DropdownButtonFormField(
          autofocus: false,
          value: _municipiosDespacho,
          decoration: InputDecoration(
            labelText: 'Municipio Direccion de Despacho',
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items: municipiodespacho['municipio']?.map<DropdownMenuItem>((map) {
                return DropdownMenuItem(
                  child: Text(map['nombre'].toString()),
                  value: map,
                );
              })?.toList() ??
              [],
          onChanged: (value) {
            if (_estadoDespacho==null) {
              setState(() {
                _municipiosDespacho = value;
              });    
            } else {
              setState(() {
                _municipiosDespacho =null;
                _parroquiasDespacho =null;
                _municipiosDespacho = value;

              });
            }
            FocusScope.of(context).requestFocus(new FocusNode());
            vitrinaprovider.parroquia(_municipiosDespacho).then((value) {
              setState(() {
                parroquiadespacho=value;
              });
            });
          },
        ),
      );
    } else {// VACIO
      return Container(
        child: DropdownButtonFormField(
          autofocus: false,
          value: _municipiosDespacho,
          decoration: InputDecoration(
            labelText: 'Municipio Direccion de Despacho',
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items: [],
          onChanged: (value) {
            
          },
        ),
      );
    }
  }

  _parroquiaDespacho() {
    if (parroquiadespacho != null) {
      return Container(
        child: DropdownButtonFormField(
          autofocus: false,
          value: _parroquiasDespacho,
          decoration: InputDecoration(
            labelText: 'Parroquia Direccion de Despacho',
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items: parroquiadespacho['parroquia']?.map<DropdownMenuItem>((map) {
                return DropdownMenuItem(
                  child: Text(map['nombre'].toString()),
                  value: map,
                );
              })?.toList() ??
              [],
          onChanged: (value) {
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _parroquiasDespacho = value;
            });
          },
        ),
      );
    } else {
      return Container(
        child: DropdownButtonFormField(
          autofocus: false,
          value: _parroquiasDespacho,
          decoration: InputDecoration(
            labelText: 'Parroquia Direccion de Despacho',
            icon: Icon(
              Icons.location_on,
              size: 40,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items: [],
          onChanged: (value) {
      
          },
        ),
      );
    }
  }
}
