import 'dart:convert';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:yegoapp/pages/buscar_page.dart';
import 'package:yegoapp/pages/buscar_vender_page.dart';
import 'package:yegoapp/pages/cuenta_page.dart';
import 'package:yegoapp/pages/cuenta_vendedor_page.dart';
import 'package:yegoapp/pages/vender_page.dart';
import 'package:yegoapp/pages/vitrina_page.dart';
import 'package:yegoapp/providers/subir_documentos_provider.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';
import 'package:yegoapp/widget/mydrawer.dart';

class SubirDocumentos extends StatefulWidget {
  @override
  State<SubirDocumentos> createState() => _SubirDocumentosState();
}

class _SubirDocumentosState extends State<SubirDocumentos> {
  String _ruta;
  String _ruta1;
  String _ruta2;
  String _ruta3;
  String _perfil;
  String _documento;
  String _carta;
  String _rif;
  final subirdocumentosprovider = SubirDocumentosProvider();
  final prefs = PreferenciasUsuario();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: HexColor('#0067A2').withOpacity(0.7),
        ),
        child: myDrawer(context),
      ),
      appBar: AppBar(
        backgroundColor: HexColor('#0067A2'),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 140 ),
            child: ClipRRect(
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, 'vitrina'),
                child: FadeInImage(
                    image: AssetImage('assets/img/4.png'),
                    placeholder: AssetImage('assets/img/no-image.jpg'),
                    fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
        leading: IconButton(
          iconSize: 35,
          icon: Icon(Icons.menu),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 30),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: [
                    Container(
                      child: Text(
                        "CAMBIO DE USUARIO A VENDEDOR",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: HexColor('#0067A2'),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 25),
                      child: Text(
                        "Asegúrate que toda la información sea legible y no esté borrosa.Igualmente asegúrate de que se vean todas las esquinas de este documento. No se aceptará ningún documento si no se puede leer correctamente, y si no se ven las cuatro esquinas de este documento. En la imagen inferior te ofrecemos un ejemplo de cómo debe enmarcarse la fotografía de este documento.",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          color: HexColor('#0067A2'),
                          
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
              (_ruta == null ) ? Container() : Image.file(File(_ruta),width: 300,height: 200,),
              ElevatedButton (
                child: Text("Foto Perfil"),
                onPressed: () async {
                  final ImagePicker _picker = ImagePicker();
                  XFile _archivo = await _picker.pickImage(source: ImageSource.gallery);
                  setState(() {
                    _ruta =_archivo.path;
                  });
                  List bytes = new File(_ruta).readAsBytesSync();
                  _perfil =base64.encode(bytes);
                }
              ),
              (_ruta1 == null ) ? Container() : Image.file(File(_ruta1),width: 300,height: 200,),
              ElevatedButton (
                child: Text("Foto Documento de Identidad"),
                onPressed: () async {
                  final ImagePicker _picker = ImagePicker();
                  XFile _archivo = await _picker.pickImage(source: ImageSource.gallery);
                  setState(() {
                    _ruta1 =_archivo.path;
                  });
                  List bytes = new File(_ruta1).readAsBytesSync();
                  _documento =base64.encode(bytes);
                }
              ),
              (_ruta2 == null ) ? Container() : Text(
                _ruta2,
                style: TextStyle(
                  fontSize: 25,
                  color: HexColor('#0067A2'),
                ),
              ),
              ElevatedButton (
                child: Text("PDF del RIF"),
                onPressed: () async {
                  FilePickerResult  result = await FilePicker.platform.pickFiles(); 
                  List rif = new File(result.files.first.path).readAsBytesSync();
                  _rif=base64.encode(rif);
                  //final file = result.files.first;
                  setState(() {
                    _ruta2 =result.files.first.name;
                  });
                
                }
              ),
              (_ruta3 == null ) ? Container() : Text(
                _ruta3,
                style: TextStyle(
                  fontSize: 25,
                  color: HexColor('#0067A2'),
                ),
              ),
              ElevatedButton (
                child: Text("PDF de Carta de Residencia"),
                onPressed: () async {
                  FilePickerResult  result = await FilePicker.platform.pickFiles();
                  //Uint8List fileBytes = result.files.first.bytes;
                  List carta = new File(result.files.first.path).readAsBytesSync();
                  _carta=base64.encode(carta);
                  //_rif =base64.encode(result);
                  setState(() {
                    _ruta3 =result.files.first.name;
                    //_rif = result;
                  });
                }
              ),
              ElevatedButton (
                child: Text("ENVIAR SOLICITUD"),
                onPressed: (){
                  subirdocumentosprovider.subirDocumentos(_perfil,_carta,_rif,_documento);
              })
            ],
          ),
        ),
      ),
      bottomNavigationBar: _footer(context)
    );
  }
  _footer(BuildContext context) {
    var idtipo = prefs.idTipo;
    var _currentPage = 0;
    if (idtipo == 1) {
      return Theme(
        data: Theme.of(context).copyWith(
          canvasColor: HexColor('#0067A2'),
          primaryColor: Colors.yellowAccent,
        ),
        child: BottomNavigationBar(
          unselectedItemColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.house, size: 30),
              label: 'Comprar',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search, size: 30),
              label: 'Buscar',
            ),
            /*BottomNavigationBarItem(
                icon: Icon(Icons.category, size: 30), label: 'Categorías'),*/
            BottomNavigationBarItem(
                icon: Icon(Icons.account_circle, size: 30), label: 'Cuenta'),
          ],
          currentIndex: _currentPage,
          onTap: (int index) {
            setState(() {
              _currentPage = index;
              switch (_currentPage) {
                case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Vitrina()),
                  );
                  break;
                case 1:
                  showSearch(context: context, delegate: DataSearch());
                  break;
                case 2:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Cuenta()),
                  );
                  break;
                default:
              }
            });
          },
        ),
      );
    }
    if (idtipo == 2) {
      return Theme(
        data: Theme.of(context).copyWith(
          canvasColor: HexColor('#0067A2'),
          primaryColor: Colors.yellowAccent,
        ),
        child: BottomNavigationBar(
          unselectedItemColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.house, size: 30),
              label: 'Comprar',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search, size: 30),
              label: 'Buscar',
            ),
            /*BottomNavigationBarItem(
                icon: Icon(Icons.category, size: 30), label: 'Categorías'),*/
            BottomNavigationBarItem(
                icon: Icon(Icons.credit_card_rounded, size: 30),
                label: 'Vender'),
            BottomNavigationBarItem(
                icon: Icon(Icons.account_circle, size: 30), label: 'Cuenta'),
          ],
          currentIndex: _currentPage,
          onTap: (int index) {
            setState(() {
              _currentPage = index;
              switch (_currentPage) {
                case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Vitrina()),
                  );
                  break;
                case 1:
                  showSearch(context: context, delegate: DataSearchVender());
                  break;
                case 2:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Vender()),
                  );
                  break;
                case 3:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CuentaVendedor()),
                  );
                  break;
                default:
              }
            });
          },
        ),
      );
    }
  }
}