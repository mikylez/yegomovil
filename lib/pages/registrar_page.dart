import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:yegoapp/models/validacion_formularios.dart';
import 'package:yegoapp/providers/registro_providers.dart';

class Registrar extends StatefulWidget {
  @override
  State<Registrar> createState() => _RegistrarState();
}

class _RegistrarState extends State<Registrar> {
  final a = Registro();
  final validator = ValidacionFormulario();
  TextEditingController _imputUsuario = new TextEditingController();
  TextEditingController _imputEmail = new TextEditingController();
  TextEditingController _imputPassword = new TextEditingController();
  TextEditingController _imputConfirmarPassword = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        _crearFondo(context),
        _crearFormulario(context),
      ],
    ));
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final fondoLogin = Container(
      color: HexColor("0067A2"),
      width: double.infinity,
      height: size.height,
      child: Stack(),
    );
    return Stack(children: <Widget>[
      fondoLogin,
    ]);
  }

  Widget _crearFormulario(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 0,
            ),
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  "assets/img/logo_yego.png",
                ),
                // Text('Ingreso',
                //     style: TextStyle(
                //         fontSize: 35.0,
                //         color: HexColor("#FFFFFF"),
                //         fontWeight: FontWeight.bold)),
                _crearUsuario(),
                SizedBox(
                  height: 15,
                ),
                _crearEmail(),
                SizedBox(
                  height: 15,
                ),
                _crearPassword(),
                SizedBox(
                  height: 15,
                ),
                _confirmarPassword(),
                SizedBox(
                  height: 31,
                ),
                _crearBoton(),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _crearEmail() {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15),
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
        color: HexColor("FFFFFF").withOpacity(1),
        borderRadius: BorderRadius.circular(25),
      ),
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        controller: _imputEmail,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          icon: Icon(
            Icons.mail_outlined,
            size: 40,
            color: HexColor("0067A2"),
          ),
          hintText: 'ejemplo@correo.com',
          labelText: 'Correo Electronico',
          //counterText: snapshot.data,
          border: InputBorder.none,
        ),
        validator: validator.validateEmail,
      ),
    );
  }

  Widget _crearUsuario() {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15),
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
        color: HexColor("FFFFFF").withOpacity(1),
        borderRadius: BorderRadius.circular(25),
      ),
      child: TextFormField(
        controller: _imputUsuario,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(
            Icons.account_circle,
            size: 40,
            color: HexColor("0067A2"),
          ),
          // hintText: 'ejemplo@correo.com',
          labelText: 'Usuario',
          //counterText: snapshot.data,
          border: InputBorder.none,
        ),
      ),
    );
  }

  Widget _crearPassword() {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15),
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
        color: HexColor("FFFFFF").withOpacity(1),
        borderRadius: BorderRadius.circular(25),
      ),
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: validator.validatorconfirmarpassword,
        maxLength: 8,
        controller: _imputPassword,
        //keyboardType: TextInputType.emailAddress,
        obscureText: true,
        decoration: InputDecoration(
          icon: Icon(
            Icons.lock_outline,
            size: 40,
            color: HexColor("#0067A2"),
          ),
          hintText: '**********',
          labelText: 'Contraseña',
          //counterText: snapshot.data,
          border: InputBorder.none,
        ),
      ),
    );
  }

  Widget _confirmarPassword() {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15),
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
        color: HexColor("FFFFFF").withOpacity(1),
        borderRadius: BorderRadius.circular(25),
      ),
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: validator.validatorconfirmarpassword,
        maxLength: 8,
        controller: _imputConfirmarPassword,
        //keyboardType: TextInputType.emailAddress,
        obscureText: true,
        decoration: InputDecoration(
          icon: Icon(
            Icons.lock_outline,
            size: 40,
            color: HexColor("#0067A2"),
          ),
          hintText: '**********',
          labelText: 'Confirmar Contraseña',
          //counterText: snapshot.data,
          border: InputBorder.none,
        ),
      ),
    );
  }

  Widget _crearBoton() {
    return ElevatedButton(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 60.0, vertical: 20.0),
        child: Text(
          'Registrarse',
          style: TextStyle(
              color: HexColor("FFFFFF"),
              fontWeight: FontWeight.bold,
              fontSize: 15),
        ),
      ),
      style: ButtonStyle(
          elevation: MaterialStateProperty.all<double>(0.0),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0)))),
      onPressed: () {
        a
            .registro(_imputUsuario.text, _imputEmail.text, _imputPassword.text,
                _imputConfirmarPassword.text)
            .then((value) {
          switch (value) {
            case '1':
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text('Rellene todos los campos'),
                  backgroundColor: Colors.red,
                ),
              );
              break;

            case '2':
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text('Las contraseñas deben ser iguales'),
                  backgroundColor: Colors.red,
                ),
              );
              break;

            case '3':
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text('Correo ya registrado'),
                  backgroundColor: Colors.red,
                ),
              );
              break;

            case '4':
              Navigator.pushNamed(context, 'login');
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text('Registro Exitoso'),
                  backgroundColor: Colors.green,
                ),
              );
              break;
            default:
          }
        });
      },
    );
  }
}
