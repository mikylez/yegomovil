import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
//import 'package:yegoapp/pages/entrega_mapa_page.dart';
import 'package:yegoapp/providers/entregas_pendientes_provider.dart';
//import 'package:toast/toast.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:yegoapp/widget/mydrawer.dart';
//import 'entrega_mapa_page.dart';
import 'Dart:io';


class EntregaInicio extends StatefulWidget {
  EntregaInicio({Key key}) : super(key: key);

  @override
  _EntregaInicioState createState() => _EntregaInicioState();
}

class _EntregaInicioState extends State<EntregaInicio> {

  final despachoProvider = EntregasProvider();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    void initState() { 
    super.initState();
    //despachoProvider.despachos();
  }

  @override
  Widget build(BuildContext context) {
    //ScrollController _direccion = ScrollController (debugLabel: "");
    return Scaffold(
      key: _scaffoldKey,
      drawer: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: HexColor('#0067A2').withOpacity(0.7),
          ),
          child: myDrawer(context),
        ),
      appBar: AppBar(
        backgroundColor: HexColor('#0067A2'),
        title: Text('Entregas'),
        actions: [
          Container(
            margin: EdgeInsets.only(left: 0),
            child: ClipRRect(
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, 'lista de entregas'),
                child: FadeInImage(
                  image: AssetImage('assets/img/4.png'),
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
        leading: IconButton(
          iconSize: 35,
          icon: Icon(Icons.menu),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
      ),
      body: FutureBuilder(
        future:despachoProvider.despachos(),
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if (snapshot.hasData) {
            return ListView.builder( 
            itemCount: snapshot.data['ventas'].length,
            //controller: _direccion, 
            itemBuilder: (BuildContext context, int index){
                var orden = snapshot.data['ventas'][index][0];
                var o = orden.toString(); 
                var estado = snapshot.data['ventas'][index][1];
                var e = estado.toString();
                var municipio = snapshot.data['ventas'][index][2];
                var m = municipio.toString();
                var parroquia = snapshot.data['ventas'][index][3];
                var p = parroquia.toString();
                var idorden = snapshot.data['ventas'][index][5];
                //return ListTile( title: Text(i));
                if (snapshot.data['ventas'][index][4] == 1) {
                return Slidable(key: ValueKey(index),
                  actionPane: SlidableBehindActionPane(),
                  secondaryActions: <Widget>[
                    IconSlideAction(
                      caption: 'Entregar',
                      color:  Colors.grey.shade300,
                      icon: Icons.map_rounded,
                      closeOnTap: false,
                      onTap: (){
                        var latitud = snapshot.data['ventas'][index][6];
                        var longitud = snapshot.data['ventas'][index][7];
                        //print(_direccion);
                        _openMap(latitud,longitud);
                        //Navigator.pushReplacementNamed(context, 'entrega_mapa', arguments: o);//paso id direccion de despacho
                        //Toast.show('Udate on $d', context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                      }
                    ),
                    IconSlideAction(
                      caption: 'codigo',
                      color:  Colors.grey.shade300,
                      icon: Icons.phonelink_lock,
                      closeOnTap: false,
                      onTap: (){
                        print(idorden);
                        Navigator.pushReplacementNamed(context, 'entrega_codigo', arguments: idorden);//paso id de orden
                        //Toast.show('Udate on $d', context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                      }
                    )
                  ], 
                  //dismissal: SlidableDismissal(child: SlidableDrawerDismissal(),),
                  child: ListTile(
                    title: Text('$o'),
                    subtitle: Text('$e'+', '+'$m'+', '+'$p'),
                    
                  ),
  
                );
                }else{
                return Slidable(key: ValueKey(index),
                  actionPane: SlidableBehindActionPane(),
                  secondaryActions: <Widget>[
                    IconSlideAction(
                      caption: 'codigo',
                      color:  Colors.grey.shade300,
                      icon: Icons.phonelink_lock,
                      closeOnTap: false,
                      onTap: (){
                        //print(idorden);
                        Navigator.pushReplacementNamed(context, 'entrega_codigo', arguments: idorden);//paso id de orden
                        //Toast.show('Udate on $d', context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                      }
                    )
                  ], 
                  //dismissal: SlidableDismissal(child: SlidableDrawerDismissal(),),
                  child: ListTile(
                    title: Text('$o'),
                    subtitle: Text('$e'+', '+'$m'+', '+'$p'),
                    
                  ),
                );

              }
              }
          );
          }else{
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
          //return Text('yeah papa sin miedo al exito');
        },
      ),
      
      /*ListView.builder(
        itemCount: 10,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Text("$index"),
          );
        },
      ), //_crearproductos(),*/


      bottomNavigationBar: _footer(context) 
    );
  }
  _footer(BuildContext context) {
    var _currentPage = 0;
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: HexColor('#0067A2'),
        primaryColor: Colors.yellowAccent,
      ),
      child: BottomNavigationBar(
        unselectedItemColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.house, size: 30),
            label: 'lista',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search, size: 30),
            label: 'mapa',
          ),
        ],
        currentIndex: _currentPage,
        onTap: (int index) {
          setState(() {
            _currentPage = index;
            switch (_currentPage) {
              case 0:
              Navigator.of(context)
              .pushNamedAndRemoveUntil('entrega_inicio', (Route<dynamic> route) => false);
                  //Navigator.pushReplacementNamed(context, 'entrega_inicio');
                  /*Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => EntregaInicio()),
                  );*/
                break;
                case 1:
                  /*Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => EntregaMapa()),
                  );*/
                break;
              default:
            }
          });
        },
      ),
    );
  }

  /*_openMap() async {
    // Android
    const url = 'geo:52.32,4.917';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      // iOS
      const url = 'http://maps.Apple.com/?ll=52.32,4.917';
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }
  }*/

  _openMap(a,b) async {
    // Android
    var url = 'https://www.google.com/maps/search/?api=1&query='+a+','+b+'';
    if (Platform.isIOS) {
      // iOS
      url = 'http://maps.Apple.com/?ll=52.32,4.917';
    }
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }



}