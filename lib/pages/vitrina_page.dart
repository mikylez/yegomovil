import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
//import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
//import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';
import 'package:loadmore/loadmore.dart';
import 'package:yegoapp/pages/buscar_vender_page.dart';
import 'package:yegoapp/pages/cambio_a_vendedor_page.dart';
//import 'package:yegoapp/pages/categorias_vender_page.dart';
import 'package:yegoapp/pages/cuenta_vendedor_page.dart';
import 'package:yegoapp/pages/ofertas_page.dart';
import 'package:yegoapp/pages/vender_page.dart';
//import 'package:yegoapp/pages/vitrina_vendedor_page.dart';
import 'package:yegoapp/providers/vitrina_provider.dart';
import 'package:yegoapp/widget/mydrawer.dart';
import 'buscar_page.dart';
import 'cart_page.dart';
//import 'categorias_page.dart';
import 'cuenta_page.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';
//probando
//import 'package:cached_network_image/cached_network_image.dart';
import 'package:transparent_image/transparent_image.dart';

var lengthh;

class Vitrina extends StatefulWidget {
  Vitrina({Key key}) : super(key: key);
  @override
  _VitrinaState createState() => _VitrinaState();
}

class _VitrinaState extends State<Vitrina> {
  final prefs = PreferenciasUsuario();
  int get count => list.length;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<dynamic> list = [];
  var page = 1;
  final rutaimg = 'http://157.245.82.111/';
  final f = NumberFormat("#,##0.00", "en_US");
  var numero;
  var numero1;
  var cantidad;
  var maxPorductos;
  final vitrinaprovider = VitrinaProvider();

  void initState() {
    super.initState();
    product();
    vitrinaprovider.getCarritoVitrina().then((value) {
      setState(() {
        lengthh = value;
      });
    });
    //list.addAll(List.generate(30, (v) => v));
  }

  product() {
    vitrinaprovider.productos(page).then((value) => {
          setState(() {
            list = value['Inventories'];
            page += 1;
            maxPorductos = value['maxPorductos'].length;
          })
        });
  }

  // void load() {
  //   vitrinaprovider.productos(page).then((value) => {
  //     setState(() {
  //       page +=1;
  //       list.addAll(value['Inventories']);
  //       //list.addAll(List.generate(10, (v) => v));
  //     })
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: _scaffoldKey,
        drawer: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: HexColor('#0067A2').withOpacity(0.7),
          ),
          child: myDrawer(context),
        ),
        appBar: AppBar(
          backgroundColor: HexColor('#0067A2'),
          actions: [
            Container(
              margin: EdgeInsets.only(right: 80),
              child: ClipRRect(
                child: GestureDetector(
                  onTap: () => Navigator.pushNamed(context, 'vitrina'),
                  child: FadeInImage(
                    image: AssetImage('assets/img/4.png'),
                    placeholder: AssetImage('assets/img/no-image.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10, right: 15),
              child: GestureDetector(
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: <Widget>[
                    Icon(
                      Icons.shopping_cart,
                      size: 36.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: CircleAvatar(
                        radius: 8,
                        backgroundColor: Colors.red,
                        foregroundColor: Colors.white,
                        child: Text(
                          lengthh.toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                onTap: () {
                  if (lengthh == 0) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('Carrito Sin Productos'),
                        backgroundColor: Colors.red,
                      ),
                    );
                  } else {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => CartPage()),
                    );
                  }
                },
              ),
            )
          ],
          leading: IconButton(
            iconSize: 35,
            icon: Icon(Icons.menu),
            onPressed: () => _scaffoldKey.currentState.openDrawer(),
          ),
        ),
        body: Center(
          child: list.length == 0
              ? CircularProgressIndicator()
              : Container(
                  child: RefreshIndicator(
                    child: LoadMore(
                      isFinish: count >= maxPorductos,
                      onLoadMore: _loadMore,
                      child: ListView.builder(
                        itemBuilder: (BuildContext context, int index) {
                          if (list[index]["oferta"] == 1) {
                            numero = (list[index]["precioViejo"]);
                            numero1 = (list[index]["price_unidad"]);
                          } else {
                            numero = (list[index]["price_unidad"]);
                          }
                          var i = double.parse(numero);
                          numero = f.format(i);
                          cantidad = (list[index]["cantidad"]).toString();
                          if (list[index]["cantidad"] > 0 &&
                              list[index]["estatus"] == 1) {
                            return Container(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40.0),
                                ),
                                elevation: 5,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Container(
                                        padding: EdgeInsets.all(5),
                                        child: Center(
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(40),
                                            child: Column(
                                              children: [
                                                GestureDetector(
                                                  child:
                                                      FadeInImage.memoryNetwork(
                                                    placeholder:
                                                        kTransparentImage,
                                                    height: 180,
                                                    width: 180,
                                                    image: ("$rutaimg" +
                                                        list[index]["photo"]),
                                                  ),
                                                  //   /*CachedNetworkImage(
                                                  //     imageUrl:("$rutaimg" +
                                                  //               snapshot.data["Inventories"]
                                                  //               [index]["photo"]),//"http://via.placeholder.com/350x150",
                                                  //     //placeholder: (context, url) => CircularProgressIndicator(),
                                                  //     progressIndicatorBuilder: (context, url, downloadProgress) =>
                                                  //     CircularProgressIndicator(value: downloadProgress.progress),
                                                  //     errorWidget: (context, url, error) => Icon(Icons.error)
                                                  //     //errorWidget: (context, url, error) => Icon(Icons.error),
                                                  //   ),*/
                                                  //   /*FadeInImage(
                                                  //     height: 180,
                                                  //     width: 180,
                                                  //     image: NetworkImage("$rutaimg" +
                                                  //         snapshot.data["Inventories"]
                                                  //             [index]["photo"]),
                                                  //     placeholder: AssetImage(
                                                  //         'assets/img/loading.gif'),
                                                  //     fit: BoxFit.fill,
                                                  //   ),*/
                                                  onTap: () =>
                                                      Navigator.pushNamed(
                                                          context,
                                                          'detalle_producto',
                                                          arguments:
                                                              list[index]),
                                                ),
                                                Container(
                                                  child: Column(
                                                    children: [
                                                      Text(
                                                        list[index]["name"],
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 18,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                      if (list[index]
                                                              ["oferta"] ==
                                                          1) ...[
                                                        Text(
                                                          "Descuento del" +
                                                              " " +
                                                              list[index][
                                                                  "porcentaje"] +
                                                              " " +
                                                              "\%",
                                                          style: TextStyle(
                                                              color: Colors.red,
                                                              fontSize: 17,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Text(
                                                              numero +
                                                                  " " +
                                                                  "\$--",
                                                              style: TextStyle(
                                                                  decoration:
                                                                      TextDecoration
                                                                          .lineThrough,
                                                                  color: Colors
                                                                      .blue,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                            Text(
                                                              numero1 +
                                                                  " " +
                                                                  "\$--" +
                                                                  cantidad +
                                                                  " Unidades Disponibles",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .blue,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            )
                                                          ],
                                                        ),
                                                      ] else ...[
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Text(
                                                              numero +
                                                                  " " +
                                                                  "\$--" +
                                                                  cantidad +
                                                                  " Unidades Disponibles",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .blue,
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            )
                                                          ],
                                                        ),
                                                      ]
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ))
                                  ],
                                ),
                              ),
                            );
                          } else {
                            return Container();
                          }
                          /* return Container(
                    padding: EdgeInsets.all(5),
                    height: 300,
                    child: Center(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(40),
                        child: Column(
                          children: [
                            GestureDetector(
                              child: FadeInImage(
                                height: 180,
                                image: NetworkImage("$rutaimg"+snapshot.data["Inventories"][index]["photo"]),
                                placeholder: AssetImage('assets/img/loading.gif'),
                                fit: BoxFit.cover,
                              ),
                              onTap: ()=>Navigator.pushNamed(context, 'detalle_producto',arguments: snapshot.data["Inventories"][index]),
                            ),
                              Container(
                                child: ListTile(
                                  title: Text(snapshot.data["Inventories"][index]["name"],style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.bold),
                                    //overflow: TextOverflow.ellipsis,
                                  ),
                                  subtitle: Text(snapshot.data["Inventories"][index]["price"],style: TextStyle(color: Colors.blue, fontSize: 18)),
                                ),
                              ),
                            Expanded(
                              child: Container(
                                width: double.infinity,
                                child: ElevatedButton(
                                  child: Text(
                                      'Agregar',
                                    style: TextStyle(fontSize: 18),
                                  ),
                                  onPressed: () {},
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  );*/
                        },
                        itemCount: count,
                      ),
                      whenEmptyLoad: false,
                      delegate: DefaultLoadMoreDelegate(),
                      textBuilder: DefaultLoadMoreTextBuilder.english,
                    ),
                    onRefresh: _refresh,
                  ),
                ),
        ),
        bottomNavigationBar: _footer(context));
  }

  Future<bool> _loadMore() async {
    //await Future.delayed(Duration(seconds: 0, milliseconds: 5000));
    await vitrinaprovider.productos(page).then((value) => {
          setState(() {
            page += 1;
            list.addAll(value['Inventories']);
            //list.addAll(List.generate(10, (v) => v));
          })
        });
    return true;
  }

  Future<void> _refresh() async {
    //await Future.delayed(Duration(seconds: 0, milliseconds: 5000));
    list.clear();
    page = 1;
    //load();
    await vitrinaprovider.productos(page).then((value) => {
          setState(() {
            page += 1;
            list.addAll(value['Inventories']);
            //list.addAll(List.generate(10, (v) => v));
          })
        });
  }

  _footer(BuildContext context) {
    var idtipo = prefs.idTipo;
    var _currentPage = 0;
    if (idtipo == 1) {
      return Theme(
        data: Theme.of(context).copyWith(
          canvasColor: HexColor('#0067A2'),
          primaryColor: Colors.yellowAccent,
        ),
        child: BottomNavigationBar(
          unselectedItemColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.house, size: 30),
              label: 'Comprar',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search, size: 30),
              label: 'Buscar',
            ),
            /*BottomNavigationBarItem(
                icon: Icon(Icons.category, size: 30), label: 'Categorías'),*/
            BottomNavigationBarItem(
                icon: Icon(Icons.account_circle, size: 30), label: 'Cuenta'),
          ],
          currentIndex: _currentPage,
          onTap: (int index) {
            setState(() {
              _currentPage = index;
              switch (_currentPage) {
                case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Vitrina()),
                  );
                  break;
                case 1:
                  showSearch(context: context, delegate: DataSearch());
                  break;
                case 2:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Cuenta()),
                  );
                  break;
                default:
              }
            });
          },
        ),
      );
    }
    if (idtipo == 2) {
      return Theme(
        data: Theme.of(context).copyWith(
          canvasColor: HexColor('#0067A2'),
          primaryColor: Colors.yellowAccent,
        ),
        child: BottomNavigationBar(
          unselectedItemColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.house, size: 30),
              label: 'Comprar',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search, size: 30),
              label: 'Buscar',
            ),
            /*BottomNavigationBarItem(
                icon: Icon(Icons.category, size: 30), label: 'Categorías'),*/
            BottomNavigationBarItem(
                icon: Icon(Icons.credit_card_rounded, size: 30),
                label: 'Vender'),
            BottomNavigationBarItem(
                icon: Icon(Icons.card_giftcard, size: 30), label: 'Ofertas'),
            BottomNavigationBarItem(
                icon: Icon(Icons.account_circle, size: 30), label: 'Cuenta'),
          ],
          currentIndex: _currentPage,
          onTap: (int index) {
            setState(() {
              _currentPage = index;
              switch (_currentPage) {
                case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Vitrina()),
                  );
                  break;
                case 1:
                  showSearch(context: context, delegate: DataSearchVender());
                  break;
                case 2:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Vender()),
                  );
                  break;
                case 3:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Ofertas()),
                  );
                  break;
                case 4:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CuentaVendedor()),
                  );
                  break;
                default:
              }
            });
          },
        ),
      );
    }
  }

  // _crearproductos() {
  //   final rutaimg = 'http://157.245.82.111/';
  //   final f = NumberFormat("#,##0.00", "en_US");
  //   var numero;
  //   var cantidad;
  //    Container(
  //       child: RefreshIndicator(
  //         child: LoadMore(
  //           isFinish: count >= 107,////////////arreglar a
  //           onLoadMore: _loadMore,
  //           child: ListView.builder(
  //             itemBuilder: (BuildContext context, int index) {
  //                   return Container(
  //                 child: Text('fdfd'),
  //                 height: 40.0,
  //                 alignment: Alignment.center,
  //               );
  //             },
  //             itemCount: count,
  //           ),
  //           whenEmptyLoad: false,
  //           delegate: DefaultLoadMoreDelegate(),
  //           textBuilder: DefaultLoadMoreTextBuilder.chinese,
  //         ),
  //         onRefresh: _refresh,
  //       ),
  //     );
  // }
  /*_swiperTarjetas(BuildContext context) {
    final rutaimg = 'http://157.245.82.111/';
    final _screenSize = MediaQuery.of(context).size;
    return FutureBuilder(
      future: vitrinaprovider.productos(),
      builder: (BuildContext context, AsyncSnapshot snapshot){
        if (snapshot.hasData) {
          return Container(
          padding: EdgeInsets.only(top: 20),
          child: Swiper(
            layout: SwiperLayout.STACK,
            itemWidth: _screenSize.width * 0.7,
            itemHeight: _screenSize.height * 0.3,
            itemBuilder: (BuildContext context, index) {
              return Hero(
                tag: 1,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => DetalleProductoPage()),
                      );
                    },
                    child: FadeInImage(
                      image: NetworkImage("$rutaimg"+snapshot.data["Inventories"][index]["photo"]),
                      placeholder: AssetImage('assets/img/loading.gif'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              );
            },
            itemCount: snapshot.data["Inventories"].length,
          ),
        );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );

  }*/

  Drawer drawer() {
    return Drawer(
      child: ListView(
        children: <Widget>[
          Container(
            child: DrawerHeader(
              child: CircleAvatar(
                child: Icon(
                  Icons.add_a_photo,
                  color: Colors.white,
                  size: 55.0,
                  /*Text('YEGO',
                      style:
                          TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),*/
                ),
              ),
            ),
            //color: HexColor('#0067A2'),
          ),
          Container(
            //color: HexColor('#0067A2'),
            child: Column(children: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CambioAVendedor()),
                  );
                },
                child: Center(
                  child: Text(
                    "¿QUIERES SER YEGO VENDEDOR?",
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
              ),
            ]),
          )
        ],
      ),
    );
  }
}
