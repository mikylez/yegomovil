import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:yegoapp/bloc/provider.dart';
import 'package:yegoapp/providers/user_provider.dart';
import 'package:yegoapp/utils/utils.dart';

class LoginPage extends StatelessWidget {
  //const LoginPage({Key key}) : super(key: key);
  final usuarioProvider = new UsuarioProvider();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        _crearFondo(context),
        _loginForm(context),
      ],
    ));
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final fondoLogin = Container(
      color: HexColor("0067A2"),
      width: double.infinity,
      height: size.height,
      child: Stack(
          /* alignment: Alignment.center,
        children: <Widget>[
          Positioned(
              top: 0,
              left: 0,
              child: Image.asset(
                "assets/img/Login1.png",
                width: size.width * 0.5,
              )),
          Positioned(
              bottom: 0,
              right: 0,
              child: Image.asset(
                "assets/img/Login2.png",
                width: size.width * 0.5,
              )),
        ],*/
          ),
    );
    return Stack(children: <Widget>[
      fondoLogin,
      //logo
      //Positioned(child: Image(image: image, ), top : 90, left: 140,),
    ]);
  }

  Widget _loginForm(BuildContext context) {
    final bloc = Provider.of(context);
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 0,
            ),
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  "assets/img/logo_yego.png",
                ),
                Text('Ingreso',
                    style: TextStyle(
                        fontSize: 35.0,
                        color: HexColor("#FFFFFF"),
                        fontWeight: FontWeight.bold)),
                SizedBox(
                  height: 15,
                ),
                _crearEmail(bloc),
                SizedBox(
                  height: 15,
                ),
                _crearPassword(bloc),
                SizedBox(
                  height: 31,
                ),
                _crearBoton(bloc),
                SizedBox(
                  height: 15,
                ),
                /*_crearBotonRegistro(bloc),
                SizedBox(
                  height: 15,
                ),
                Text(' Olvido la Contraseña?',
                    style: TextStyle(
                        color: HexColor("FFFFFF"),
                        fontWeight: FontWeight.bold,
                        fontSize: 15)),
                SizedBox(
                  height: 100.0,
                )*/
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _crearEmail(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          margin: EdgeInsets.only(left: 15, right: 15),
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          decoration: BoxDecoration(
            color: HexColor("FFFFFF").withOpacity(1),
            borderRadius: BorderRadius.circular(25),
          ),
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              icon: Icon(
                Icons.mail_outlined,
                color: HexColor("0067A2"),
              ),
              hintText: 'ejemplo@correo.com',
              labelText: 'Correo Electronico',
              //counterText: snapshot.data,
              errorText: snapshot.error,
              border: InputBorder.none,
            ),
            onChanged: bloc.changeEmail,
          ),
        );
      },
    );
  }

  Widget _crearPassword(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          margin: EdgeInsets.only(left: 15, right: 15),
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          decoration: BoxDecoration(
            color: HexColor("FFFFFF").withOpacity(1),
            borderRadius: BorderRadius.circular(25),
          ),
          child: TextField(
            //keyboardType: TextInputType.emailAddress,
            obscureText: true,
            decoration: InputDecoration(
              icon: Icon(
                Icons.lock_outline,
                color: HexColor("#0067A2"),
              ),
              hintText: '**********',
              labelText: 'Contraseña',
              //counterText: snapshot.data,
              errorText: snapshot.error,
              border: InputBorder.none,
            ),
            onChanged: bloc.changePassword,
          ),
        );
      },
    );
  }

  Widget _crearBoton(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return ElevatedButton(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 60.0, vertical: 20.0),
            child: Text(
              'Ingresar',
              style: TextStyle(
                  color: HexColor("FFFFFF"),
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
          ),
          style: ButtonStyle(
              elevation: MaterialStateProperty.all<double>(0.0),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0)))),
          onPressed: snapshot.hasData ? () => _login(bloc, context) : null,
        );
      },
    );
  }

  /*Widget _crearBotonRegistro(LoginBloc bloc) {
    // para registros q pao
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return ElevatedButton(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 20.0),
            child: Text(
              'Registrarse',
              style: TextStyle(
                  color: HexColor("FFFFFF"),
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
          ),
          style: ButtonStyle(
              elevation: MaterialStateProperty.all<double>(0.0),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0)))),
          onPressed: () {
            Navigator.pushNamed(context, 'registar');
          },
        );
      },
    );
  }*/

  _login(LoginBloc bloc, BuildContext context) async {
    //print('=================');
    //print('email: ${ bloc.email}');
    //print('password: ${bloc.password}');
    //print('=================');
    Map info = await usuarioProvider.login(bloc.email, bloc.password);

    if (info['ok']) {
      switch (info['id_tipo']) {
        case 1:
          Navigator.pushReplacementNamed(context, 'vitrina');
          break;
        case 2:
          Navigator.pushReplacementNamed(context, 'vitrina');
          break;
        case 7:
          Navigator.pushReplacementNamed(context, 'entrega_inicio');
          break;
        case 9:
          Navigator.pushReplacementNamed(context, 'vitrina_distribuidor');
          break;
        default:
      }
      //Navigator.pushReplacementNamed(context, 'vitrina');

    } else {
      mostarAlerta(context, info['mensaje']);
    }

    //Navigator.pushNamed(context, 'vitrina');
  }
}


/*Version 1.0
import 'package:flutter/material.dart';
import 'package:yegoapp/bloc/provider.dart';
import 'package:yegoapp/providers/user_provider.dart';
import 'package:yegoapp/utils/utils.dart';

class LoginPage extends StatelessWidget {
  //const LoginPage({Key key}) : super(key: key);
  final usuarioProvider = new UsuarioProvider();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _crearFondo(context),
          _loginForm(context),
        ],
      ),
    );
  }

  Widget _crearFondo(BuildContext context) {
    //final _size = MediaQuery.of(context).size;

    final fondoLogin = Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Color.fromRGBO(0, 103, 162, 1.0),
        Color.fromRGBO(23, 111, 176, 1.0)
      ])),
    );

    final logo = Container(
      child : Image.asset(
        'assets/img/4.png',
        height: _size.height*0.4,
        width: _size.width*0.95,  
        fit: BoxFit.fitWidth,
        ),
    );

    return Stack(children: <Widget>[
      fondoLogin,
      //logo
      //Positioned(child: Image(image: image, ), top : 90, left: 140,),
    ]);
  }

  Widget _loginForm(BuildContext context) {
    final bloc = Provider.of(context);
    final _size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      //controller: controller,
      child: Column(
        children: <Widget>[
          SafeArea(
              child: Container(
            height: 200.0,
          )),
          Container(
              width: _size.width * 0.85,
              margin: EdgeInsets.symmetric(vertical: 30.0),
              padding: EdgeInsets.symmetric(vertical: 50.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.black26,
                        blurRadius: 3.0,
                        offset: Offset(0.0, 5.0),
                        spreadRadius: 3.0)
                  ]),
              child: Column(children: <Widget>[
                Text('ingreso', style: TextStyle(fontSize: 20.0)),
                SizedBox(height: 20.0),
                _crearEmail(bloc),
                SizedBox(height: 20.0),
                _crearPassword(bloc),
                SizedBox(height: 20.0),
                _crearBoton(bloc),
              ])),
          Text(' Olvido la Contraseña?', style: TextStyle(color: Colors.white)),
          SizedBox(
            height: 100.0,
          )
        ],
      ),
    );
  }

  Widget _crearEmail(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              icon: Icon(
                Icons.mail_outlined,
                color: Colors.blue,
              ),
              hintText: 'ejemplo@correo.com',
              labelText: 'Correo Electronico',
              counterText: snapshot.data,
              errorText: snapshot.error,
            ),
            onChanged: bloc.changeEmail,
          ),
        );
      },
    );
  }

  Widget _crearPassword(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            //keyboardType: TextInputType.emailAddress,
            obscureText: true,
            decoration: InputDecoration(
              icon: Icon(
                Icons.lock_outline,
                color: Colors.blue,
              ),
              //hintText: 'ejemplo@correo.com',
              labelText: 'Contraseña',
              counterText: snapshot.data,
              errorText: snapshot.error,
            ),
            onChanged: bloc.changePassword,
          ),
        );
      },
    );
  }

  Widget _crearBoton(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return ElevatedButton(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 20.0),
            child: Text('ingresar'),
          ),
          style: ButtonStyle(
              elevation: MaterialStateProperty.all<double>(0.0),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0)))),
          onPressed: snapshot.hasData ? () => _login(bloc, context) : null,
        );
      },
    );
  }

  _login(LoginBloc bloc, BuildContext context) async {
    //print('=================');
    //print('email: ${ bloc.email}');
    //print('password: ${bloc.password}');
    //print('=================');
    Map info = await usuarioProvider.login(bloc.email, bloc.password);

    if (info['ok']) {
      switch (info['id_tipo']) {
        case 1:
          Navigator.pushReplacementNamed(context, 'vitrina');
          break;
        case 2:
          Navigator.pushReplacementNamed(context, 'vitrina_vendedor');
          break;
        default:
      }
      //Navigator.pushReplacementNamed(context, 'vitrina');

    } else {
      mostarAlerta(context, info['mensaje']);
    }

    //Navigator.pushNamed(context, 'vitrina');
  }
}
*/
