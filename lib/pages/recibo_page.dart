import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:yegoapp/pages/vitrina_page.dart';

class ReciboPage extends StatefulWidget {
  @override
  _ReciboPage createState() => _ReciboPage();
}

class _ReciboPage extends State<ReciboPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _fondoAppRecibo(context),
          _reciboCompra(context),
        ],
      ),
    );
  }

  //Fondo o Appbar

  Widget _fondoAppRecibo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final fondoRecibo = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(color: HexColor("#0067A2")),
    );

    return Stack(
      children: <Widget>[
        fondoRecibo,
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon(
                Icons.shopping_cart,
                color: HexColor("#FFFFFF"),
                size: 100.0,
              ),
              SizedBox(
                height: 1.0,
                width: double.infinity,
              ),
              Text(
                "Gracias por su compra",
                style: TextStyle(
                    color: HexColor("#FFFFFF"),
                    fontWeight: FontWeight.bold,
                    fontSize: 25.0),
              )
            ],
          ),
        )
      ],
    );
  }

  //Contenedor que tiene los datos de la compra

  Widget _reciboCompra(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
              child: Container(
            height: 150,
          )),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 30.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
                color: HexColor("#FFFFFF"),
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: HexColor("#0067A2"),
                      blurRadius: 1.0,
                      offset: Offset(0.0, 2.0),
                      spreadRadius: 3.0)
                ]),
            child: Column(
              children: <Widget>[
                Text(
                  'RECIBO',
                  style: TextStyle(
                      color: HexColor("#0067A2"),
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0),
                ),
                _codigoDeBarras(),
                _datosDeCompra(),
              ],
            ),
          ),
          _botonVolverVitrina()
        ],
      ),
    );
  }

  //Acá se generará el Qr de la compra

  Widget _codigoDeBarras() {
    return Container(
      padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
      child: Column(
        children: <Widget>[
          Container(
            height: 150,
            width: 150,
            decoration: BoxDecoration(color: HexColor("#0067A2")),
          )
        ],
      ),
    );
  }

  //Datos de la compra, N_Orden, entre otros datos

  Widget _datosDeCompra() {
    return Container(
      child: Column(
        children: <Widget>[
          Text(''),
          SizedBox(height: 5.0),
          Text(''),
          Text(''),
          SizedBox(height: 5.0),
          Text(''),
          Text(''),
          SizedBox(height: 5.0),
          Text(''),
          SizedBox(
            height: 5.0,
          ),
        ],
      ),
    );
  }

  //Boton para volver a la vitrina

  Widget _botonVolverVitrina() {
    return Container(
      child: TextButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Vitrina()));
        },
        child: Container(
          width: 150.0,
          decoration: BoxDecoration(
            color: HexColor('#0067A2'),
            borderRadius: BorderRadius.circular(10),
          ),
          height: 50,
          child: Center(
            child: Text(
              "  VITRINA  ",
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
