
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:show_up_animation/show_up_animation.dart';
import 'package:yegoapp/models/validacion_formularios.dart';
import 'package:yegoapp/pages/vitrina_page.dart';
import 'package:yegoapp/providers/direcciones_provider.dart';
//import 'package:yegoapp/pages/vitrina_distribuidor.dart';
//import 'package:yegoapp/pages/vitrina_vendedor_page.dart';
import 'package:yegoapp/providers/vitrina_provider.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';
//import 'cart_vender_page.dart';

class RegistrarUsuario extends StatefulWidget {
  RegistrarUsuario({Key key}) : super(key: key);

  @override
  _RegistrarUsuarioState createState() => _RegistrarUsuarioState();
}

class _RegistrarUsuarioState extends State<RegistrarUsuario> {
  final prefs = PreferenciasUsuario();
  final vitrinaprovider =VitrinaProvider();
  final negociosProvider = Directionrepository();
  final validator = ValidacionFormulario();
  final _formulario =GlobalKey<FormState>();
  TextEditingController _inputnombrecomercio =new TextEditingController();
  TextEditingController _inputtelefonocomercio =new TextEditingController();
  TextEditingController _inputdocumentocomercio =new TextEditingController();
  TextEditingController _inputnombre =new TextEditingController();
  TextEditingController _inputBuscar =new TextEditingController();
  TextEditingController _inputapellido =new TextEditingController();
  TextEditingController _inputdocumento =new TextEditingController();
  TextEditingController _inputcorreo =new TextEditingController();
  TextEditingController _inputtelefono =new TextEditingController();
  TextEditingController _imputCredito =new TextEditingController();
  //TextEditingController _inputfecha =new TextEditingController();
  TextEditingController _inputdireccionfiscal =new TextEditingController();
  TextEditingController _inputdirecciondespacho =new TextEditingController();
  //int _radioValue = 2;
  int _radioValueContribuyente = 2;
  String _opcionSelecionadaNacionalidad ='V';
  List<String> _nacionalidad = ['V','E','J'];
  String _opcionSelecionadaNacionalidadcomercio ='J';

  var ndocumento;
  var sdocumento;
  var comercio;
  var snegocio;
  var _tiponegocio;
  bool activos = false;
  var _estadoFiscal;
  var _municipiosFiscal;
  var _parroquiasFiscal;
  var _estadoDespacho ;
  var _municipiosDespacho;
  var _parroquiasDespacho;
  //var _info;
  var datosDireccion;
  var lengthh;
  var datosNegocio;
  var _negocioelegido = 0;
  var dVisible = false;

  /*void _inputsexo(int value) {
    setState(() {
      _radioValue = value;
    });
  }*/
  void _inputcontribuyente(int value) {
    setState(() {
      _radioValueContribuyente = value;
    });
  }

  _loadEstadosMUnicipiosParroquia()async{
    var datosEstadosMunicipiosParroquia;
    datosEstadosMunicipiosParroquia =await vitrinaprovider.direccion();
    setState(() {
      datosDireccion =datosEstadosMunicipiosParroquia;
    });
  }

  _loadNegocios()async{
    var listaNegocios;
    listaNegocios =await negociosProvider.negocios();
    setState(() {
      datosNegocio =listaNegocios;
    });
  }

  void initState() {
    super.initState();
    _loadEstadosMUnicipiosParroquia();
    _loadNegocios();
 
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
          centerTitle: true,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                child: Text(
                  'Registrar Cliente',
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
              ),
            ],
          ),
          backgroundColor: HexColor('#0067A2'),
        ),
        body: Visibility(
        visible: true, 
        child: SingleChildScrollView(
          child: Form(
            key: _formulario,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  ShowUpAnimation(
                    //delayStart: Duration(seconds:1 ),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: _crearImputBuscar(),
                  ),
                  ShowUpAnimation(
                    animationDuration: Duration(seconds:2),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child: TextButton(
                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blue)),
                      child: Text('Buscar Usuario',style: TextStyle(color: Colors.white)),
                      onPressed: (){
                        if (_inputBuscar.text.length != 0) {
                          vitrinaprovider.informacionUsuarioVender(_inputBuscar.text).then((value) {
                            if (value["info"]!=null) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  content: Text('Documento ya esta registrado'),
                                  backgroundColor: Colors.red,
                                ),
                              ); 
                              }
                            else{
                              setState(() {
                              activos = true;
                              });
                            }
                          });
                        }else{
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text('Introducir Documento de Usuario'),
                              backgroundColor: Colors.red,
                            ),
                          );
                        }
                      },
                    ),
                  ),

                  //desde aca

                  //nuevos cambios


                  //hasta aca nuevos cambios
                  Visibility(
                  visible: activos,
                  child:Column(children: [
                    //AGREGANDO TITULO
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds:1 ),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: Text(
                      'Datos Negocio',
                        style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  //HASTA AQUI
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:_negocios(),
                  ),
                  Visibility(//esto es para unidades
                      visible: dVisible,
                      child:Column(children: [
                        SizedBox( height: 15),
                        ShowUpAnimation(
                        //delayStart: Duration(seconds: 6),
                        animationDuration: Duration(seconds: 1),
                        curve: Curves.bounceInOut,
                        direction: Direction.horizontal,
                        offset: 0.5,
                        child:_crearImputNombreComercio(),
                        ),
                        SizedBox( height: 15),
                        ShowUpAnimation(
                        //delayStart: Duration(seconds: 6),
                        animationDuration: Duration(seconds: 1),
                        curve: Curves.bounceInOut,
                        direction: Direction.horizontal,
                        offset: 0.5,
                        child:_crearImputTefonoComercio(),
                        ),
                        SizedBox( height: 15),
                        ShowUpAnimation(
                        //delayStart: Duration(seconds: 6),
                        animationDuration: Duration(seconds: 1),
                        curve: Curves.bounceInOut,
                        direction: Direction.horizontal,
                        offset: 0.5,
                        child:_crearImputDocumentoComercio(),
                        ),
                      ],)
                  ),
                  //AGREGANDO TITULO
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds:1 ),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: Text(
                      'Datos Personales',
                        style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  //HASTA AQUI
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds:1 ),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: _crearImputNombre(),
                  ),
                  SizedBox( height: 15),
                    ShowUpAnimation(
                    //delayStart: Duration(seconds: 2),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child: _crearImputApellido(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 2),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: _crearImputDocumento(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 3),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:  _crearImputCorreo(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 4),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:  _crearImputTefono(),
                  ),
                  //AGREGANDO TITULO
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds:1 ),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: Text(
                      'Direccion Fiscal',
                        style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  //HASTA AQUI
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 4),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:  _crearImputCredito(),
                  ),
                  SizedBox( height: 15),
                  /* ShowUpAnimation(
                    //delayStart: Duration(seconds: 5),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:  _crearImputFecha(),
                  ),*/
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:_estados(),
                  ),
                  SizedBox( height: 15),
                    ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:_municipio(),
                  ),
                    SizedBox( height: 15),
                    ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:_parroquia(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:  _crearImputDireccionfiscal(),
                  ),
                  //AGREGANDO TITULO
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds:1 ),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: Text(
                      'Direccion Despacho',
                        style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  //HASTA AQUI
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:_estadosDespacho(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:_municipioDespacho(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:_parroquiaDespacho(),
                  ),
                  SizedBox( height: 15),
                    ShowUpAnimation(
                    //delayStart: Duration(seconds: 7),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:  _crearImputDirecciondespacho(),
                  ),
                  SizedBox( height: 15),
                 /* ShowUpAnimation(
                    //delayStart: Duration(seconds: 8),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: Row(
                      children: [
                        Icon(Icons.person,size: 40,color: Colors.grey,),
                        Radio(
                          value: 0,
                          groupValue: _radioValue,
                          activeColor: Colors.blue,
                          onChanged: (value){
                           _inputsexo(value);
                          },
                        ),
                        Text('Hombre'),
                        Radio(
                          value: 1,
                          groupValue: _radioValue,
                          activeColor: Colors.blue,
                          onChanged: (value){
                            _inputsexo(value);
                          },
                        ),
                        Text('Mujer'),
                      ],
                    ),
                  ),*/


                  Visibility(//esto es para unidades
                      visible: false,
                      child:
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 8),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: Row(
                      children: [
                        Icon(Icons.person,size: 40,color: Colors.grey,),
                        Text('Contribuyente Especial'),
                        Radio(
                          value: 1,
                          groupValue: _radioValueContribuyente,
                          activeColor: Colors.blue,
                          onChanged: (value){
                           _inputcontribuyente(value);
                          },
                        ),
                        Text('Si'),
                        Radio(
                          value: 0,
                          groupValue: _radioValueContribuyente,
                          activeColor: Colors.blue,
                          onChanged: (value){
                            _inputcontribuyente(value);
                          },
                        ),
                        Text('No'),
                      ],
                    ),
                  ),
                  ),



                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ShowUpAnimation(
                        animationDuration: Duration(seconds:2),
                        curve: Curves.bounceInOut,
                        direction: Direction.horizontal,
                        offset: -0.5,
                        child: TextButton(
                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blue)),
                          child: Text('Guardar Cliente',style: TextStyle(color: Colors.white)),
                          onPressed: (){
                            if (_formulario.currentState.validate()){
                              _radioValueContribuyente=0;
                              if(_radioValueContribuyente==2){
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text('Selecccionar Si es Contribuyente Especial'),
                                    backgroundColor: Colors.red,
                                  ),
                                );
                              }else{
                                vitrinaprovider.registrarCliente(_inputBuscar.text,_inputcorreo.text,_inputnombre.text,_inputapellido.text,
                                _inputtelefono.text,_opcionSelecionadaNacionalidad,_radioValueContribuyente,_estadoFiscal,
                                _municipiosFiscal,_parroquiasFiscal,_estadoDespacho,_municipiosDespacho,_parroquiasDespacho,
                                _inputdireccionfiscal.text,_inputdirecciondespacho.text,_inputdocumento.text,
                                _imputCredito.text,
                                _inputdocumentocomercio.text,
                                _inputnombrecomercio.text,
                                _inputtelefonocomercio.text,
                                _opcionSelecionadaNacionalidadcomercio,
                                _negocioelegido).then((value){
                                  if (value["error"] == true) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text('Cliente ya esta Registrado'),
                                        backgroundColor: Colors.red,
                                      ),
                                    );
                                  } else {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => Vitrina()),
                                    );
                                    
                                      ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text('Cliente Guardado'),
                                        backgroundColor: Colors.green,
                                      ),
                                    );
                                  }
                                });
                              }
                            }
                          },
                        ),
                      )
                    ],
                  )
                ],))
                ]
              ),
            ),
          ),
        ),)
    );
  }
  _crearImputBuscar() {
    return TextField(
      inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
      autofocus: false,
      controller: _inputBuscar,
      textCapitalization: TextCapitalization.sentences,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Buscar Documento del Usuario',
       labelText: 'Buscar Documento del Usuario',
       suffixIcon: Icon(Icons.search,size: 30,),
       icon: Icon(Icons.search,size:40,),
      ),
    );
  }
  _crearImputNombre() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: _inputnombre,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Nombre',
       labelText: 'Nombre',
       suffixIcon: Icon(Icons.account_circle,size: 30,),
       icon: Icon(Icons.account_circle,size:40,),
      ),
      validator: validator.validateName,
    );
  }
  _crearImputApellido() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: _inputapellido,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Apellido',
       labelText: 'Apellido',
       suffixIcon: Icon(Icons.account_circle,size: 30,),
       icon: Icon(Icons.account_circle,size:40,),
      ),
         validator: validator.validateApellido,
    );
  }
  _crearImputCorreo() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.emailAddress,
      controller: _inputcorreo,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Correo',
       labelText: 'Correo',
       suffixIcon: Icon(Icons.email,size: 30,),
       icon: Icon(Icons.email,size:40,),
      ),
        validator: validator.validateEmail,
    );
  }

  _crearImputTefono() {
      return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.number,
      controller: _inputtelefono,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Telefono',
       labelText: ' Telefono',
       suffixIcon: Icon(Icons.phone,size: 30,),
       icon: Icon(Icons.phone,size:40,),
      ),
        inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
          validator: (value){
        if (value.isEmpty) {
          return 'El Telefono es Obligatorio';
        }
        return null;
      },
    );
  }
  _crearImputCredito() {
    if (prefs.idTipo == 9) {
      return TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        autofocus: false,
        keyboardType: TextInputType.number,
        controller: _imputCredito,
        decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        hintText: 'Dias de Credito',
        labelText: 'Dias de Credito',
        suffixIcon: Icon(Icons.credit_card,size: 30,),
        icon: Icon(Icons.credit_card,size:40,),
        ),
          inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
            validator: (value){
          if (value.isEmpty) {
            return 'El Campo es Obligatorio';
          }
          return null;
        },
      );
    }
  }
    List<DropdownMenuItem<String>> getOpcionesDropdown(){
    List<DropdownMenuItem<String>> _lista= [];
    _nacionalidad.forEach((nacionalida) {
      _lista.add(DropdownMenuItem(
        child: Text(nacionalida),
        value: nacionalida,
      ));
    });
    return _lista;
  }

  _crearImputDocumento() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.number,
      controller: _inputdocumento,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Documento',
       labelText: ' Documento',
       prefixIcon:Container(
         padding: EdgeInsets.only(left: 15),
         child: DropdownButton(
          underline: Container(
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide.none)
            )
          ),
          value: _opcionSelecionadaNacionalidad,
          items: getOpcionesDropdown(),
          onChanged: (opt){
            setState(() {
              _opcionSelecionadaNacionalidad =opt;
            });
          },
        ),
       ),
       icon:Icon(Icons.account_box,size: 40,) ,
       suffixIcon: Icon(Icons.account_box,size: 30,),
      ),
      inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
          validator: (value){
        if (value.isEmpty) {
          return 'El Documento es Obligatorio';
        }
        return null;
      },
    );
  }

  /*_crearImputFecha() {
    return TextFormField(
    autofocus: false,
    controller: _inputfecha,
    decoration: InputDecoration(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      hintText: 'Fecha de Nacimiento',
      labelText: 'Fecha de Nacimiento',
      suffixIcon: Icon(Icons.calendar_today,size: 30),
      icon: Icon(Icons.calendar_today,size:40),
    ),
     onTap: (){
       FocusScope.of(context).requestFocus(new FocusNode());
       _selectDate(context);
     },
        validator: (value){
        if (value.isEmpty) {
          return 'La Fecha es Obligatoria';
        }
        return null;
      },
    );
  }*/

 /* _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(1920),
      lastDate:  new DateTime(2025),
    );
    if (picked!=null) {
      setState(() {
        String fecha = DateFormat('dd-MM-yyyy').format(picked);
        _inputfecha.text = fecha;
      });
    }
  }*/

  _estados(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_estadoFiscal,
          decoration: InputDecoration(
            labelText: 'Estado Direccion Fiscal',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[0]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
              value:map,
            );
          })?.toList() ?? [],
          onChanged: (value){
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _estadoFiscal=value;
            });
          },
        ),
      );
    }else{
      return CircularProgressIndicator();
    }     
  }

  _municipio(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_municipiosFiscal,
          decoration: InputDecoration(
            labelText: 'Municipio Direccion Fiscal',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[1]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
              value: map,
            );
          })?.toList() ?? [],
              onChanged: (value){
                FocusScope.of(context).requestFocus(new FocusNode());
                setState(() {
                  _municipiosFiscal=value;
                });
              },
        ),
      );
    }else{
      return CircularProgressIndicator();
    }  
  }

  _parroquia(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_parroquiasFiscal,
          decoration: InputDecoration(
            labelText: 'Parroquia',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[2]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
              value: map,
            );
          })?.toList() ?? [],
          onChanged: (value){
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _parroquiasFiscal=value;
            });
          },
        ),
      );
    }else{
      return CircularProgressIndicator();
    }      
  }
  _crearImputDireccionfiscal() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      maxLines: null,
      keyboardType: TextInputType.multiline,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      controller: _inputdireccionfiscal,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Direccion Fiscal',
       labelText: 'Direccion Fiscal',
       icon: Icon(Icons.location_on,size:40,),
       suffixIcon: Icon(Icons.location_on,size: 30,),
      ),
         //validator: validator.validateDireccionFiscal,
    );
  }

  _crearImputDirecciondespacho() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      maxLines: null,
      keyboardType: TextInputType.multiline,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      controller: _inputdirecciondespacho,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Direccion de Despacho',
       labelText: 'Direccion de Despacho',
       icon: Icon(Icons.location_on,size:40,),
       suffixIcon: Icon(Icons.location_on,size: 30,),
      ),
      //validator: validator.validateDireccionDespacho,
    );
  }

  _estadosDespacho(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_estadoDespacho,
          decoration: InputDecoration(
            labelText: 'Estado Direccion de Despacho',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[0]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
              value: map,
            );
          })?.toList() ?? [],
          onChanged: (value){
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _estadoDespacho=value;
            });
          },
        ),
          );
     }else{
       return CircularProgressIndicator();
     }
      
  }

  _municipioDespacho(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_municipiosDespacho,
          decoration: InputDecoration(
            labelText: 'Municipio Direccion de Despacho',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[1]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
              value: map,
            );
          })?.toList() ?? [],
          onChanged: (value){
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _municipiosDespacho=value;
            });
          },
        ),
      );
    }else{
      return CircularProgressIndicator();
    }  
  }

  _parroquiaDespacho(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_parroquiasDespacho,
          decoration: InputDecoration(
            labelText: 'Parroquia Direccion de Despacho',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[2]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
               value: map,
            );
          })?.toList() ?? [],
          onChanged: ( value){
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _parroquiasDespacho=value;
            });
          },
            ),
          );
    }else{
      return CircularProgressIndicator();
    }
  }

    _negocios(){
      if (datosDireccion!=null) {
        return Container( 
          child: DropdownButtonFormField(
            autofocus: false,
            value:_tiponegocio,
            decoration: InputDecoration(
              labelText: 'Tipo Negocio',
              icon:Icon(Icons.location_on,size: 40,),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
              ),
            ),
            items:datosNegocio[0]?.map<DropdownMenuItem>((map){
              return DropdownMenuItem(
                child: Text(map),
                value:map,
              );
            })?.toList() ?? [],
            onChanged: (value){
              FocusScope.of(context).requestFocus(new FocusNode());
              setState(() {
                _tiponegocio=value;
                _negocioelegido = datosNegocio[0].indexOf(value)+1;
                if(_negocioelegido!=1){
                  dVisible = true;
                }else{
                  dVisible = false;
                }
                print(_negocioelegido);
              });
            },
          ),
        );
      }else{
        return CircularProgressIndicator();
      }     
    }

     _crearImputNombreComercio() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: _inputnombrecomercio,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Nombre Comercio',
       labelText: 'Nombre Comercio',
       suffixIcon: Icon(Icons.account_circle,size: 30,),
       icon: Icon(Icons.account_circle,size:40,),
      ),
      validator: validator.validateName,
    );
  }

  _crearImputTefonoComercio() {
      return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.number,
      controller: _inputtelefonocomercio,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Telefono Comercio',
       labelText: ' Telefono Comercio',
       suffixIcon: Icon(Icons.phone,size: 30,),
       icon: Icon(Icons.phone,size:40,),
      ),
        inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
          /*validator: (value){
        if (value.isEmpty) {
          return 'El Telefono es Obligatorio';
        }
        return null;
      },*/
    );
  }
  
  _crearImputDocumentoComercio() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.number,
      controller: _inputdocumentocomercio,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Documento Comercio',
       labelText: 'Documento Comercio',
       prefixIcon:Container(
         padding: EdgeInsets.only(left: 15),
         child: DropdownButton(
          underline: Container(
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide.none)
            )
          ),
          value: _opcionSelecionadaNacionalidadcomercio,
          items: getOpcionesDropdown(),
          onChanged: (opt){
            setState(() {
              _opcionSelecionadaNacionalidadcomercio =opt;
            });
          },
        ),
       ),
       icon:Icon(Icons.account_box,size: 40,) ,
       suffixIcon: Icon(Icons.account_box,size: 30,),
      ),
      inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
          /*validator: (value){
        if (value.isEmpty) {
          return 'El Documento es Obligatorio';
        }
        return null;
      },*/
    );
  }
}