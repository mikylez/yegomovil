import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:yegoapp/pages/vender_page.dart';
import 'package:yegoapp/pages/vitrina_vendedor_page.dart';
import 'package:yegoapp/providers/vitrina_provider.dart';
import 'buscar_vender_page.dart';
import 'cart_vender_page.dart';
import 'cuenta_vendedor_page.dart';

var lengthh;

class CategoriasVender extends StatefulWidget {
  @override
  _CategoriasVenderState createState() => _CategoriasVenderState();
}

class _CategoriasVenderState extends State<CategoriasVender> {

   final vitrinaprovider = VitrinaProvider();
  final rutaimg = 'http://157.245.82.111/';
   void initState() { 
    super.initState();
    vitrinaprovider.getCarritoVitrina().then((value){
      setState(() {
        lengthh =value;
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset('assets/img/4.png'),
              Container(
                padding: EdgeInsets.only(right: 30),
                child: Text(
                  'Categorias',
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
              ),
            Container(
              margin: EdgeInsets.only(right: 15),
              child: GestureDetector(
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: <Widget>[
                    Icon(
                      Icons.shopping_cart,
                      size: 36.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: CircleAvatar(
                        radius: 8,
                        backgroundColor: Colors.red,
                        foregroundColor: Colors.white,
                        child: Text(
                          lengthh.toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                onTap: (){
                  if (lengthh==0) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('Carrito Sin Productos'),
                        backgroundColor: Colors.red,
                      ),
                    );
                  }else{
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => CartVender()),
                    );
                  }
                },
              ),
            )
            ],
          ),
          backgroundColor: HexColor('#0067A2'),
        ),
        bottomNavigationBar: _footer(context),
      body: FutureBuilder(
        // future: vitrinaprovider.productos(),
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if (snapshot.hasData){
            return Container(
              child: GridView.builder(
                itemCount: snapshot.data["Categorias"].length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 1,
                  crossAxisSpacing: 4,
                  mainAxisSpacing: 4,  
                  childAspectRatio: 2
                ),
                itemBuilder: (BuildContext context, int index){
                return SingleChildScrollView(
                  child: Center(
                    child: Column(
                      children: [
                        Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                          margin: EdgeInsets.all(20),
                          elevation: 10,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Column(
                              children: [
                                Text(snapshot.data["Categorias"][index]["nombre_categoria"],style: TextStyle(color: Colors.blue, fontSize: 20,)),
                                FadeInImage(
                                  image: AssetImage('assets/img/alargada.jpg'),
                                  height: 150,
                                  width: 400,
                                  placeholder: AssetImage('assets/img/loading.gif'),
                                  fit: BoxFit.cover,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
                },
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      )
    );
  }

  _footer(BuildContext context) {
    var _currentPage = 2;
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: HexColor('#0067A2'),
        primaryColor: Colors.yellowAccent,
      ),
      child: BottomNavigationBar(
        unselectedItemColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.house, size: 30),
            label: 'Comprar',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search, size: 30),
            label: 'Buscar',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.category, size: 30), 
            label: 'Categorías'
          ),
             BottomNavigationBarItem(
            icon: Icon(Icons.credit_card_rounded, size: 30), 
            label: 'Vender'
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle, size: 30), 
            label: 'Cuenta'
          ),
        ],
        currentIndex: _currentPage,
        onTap: (int index) {
          setState(() {
            _currentPage = index;
            switch (_currentPage) {
              case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => VitrinaVendedor()),
                  );
                break;
              case 1:
                showSearch(context: context, delegate: DataSearchVender());
                break;
              case 2:
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CategoriasVender()),
                );
                break;
              case 3 :
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Vender()),
                );
                break;
              case 4:
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CuentaVendedor()),
                );
                break;
              default:
            }
          });
        },
      ),
    );
  }
}