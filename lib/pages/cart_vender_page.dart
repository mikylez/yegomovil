import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:yegoapp/carrito_preferences/carrito_preferences.dart';
import 'package:yegoapp/pages/vender_page.dart';
import 'package:yegoapp/pages/vitrina_distribuidor.dart';
import 'package:yegoapp/pages/vitrina_vendedor_page.dart';
import 'package:yegoapp/providers/vitrina_provider.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';
//import 'buscar_usuario_vender_page.dart';

class CartVender extends StatefulWidget {
  @override
  _CartVenderState createState() => _CartVenderState();
}

class _CartVenderState extends State<CartVender> {
  final prefs = PreferenciasUsuario();
  final vitrinaprovider = VitrinaProvider();
  final cart = CarritoPreferences();
  var flete;

  @override
  Widget build(BuildContext context) {
    final rutaimg = 'http://157.245.82.111/';
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              margin: EdgeInsets.only(right: 30),
              child: Text(
                'Carrito',
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
            ),
            Image.asset('assets/img/4.png'),
          ],
        ),
        backgroundColor: HexColor('#0067A2'),
      ),
      body: Container(
          padding: EdgeInsets.all(5),
          child: StaggeredGridView.countBuilder(
            staggeredTileBuilder: (int index) => StaggeredTile.fit(2),
            itemCount: jsonDecode(cart.carrito).length,
            crossAxisCount: 2,
            mainAxisSpacing: 5,
            itemBuilder: (BuildContext context, int index) {
              if (jsonDecode(cart.carrito)[index]['unidad'] == 1) {
                return Container(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40.0),
                    ),
                    elevation: 5,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          //padding: EdgeInsets.all(10),
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(15),
                                  child: FadeInImage(
                                    width: 80,
                                    height: 80,
                                    image: NetworkImage("$rutaimg" +
                                        jsonDecode(cart.carrito)[index]
                                            ['photo']),
                                    placeholder:
                                        AssetImage('assets/img/loading.gif'),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                width: 150,
                                child: Column(
                                  children: [
                                    Container(
                                      child: Center(
                                        child: Text(
                                            jsonDecode(cart.carrito)[index]
                                                ['name']),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 15),
                                      child: Center(
                                        child: Text(
                                            jsonDecode(cart.carrito)[index]
                                                    ["price_unidad"] +
                                                ' \$ C/U'),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                //margin: EdgeInsets.only(left: 10),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    IconButton(
                                      icon: Icon(
                                        Icons.remove,
                                        color: Colors.red,
                                      ),
                                      onPressed: () {
                                        var id = jsonDecode(cart.carrito)[index]
                                            ["id"];
                                        vitrinaprovider
                                            .restarProducto(id)
                                            .then((value) {
                                          if (value == 1) {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(
                                              const SnackBar(
                                                content: Text(
                                                    'No es Posible Seguir Restando'),
                                                backgroundColor: Colors.red,
                                              ),
                                            );
                                          } else {
                                            Future.delayed(
                                                const Duration(
                                                    milliseconds: 500), () {
                                              setState(() {});
                                            });
                                          }
                                        });
                                      },
                                    ),
                                    Container(
                                      child: Center(
                                        child: Text(
                                            jsonDecode(cart.carrito)[index]
                                                    ["quantity"]
                                                .toString()),
                                      ),
                                    ),
                                    IconButton(
                                      icon: Icon(
                                        Icons.add,
                                        color: Colors.green,
                                      ),
                                      onPressed: () {
                                        var id = jsonDecode(cart.carrito)[index]
                                            ["id"];
                                        vitrinaprovider
                                            .sumarProducto(id)
                                            .then((value) {
                                          if (value == 1) {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(
                                              const SnackBar(
                                                content: Text(
                                                    'No es Posible Seguir Sumando este Producto'),
                                                backgroundColor: Colors.red,
                                              ),
                                            );
                                          } else {
                                            Future.delayed(
                                                const Duration(
                                                    milliseconds: 500), () {
                                              setState(() {});
                                            });
                                          }
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 25),
                                child: Center(
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.delete,
                                      size: 50,
                                      color: Colors.red,
                                    ),
                                    onPressed: () {
                                      if (jsonDecode(cart.carrito).length ==
                                          1) {
                                        cart.removetPrefs();
                                        if (prefs.idTipo == 9) {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    VitrinaDistribuidor()),
                                          );
                                        } else {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => Vender()),
                                          );
                                        }
                                      } else {
                                        var id = jsonDecode(cart.carrito)[index]
                                            ["id"];
                                        vitrinaprovider.elimarProducto(id);
                                        Future.delayed(
                                            const Duration(milliseconds: 500),
                                            () {
                                          setState(() {});
                                        });
                                      }
                                    },
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              } else {
                return Container(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40.0),
                    ),
                    elevation: 5,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          //padding: EdgeInsets.all(10),
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(15),
                                  child: FadeInImage(
                                    width: 80,
                                    height: 80,
                                    image: NetworkImage("$rutaimg" +
                                        jsonDecode(cart.carrito)[index]
                                            ['photo']),
                                    placeholder:
                                        AssetImage('assets/img/loading.gif'),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                width: 150,
                                child: Column(
                                  children: [
                                    Container(
                                      child: Center(
                                        child: Text(
                                            jsonDecode(cart.carrito)[index]
                                                ['name']),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 15),
                                      child: Center(
                                        child: Text((double.parse(jsonDecode(
                                                            cart.carrito)[index]
                                                        ["price_caja"]) /
                                                    double.parse(jsonDecode(
                                                            cart.carrito)[index]
                                                        ["detal"]))
                                                .toString() +
                                            ' \$ C/U'),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                //margin: EdgeInsets.only(left: 10),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    IconButton(
                                      icon: Icon(
                                        Icons.remove,
                                        color: Colors.red,
                                      ),
                                      onPressed: () {
                                        var id = jsonDecode(cart.carrito)[index]
                                            ["id"];
                                        vitrinaprovider
                                            .restarProducto(id)
                                            .then((value) {
                                          if (value == 1) {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(
                                              const SnackBar(
                                                content: Text(
                                                    'No es Posible Seguir Restando'),
                                                backgroundColor: Colors.red,
                                              ),
                                            );
                                          } else {
                                            Future.delayed(
                                                const Duration(
                                                    milliseconds: 500), () {
                                              setState(() {});
                                            });
                                          }
                                        });
                                      },
                                    ),
                                    Container(
                                      child: Center(
                                        child: Text(
                                            jsonDecode(cart.carrito)[index]
                                                    ["quantity"]
                                                .toString()),
                                      ),
                                    ),
                                    IconButton(
                                      icon: Icon(
                                        Icons.add,
                                        color: Colors.green,
                                      ),
                                      onPressed: () {
                                        var id = jsonDecode(cart.carrito)[index]
                                            ["id"];
                                        vitrinaprovider
                                            .sumarProducto(id)
                                            .then((value) {
                                          if (value == 1) {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(
                                              const SnackBar(
                                                content: Text(
                                                    'No es Posible Seguir Sumando este Producto'),
                                                backgroundColor: Colors.red,
                                              ),
                                            );
                                          } else {
                                            Future.delayed(
                                                const Duration(
                                                    milliseconds: 500), () {
                                              setState(() {});
                                            });
                                          }
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 25),
                                child: Center(
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.delete,
                                      size: 50,
                                      color: Colors.red,
                                    ),
                                    onPressed: () {
                                      if (jsonDecode(cart.carrito).length ==
                                          1) {
                                        cart.removetPrefs();
                                        if (prefs.idTipo == 9) {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    VitrinaDistribuidor()),
                                          );
                                        } else {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    VitrinaVendedor()),
                                          );
                                        }
                                      } else {
                                        var id = jsonDecode(cart.carrito)[index]
                                            ["id"];
                                        vitrinaprovider.elimarProducto(id);
                                        Future.delayed(
                                            const Duration(milliseconds: 500),
                                            () {
                                          setState(() {});
                                        });
                                      }
                                    },
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }
            },
          )),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          RawMaterialButton(
            onPressed: () async {
              await vitrinaprovider.getCarritoComprar().then((value) {
                setState(() {
                  flete = value['flete'];
                  if (flete == 0) {
                    flete = value['flete'];
                  } else {
                    flete = value['flete'][0]['precio'];
                  }
                });
              });
              _showSimpleModalDialog(context);
              /*Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => BuscarUsuarioVender()),
              );*/
            },
            elevation: 5,
            fillColor: Colors.blue,
            child: Text('Ver Total',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold)),
            padding: EdgeInsets.all(30),
            shape: CircleBorder(),
          ),
        ],
      ),
    );
  }

  _showSimpleModalDialog(context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          var carrito = jsonDecode(cart.carrito);
          var subtotal = 0.0;
          carrito.forEach((totales) {
            if (totales['unidad'] == 1) {
              subtotal = subtotal +
                  ((double.parse(totales['price_unidad'])) *
                      ((totales['quantity'])));
            } else {
              subtotal = subtotal +
                  ((double.parse(totales['price_caja']) /
                          double.parse(totales['detal'])) *
                      ((totales['quantity'])));
            }
          });
          var iva = subtotal * 0.16;
          var totalfinal = subtotal + iva + flete;
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              constraints: BoxConstraints(maxHeight: 350),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          text: 'subtotal: ' + subtotal.toString() + ' \$',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Colors.black,
                              wordSpacing: 1)),
                    ),
                    RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          text: 'flete: ' + flete.toString() + ' \$',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Colors.black,
                              wordSpacing: 1)),
                    ),
                    RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          text: 'iva: ' + iva.toString() + ' \$',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Colors.black,
                              wordSpacing: 1)),
                    ),
                    RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          text: 'total: ' + totalfinal.toString() + ' \$',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Colors.black,
                              wordSpacing: 1)),
                    ),
                    RawMaterialButton(
                      onPressed: () {
                        //_showSimpleModalDialog(context);
                        if (subtotal < 10) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text('el subtotal minimo son 10\$'),
                              backgroundColor: Colors.red,
                            ),
                          );
                          Navigator.pop(context);
                        } else {
                          // Navigator.push(
                          //   context,
                          //   MaterialPageRoute(
                          //       builder: (context) => BuscarUsuarioVender()),
                          // );
                          Navigator.pushNamed(context, 'buscar_usuario_vender',
                              arguments: flete);
                        }
                      },
                      elevation: 5,
                      fillColor: Colors.blue,
                      child: Text('Procesar',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.bold)),
                      padding: EdgeInsets.all(30),
                      shape: CircleBorder(),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
