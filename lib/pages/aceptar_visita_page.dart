/*import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:yegoapp/pages/vitrina_page.dart';*/
import 'package:toast/toast.dart';
import 'package:yegoapp/pages/vender_page.dart';
import 'package:yegoapp/pages/vitrina_page.dart';
import 'package:yegoapp/providers/aceptar_visita_provider.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
//import 'package:yegoapp/pages/entrega_mapa_page.dart';
//import 'package:yegoapp/providers/entregas_pendientes_provider.dart';
//import 'package:toast/toast.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
//import 'package:url_launcher/url_launcher.dart';
import 'package:yegoapp/widget/mydrawer.dart';

import 'buscar_vender_page.dart';
import 'cuenta_vendedor_page.dart';
//import 'entrega_mapa_page.dart';
//import 'Dart:io';

class AceptarVisita extends StatefulWidget {
  @override
  _AceptarVisitaState createState() => _AceptarVisitaState();
}

class _AceptarVisitaState extends State<AceptarVisita> {
  //@override

  final visitaporaceptarprovider = AceptarVisitaProvider();
  final aceptarvisitaprovider = AceptarVisitaProvider();
  /*Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarAceptarVisita(),
      body: bodyAceptarvisita(),
    );
  }*/
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  void initState() { 
    super.initState();
    //despachoProvider.despachos();
  }
   @override
  Widget build(BuildContext context) {
    //ScrollController _direccion = ScrollController (debugLabel: "");
    return Scaffold(
      key: _scaffoldKey,
      drawer: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: HexColor('#0067A2').withOpacity(0.7),
          ),
          child: myDrawer(context),
        ),
      appBar: AppBar(
        backgroundColor: HexColor('#0067A2'),
        title: Text('Visitas por Aceptar'),
        actions: [
          Container(
            margin: EdgeInsets.only(left: 0),
            child: ClipRRect(
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, 'lista de entregas'),
                child: FadeInImage(
                  image: AssetImage('assets/img/4.png'),
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
        leading: IconButton(
          iconSize: 35,
          icon: Icon(Icons.menu),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
      ),
      body: FutureBuilder(
        future:visitaporaceptarprovider.visitaporaceptar(),
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if (snapshot.hasData) {
            return ListView.builder( 
            itemCount: snapshot.data['informacion'].length,
            //controller: _direccion, 
            itemBuilder: (BuildContext context, int index){
                var idvisita = snapshot.data['informacion'][index][0];
                //var idvisita = orden.toString(); 
                var nombre = snapshot.data['informacion'][index][1];
                var n = nombre.toString();
                var apellido = snapshot.data['informacion'][index][2];
                var a = apellido.toString();
                var estado = snapshot.data['informacion'][index][3];
                var e = estado.toString();
                var municipio = snapshot.data['informacion'][index][4];
                var m = municipio.toString();
                var parroquia = snapshot.data['informacion'][index][5];
                var p = parroquia.toString();
                //return ListTile( title: Text(i));
                return Slidable(key: ValueKey(index),
                  actionPane: SlidableBehindActionPane(),
                  secondaryActions: <Widget>[
                    IconSlideAction(
                      caption: 'aceptar',
                      color:  Colors.grey.shade300,
                      icon: Icons.check,
                      closeOnTap: false,
                      onTap: () async{
                        print(idvisita);
                        var resultado = await visitaporaceptarprovider.aceptarvisita(idvisita);
                        print('respuesta: '+"$resultado['respuesta']");
                        if(resultado['respuesta'] == false){
                          Toast.show('Visita no Asignada', context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER, backgroundColor: Colors.red);
                        }
                        else{
                          Toast.show('Visita Aceptada', context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER, backgroundColor: Colors.green);
                          Navigator.pushReplacementNamed(context, 'aceptar_visita');
                        }

                        //visitaporaceptarprovider.aceptarvisita(idvisita);
                        //print(idorden);
                        //Navigator.pushReplacementNamed(context, 'entrega_codigo', arguments: idorden);//paso id de orden
                        //Toast.show('Udate on $d', context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                      }
                    )
                  ], 
                  //dismissal: SlidableDismissal(child: SlidableDrawerDismissal(),),
                  child: ListTile(
                    title: Text('$n'+' '+'$a'),
                    subtitle: Text('$e'+', '+'$m'+', '+'$p'),
                    
                  ),
                );

              }
          );
          }else{
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
          //return Text('yeah papa sin miedo al exito');
        },
      ),
      
      /*ListView.builder(
        itemCount: 10,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Text("$index"),
          );
        },
      ), //_crearproductos(),*/


      bottomNavigationBar: _footer(context) 
    );
  }
  _footer(BuildContext context) {
  var _currentPage = 3;
  return Theme(
    data: Theme.of(context).copyWith(
      canvasColor: HexColor('#0067A2'),
      primaryColor: Colors.yellowAccent,
    ),
      child: BottomNavigationBar(
        unselectedItemColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.house, size: 30),
            label: 'Comprar',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search, size: 30),
            label: 'Buscar',
          ),
          /*BottomNavigationBarItem(
            icon: Icon(Icons.category, size: 30), 
            label: 'Categorías'
          ),*/
             BottomNavigationBarItem(
            icon: Icon(Icons.credit_card_rounded, size: 30), 
            label: 'Vender'
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle, size: 30), 
            label: 'Cuenta'
          ),
        ],
        currentIndex: _currentPage,
        onTap: (int index) {
          setState(() {
            _currentPage = index;
            switch (_currentPage) {
              case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Vitrina()),
                  );
                break;
              case 1:
                showSearch(context: context, delegate: DataSearchVender());
                break;
              case 2 :
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Vender()),
                );
                break;
              case 3:
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CuentaVendedor()),
                );
                break;
              default:
            }
          });
        },
      ),
    );
  }

  /*_openMap() async {
    // Android
    const url = 'geo:52.32,4.917';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      // iOS
      const url = 'http://maps.Apple.com/?ll=52.32,4.917';
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }
  }*/

  /*_openMap(a,b) async {
    // Android
    var url = 'https://www.google.com/maps/search/?api=1&query='+a+','+b+'';
    if (Platform.isIOS) {
      // iOS
      url = 'http://maps.Apple.com/?ll=52.32,4.917';
    }
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }*/



  //lo q habia puesto el felnando
 /*Widget appBarAceptarVisita() {
    return AppBar(
      backgroundColor: HexColor("#0067A2"),
      title: Text("ACEPTAR VISITA"),
      centerTitle: true,
      leading: IconButton(
        padding: EdgeInsets.only(top: 10, left: 15, bottom: 10),
        iconSize: 15,
        icon: Icon(Icons.arrow_back_ios),
        color: HexColor("#FFFFFF"),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Vitrina()));
        },
      ),
    );
  }

  Widget bodyAceptarvisita() {
    return Container();
  }*/
}
