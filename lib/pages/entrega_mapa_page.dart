import 'package:flutter/material.dart';
//import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:yegoapp/models/directions.dart';
//import 'package:location/location.dart';
import 'package:yegoapp/pages/entrega_inicio_page.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';
import 'dart:typed_data';
//import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:yegoapp/providers/direcciones_provider.dart';

class EntregaMapa extends StatefulWidget {
  EntregaMapa({Key key}) : super(key: key);

  @override
  _EntregaMapaState createState() => _EntregaMapaState();
}

class _EntregaMapaState extends State<EntregaMapa> {
  final Set<Polyline> polyline = {};
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  static const _initialPositionCamera = CameraPosition(
  target: LatLng(10.432441, -66.864675),
  zoom: 11.5,
  );
  int control = 1;
  List<LatLng> routeCoords;
  //PolylineWayPoint googlemapPolyline = new Google
  GoogleMapController _mapController;
  Directions _info;
  Location _locationTracker = Location();
  StreamSubscription _locationSubscription;
  Marker _origin;
  Marker _destination;
  Marker _marker;
  Circle _circle;



  //nuevo intento
    Future<Uint8List> getMarker() async {
    ByteData byteData = await DefaultAssetBundle.of(context).load("assets/car_icon.png");
    return byteData.buffer.asUint8List();
  }

  void updateMarkerAndCircle(LocationData newLocalData, Uint8List imageData) {
    LatLng latlng = LatLng(newLocalData.latitude, newLocalData.longitude);
    this.setState(() {
        _marker = Marker(
          markerId: MarkerId("home"),
          position: latlng,
          rotation: newLocalData.heading,
          draggable: false,
          zIndex: 2,
          flat: true,
          anchor: Offset(0.5, 0.5),
          icon: BitmapDescriptor.fromBytes(imageData));
        _circle = Circle(
          circleId: CircleId("car"),
          radius: newLocalData.accuracy,
          zIndex: 1,
          strokeColor: Colors.blue,
          center: latlng,
          fillColor: Colors.blue.withAlpha(70));
    });
  }

  void getCurrentLocation() async {
    try {
      final args =6;//ModalRoute.of(context).settings.arguments;
      Uint8List imageData = await getMarker();
      var location = await _locationTracker.getLocation();

      var locationDestination = await Directionrepository().getDestination(args);
      var coord = LatLng(double.parse(locationDestination['data'][0]['latitud']), double.parse(locationDestination['data'][0]['longitud']));
      //print(location);
      
      var origin = LatLng(location.latitude, location.longitude);
      var destination = coord;

      updateMarkerAndCircle(location, imageData);

      if (_locationSubscription != null) {
        _locationSubscription.cancel();
      }


      _locationSubscription = _locationTracker.onLocationChanged.listen((newLocalData) {
        if (_mapController != null) {
          if (control == 2){
            control = 1;
          _mapController.animateCamera(CameraUpdate.newCameraPosition(new CameraPosition(
              bearing: 192.8334901395799,
              target: LatLng(newLocalData.latitude, newLocalData.longitude),
              tilt: 0,
              zoom: 13.00,
              )));
          updateMarkerAndCircle(newLocalData, imageData);
          }
          else{
          updateMarkerAndCircle(newLocalData, imageData); 
          }
        }
      });

      final directions = await Directionrepository()
        .getDirections(origin: origin, destination: destination);
        setState(() => _info = directions);

    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        debugPrint("Permission Denied");
      }
    }
  }

  getDestination(args) async {
    var locationDestination = await Directionrepository().getDestination(args);
    var coord = LatLng(double.parse(locationDestination['data'][0]['latitud']), double.parse(locationDestination['data'][0]['longitud']));
    print (coord);
    return coord;
  }

  @override
  void dispose() {
    if (_locationSubscription != null) {
      _locationSubscription.cancel();
    }
    super.dispose();
  }


  //fin nuevo intento


  //LatLng _center = const LatLng(45.521563, -122.677433);
  /*Location _location = Location();
  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    _location.onLocationChanged.listen((l) { 
      mapController.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(l.latitude, l.longitude),zoom: 15),
          ),
      );
    });
  }*/
  /*@override 
  void dispose(){

    _mapController.dispose();
    super.dispose();
  }*/


  @override
   Widget build(BuildContext context) {
     final args =6;//ModalRoute.of(context).settings.arguments;
     //final directionDestination = getDestination(args);
     //print( directionDestination);
     print("este es el argumento $args");
    return Scaffold(
      key: _scaffoldKey,
      drawer: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: HexColor('#0067A2').withOpacity(0.7),
          ),
          child: drawer(),
        ),
      appBar: AppBar(
        centerTitle: false,
        title: const Text('Tu Ruta'),
        actions: [
          if(_origin != null)
          TextButton(
            onPressed: ()=> _mapController.animateCamera(
              CameraUpdate.newCameraPosition(
                CameraPosition(
                  target: _origin.position,
                  zoom: 14.5,
                  tilt: 50.0
                ),
              ),
            ),
            style: TextButton.styleFrom(
              primary: Colors.white,
              textStyle: const TextStyle(fontWeight: FontWeight.w600),
            ),
            child: const Text('TU'),
          ),
          if (_destination != null)
          TextButton(
            onPressed: ()=> _mapController.animateCamera(
              CameraUpdate.newCameraPosition(
                CameraPosition(
                  target: _destination.position,
                  zoom: 14.5,
                  tilt: 50.0
                ),
              ),
            ),
            style: TextButton.styleFrom(
              primary: Colors.white,
              textStyle: const TextStyle(fontWeight: FontWeight.w600),
            ),
            child: const Text('DESTINO'),
          ),
        ],
        leading: IconButton(
          iconSize: 35,
          icon: Icon(Icons.menu),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
      ),
      /*appBar: AppBar(
        backgroundColor: HexColor('#0067A2'),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 80),
            child: ClipRRect(
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, 'lista de entregas'),
                child: FadeInImage(
                  image: AssetImage('assets/img/4.png'),
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
        leading: IconButton(
          iconSize: 35,
          icon: Icon(Icons.menu),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
      ),*/
      body: GoogleMap(
          onMapCreated: (controller)=> _mapController = controller,
          myLocationButtonEnabled: true,
          zoomControlsEnabled: false,
          zoomGesturesEnabled: true,
          initialCameraPosition: _initialPositionCamera,
          markers: Set.of((_marker != null) ? [_marker] : []),
          circles: Set.of((_circle != null) ? [_circle] : []),
          polylines: {
            if(_info!=null)
            Polyline(
              polylineId: const PolylineId('overview_polyline'),
              color: Colors.green,
              width: 5,
              points: _info.polylinePoints
                .map((e) => LatLng(e.latitude, e.longitude))
                .toList(),
            )
          },

          /*{
            if (_origin != null) _origin,
            if (_destination != null) _destination
          },*/
          //onLongPress: _addMarker,
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.location_searching),
          onPressed: () {
            control = 2;
            getCurrentLocation();
          }),
      /*floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        /*onPressed: () => _mapController.animateCamera(
          CameraUpdate.newCameraPosition(_initialPositionCamera),
        ),*/

        child: const Icon(Icons.center_focus_strong),
      ),*/ 

      /*body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            GoogleMap(
              initialCameraPosition: CameraPosition(target: _center),
              mapType: MapType.normal,
              onMapCreated: _onMapCreated,
              myLocationEnabled: true,
            ),
          ],
        ),
      ),*/


      bottomNavigationBar: _footer(context) 
    );
  }

  /*void _addMarker(LatLng pos){
    if (_origin == null || (_origin != null && _destination != null)) {

      setState(() {
        _origin = Marker(
          markerId: const MarkerId('origen'),
          infoWindow: const InfoWindow(title: 'origen'),
          icon: 
            BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure),
          position: pos,
        );
        _destination = null;
      });
    }else{

      setState(() {
        _destination= Marker(
          markerId: const MarkerId('destination'),
          infoWindow: const InfoWindow(title: 'Destination'),
          icon: 
            BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          position: pos,
        );
      });
    }
  }*/

  Drawer drawer() {
    return Drawer(
      child: ListView(
        children: <Widget>[
          Container(
            child: DrawerHeader(
              child: CircleAvatar(
                child: ClipOval(
                  child: Text('BF',style:TextStyle(fontSize: 50, fontWeight: FontWeight.bold)),
                ),
              ),
            ),
            //color: HexColor('#0067A2'),
          ),
          Container(
            //color: HexColor('#0067A2'),
            child: Column(
              children: List.generate(8, (int index) {
                return ListTile(
                  title: Text('opciones',style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold)),
                  leading: Icon(
                    Icons.info,
                    color: Colors.white,
                  ),
                );
              }),
            ),
          )
        ],
      ),
    );
  }
  _footer(BuildContext context) {
    var _currentPage = 0;
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: HexColor('#0067A2'),
        primaryColor: Colors.yellowAccent,
      ),
      child: BottomNavigationBar(
        unselectedItemColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.house, size: 30),
            label: 'Inicio',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search, size: 30),
            label: 'mapa',
          ),
        ],
        currentIndex: _currentPage,
        onTap: (int index) {
          setState(() {
            _currentPage = index;
            switch (_currentPage) {
              case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => EntregaInicio()),
                  );
                break;
              case 1:
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EntregaMapa()),
                );
                break;
              default:
            }
          });
        },
      ),
    );
  }



}