import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:yegoapp/pages/vitrina_page.dart';
import 'package:yegoapp/providers/cambio_a_vendedor_provider.dart';

class CambioAVendedor extends StatefulWidget {
  //const name({Key? key}) : super(key: key);

  @override
  _CambioAVendedorState createState() => _CambioAVendedorState();
}

class _CambioAVendedorState extends State<CambioAVendedor> {
  //Declarando variables de todos las preguntas
  // *CREO QUE SE PUEDE OPTIMIZAR ESTO CON UN FOREACH PERO NO SÉ, CONSULTAR CON EL BRYAN, PD: MIGUEL ME DEBE UNA PIZZA
  //*Llamando al provider
  final cambioavendedorprovider = CambioAvendedorProvider();

  String _radioValue1 = 'D';
  String _radioValue2 = 'D';
  String _radioValue3 = 'D';
  String _radioValue4 = 'D';
  String _radioValue5 = 'D';
  String _radioValue6 = 'D';
  String _radioValue7 = 'D';
  String _radioValue8 = 'D';
  String _radioValue9 = 'D';
  String _radioValue10 = 'D';
  String _radioValue11 = 'D';
  String _radioValue12 = 'D'; //Joder ya estoy ladillado y van 12. 😪
  String _radioValue13 = 'D';
  String _radioValue14 = 'D';
  String _radioValue15 = 'D';
  String _radioValue16 = 'D';
  String _radioValue17 = 'D';
  String _radioValue18 = 'D';
  String _radioValue19 = 'D';
  String _radioValue20 = 'D';
  String _radioValue21 = 'D';
  String _radioValue22 = 'D';
  String _radioValue23 = 'D';
  String _radioValue24 = 'D';
  String _radioValue25 = 'D';
  String _radioValue26 = 'D';
  String _radioValue27 = 'D';
  String _radioValue28 = 'D';
  String _radioValue29 = 'D';
  String _radioValue30 = 'D';

  //Cambio de estado
  void _inputRadio1(value) {
    setState(() {
      _radioValue1 = value;
    });
  }

  void _inputRadio2(value) {
    setState(() {
      _radioValue2 = value;
    });
  }

  void _inputRadio3(value) {
    setState(() {
      _radioValue3 = value;
    });
  }

  void _inputRadio4(value) {
    setState(() {
      _radioValue4 = value;
    });
  }

  void _inputRadio5(value) {
    setState(() {
      _radioValue5 = value;
    });
  }

  void _inputRadio6(value) {
    setState(() {
      _radioValue6 = value;
    });
  }

  void _inputRadio7(value) {
    setState(() {
      _radioValue7 = value;
    });
  }

  void _inputRadio8(value) {
    setState(() {
      _radioValue8 = value;
    });
  }

  void _inputRadio9(value) {
    setState(() {
      _radioValue9 = value;
    });
  }

  void _inputRadio10(value) {
    setState(() {
      _radioValue10 = value;
    });
  }

  void _inputRadio11(value) {
    setState(() {
      _radioValue11 = value;
    });
  }

  void _inputRadio12(value) {
    setState(() {
      _radioValue12 = value;
    });
  }

  void _inputRadio13(value) {
    setState(() {
      _radioValue13 = value;
    });
  }

  void _inputRadio14(value) {
    setState(() {
      _radioValue14 = value;
    });
  }

  void _inputRadio15(value) {
    setState(() {
      _radioValue15 = value;
    });
  }

  void _inputRadio16(value) {
    setState(() {
      _radioValue16 = value;
    });
  }

  void _inputRadio17(value) {
    setState(() {
      _radioValue17 = value;
    });
  }

  void _inputRadio18(value) {
    setState(() {
      _radioValue18 = value;
    });
  }

  void _inputRadio19(value) {
    setState(() {
      _radioValue19 = value;
    });
  }

  void _inputRadio20(value) {
    setState(() {
      _radioValue20 = value;
    });
  }

  void _inputRadio21(value) {
    setState(() {
      _radioValue21 = value;
    });
  }

  void _inputRadio22(value) {
    setState(() {
      _radioValue22 = value;
    });
  }

  void _inputRadio23(value) {
    setState(() {
      _radioValue23 = value;
    });
  }

  void _inputRadio24(value) {
    setState(() {
      _radioValue24 = value;
    });
  }

  void _inputRadio25(value) {
    setState(() {
      _radioValue25 = value;
    });
  }

  void _inputRadio26(value) {
    setState(() {
      _radioValue26 = value;
    });
  }

  void _inputRadio27(value) {
    setState(() {
      _radioValue27 = value;
    });
  }

  void _inputRadio28(value) {
    setState(() {
      _radioValue28 = value;
    });
  }

  void _inputRadio29(value) {
    setState(() {
      _radioValue29 = value;
    });
  }

  void _inputRadio30(value) {
    setState(() {
      _radioValue30 = value;
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarCambioAVendedor(),
      body: bodyCambioAVendedor(),
      //floatingActionButton: enviarEncuesta(),
    );
  }

  Widget appBarCambioAVendedor() {
    return AppBar(
      backgroundColor: HexColor("#0067A2"),
      title: Text("ENCUESTA"),
      centerTitle: true,
      leading: IconButton(
        padding: EdgeInsets.only(top: 10, left: 15, bottom: 10),
        iconSize: 15,
        icon: Icon(Icons.arrow_back_ios),
        color: HexColor("#FFFFFF"),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Vitrina()));
        },
      ),
    );
  }

  Widget bodyCambioAVendedor() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.only(top: 20),
        child: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 100,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: 20.0, right: 30.0, left: 30.0),
                            child: Text(
                              "1. Se me hace fácil hablar “cara a cara” con otros:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue1,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio1(value);
                                    //print(_radioValue);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue1,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio1(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue1,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio1(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "2. Me siento descontento/a por no poder interactuar lo suficiente con mis amigos:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue2,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio2(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue2,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio2(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue2,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio2(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "3. Prefiero expresar mis preocupaciones antes que guardármelas:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue3,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio3(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue3,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio3(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue3,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio3(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 150, // *Cuando sea 4 lineas la pregunta
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "4. Cuando es necesario que una persona convenza a otra persona para conseguir algo; o para pensar diferente a como lo hace; generalmente me lo encarga a mí:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue4,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio4(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue4,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio4(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue4,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio4(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "5. Me resulta fácil mezclarme con la gente en una reunión social:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue5,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio5(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue5,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio5(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue5,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio5(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "6. Me disgusta un poco que la gente me esté mirando cuando trabajo:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue6,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio6(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue6,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio6(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue6,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio6(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "7. Soy capaz de cambiar el método de trabajo con el fin de obtener mejores resultados:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue7,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio7(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue7,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio7(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue7,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio7(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 130, // *Para 3 lineas
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "8. Me creo con la capacidad de tomar decisiones importantes para conseguir los objetivos de todo un grupo:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue8,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio8(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue8,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio8(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue8,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio8(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 15, right: 30),
                            child: Text(
                              "9. Poseo altos deseos de superación personal:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue9,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio9(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue9,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio9(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue9,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio9(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "10. La competencia dentro de mi oficio no me impide seguir avanzando:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue10,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio10(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue10,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio10(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue10,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio10(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "11. En mi vida personal consigo casi siempre todos mis propósitos:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue11,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio11(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue11,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio11(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue11,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio11(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "12. Intento organizarme antes de que aparezcan dificultades en mi trabajo:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue12,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio12(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue12,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio12(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue12,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio12(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 130,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "13. Si un vendedor de dulces me da el cambio errado (de más) al comprarle un producto; le hago ver su error y regreso el sobrante:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue13,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio13(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue13,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio13(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue13,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio13(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "14. Sigo las reglas establecidas dentro de los trabajos que se me encomiendan:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue14,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio14(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue14,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio14(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue14,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio14(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "15. Mayormente me esfuerzo por decir la verdad:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue15,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio15(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue15,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio15(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue15,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio15(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "16. Admito mis errores y desaciertos con humildad y honestidad:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue16,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio16(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue16,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio16(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue16,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio16(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 130,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "17. Dentro de los entornos de trabajo en los que he participado; he percibido que han confiado en mí:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue17,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio17(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue17,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio17(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue17,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio17(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "18. He hecho en mi oficio cosas contrarias a mis principios:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue18,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio18(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue18,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio18(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue18,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio18(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 130,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "19. Utilizo software de mensajería instantánea desde un smartphone o teléfono móvil inteligente. Por ejemplo: Whatsapp:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue19,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio19(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue19,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio19(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue19,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio19(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 130,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "20. Utilizo medios de video llamadas. Por ejemplo: Zoom, Skype, Google Meet ó Duo, entre otros:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue20,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio20(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue20,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio20(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue20,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio20(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "21. Utilizo las redes sociales (RRSS) regularmente:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue21,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio21(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue21,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio21(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue21,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio21(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "22. Manejo una o más cuentas de correo electrónico (e-mail):",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue22,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio22(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue22,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio22(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue22,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio22(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 130,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "23. Utilizo frecuentemente servicios de Google. Por ejemplo: Gmail, Maps, Drive, Noticias, Google pay, entre otros:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue23,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio23(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue23,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio23(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue23,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio23(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "24.He vendido/comprado productos a través del internet:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue24,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio24(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue24,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio24(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue24,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio24(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 20.0, right: 10),
                            child: Text(
                              "25. Puedo manejar mucha información:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue25,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio25(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue25,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio25(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue25,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio25(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "26. Me gusta obtener material educativo para conocer más sobre mi oficio:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue26,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio26(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue26,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio26(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue26,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio26(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "27. Muestro interés por las argumentaciones y conversaciones teóricas:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue27,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio27(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue27,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio27(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue27,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio27(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 30, right: 30),
                            child: Text(
                              "28. Muestro interés por diferentes temas; aún si estos son “difíciles”:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue28,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio28(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue28,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio28(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue28,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio28(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 15, right: 30),
                            child: Text(
                              "29. Me gusta tener un vocabulario amplio:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue29,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio29(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue29,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio29(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue29,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio29(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 400.0,
                    decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent)),
                    height: 120,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 15, right: 30),
                            child: Text(
                              "30. Disfruto pensando sobre las cosas.:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          Container(
                            //margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Verdadero'),
                                Radio(
                                  value: 'A',
                                  groupValue: _radioValue30,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio30(value);
                                  },
                                ),
                                Text('Algunas Veces'),
                                Radio(
                                  value: 'B',
                                  groupValue: _radioValue30,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio30(value);
                                  },
                                ),
                                Text('Falso'),
                                Radio(
                                  value: 'C',
                                  groupValue: _radioValue30,
                                  activeColor: Colors.blue,
                                  onChanged: (value) {
                                    _inputRadio30(value);
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Column(
              children: <Widget>[
                Center(
                  child: Text(
                    "ÉXITO",
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.blue),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            //Aca viene el boton de enviar y borrador
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Center(
                    child: TextButton(
                      //* envio cada opcion que selecciono el usuario al provider(controlador y de ahi se envía a la api)
                      onPressed: () {
                        cambioavendedorprovider.enviar(
                          _radioValue1,
                          _radioValue2,
                          _radioValue3,
                          _radioValue4,
                          _radioValue5,
                          _radioValue6,
                          _radioValue7,
                          _radioValue8,
                          _radioValue9,
                          _radioValue10,
                          _radioValue11,
                          _radioValue12,
                          _radioValue13,
                          _radioValue14,
                          _radioValue15,
                          _radioValue16,
                          _radioValue17,
                          _radioValue18,
                          _radioValue19,
                          _radioValue20,
                          _radioValue21,
                          _radioValue22,
                          _radioValue23,
                          _radioValue24,
                          _radioValue25,
                          _radioValue26,
                          _radioValue27,
                          _radioValue28,
                          _radioValue29,
                          _radioValue30,
                        );
                      },
                      child: Container(
                        width: 200.0,
                        decoration: BoxDecoration(
                          color: HexColor('#0067A2'),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        height: 50,
                        child: Center(
                          child: Text(
                            "  ENVIAR ENCUESTA ",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
