//import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';
import 'package:yegoapp/pages/vitrina_page.dart';
import 'package:yegoapp/providers/vitrina_provider.dart';
//import 'package:yegoapp/providers/agregar_producto_provider.dart';

class DetalleProductoPage extends StatefulWidget {

  DetalleProductoPage({Key key}) : super(key: key);
  //bool vunidad = false;

  @override
  _DetalleProductoPageState createState() => _DetalleProductoPageState();

}

class _DetalleProductoPageState extends State<DetalleProductoPage> {
  bool vunidad = false;//para cntrolar la aparicion de unidades
  bool vcaja= false; //para cntrolar la aparicion de cajas
  bool vcuartos = false;
  bool vbotoncaja = true;
  bool boton1= false;
  bool boton2= false;
  bool boton3= false;
  bool boton4= false;
  //variables para los cuartos de caja
  bool mcuarto1 = false;
  bool mcuarto2 = false;
  bool mcuarto3 = false;
  bool mcaja = false; 
  final vitrinaprovider =VitrinaProvider();
  final rutaimg = 'http://157.245.82.111/';
  final f = NumberFormat("#,##0.00", "en_US");
  var numero ;
  int _radioValue = 2;
  int _radioValue2 = 1;
  int _numeroProductos = 1;
  int _numeroProductos1 = 0;
  int cuartoCaja = 0;
  int cantidad = 0;
  int cajat = 0;
  var cuarto1;
  var cuarto2;
  var cuarto3;
  //var detal;
  var nueva;
  var nueva2;

  void _inputRadio(int value) {
    setState(() {
      _radioValue = value;
    });
  }
  void _inputRadio2(int value) {
    setState(() {
      _radioValue2 = value;
    });
  }
  void _sumar(int value) {
    setState(() {
      if((_numeroProductos+1)<=cantidad){
      _numeroProductos = value+1;
      }else{
        ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('No es Posible agregar mas unidades'),
          backgroundColor: Colors.red,
        ),
        );
      }
      //print(_numeroProductos);
    });
  }
  void _restar(int value) {
    if (_numeroProductos !=1) {
      setState(() {
        _numeroProductos=value-1;
      });
    }else{
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('No es Posible Seguir Restando'),
          backgroundColor: Colors.red,
        ),
      );
    }
  }
  // void _sumar1(int value) {
  //   print(_numeroProductos1+cajat);
  //   if((_numeroProductos1+cajat)<=cantidad){
  //   setState(() {
  //     _numeroProductos1 = _numeroProductos1+cajat;
  //     //print(_numeroProductos1);
  //   });
  //   }else{
  //     ScaffoldMessenger.of(context).showSnackBar(
  //       const SnackBar(
  //         content: Text('No es Posible agregar mas unidades'),
  //         backgroundColor: Colors.red,
  //       ),
  //     );
  //   }
  // }
  // void _restar1(int value) {
  //   if (_numeroProductos1 != 0) {
  //     setState(() {
  //       _numeroProductos1=_numeroProductos1-cajat;
  //     });
  //   }else{
  //     ScaffoldMessenger.of(context).showSnackBar(
  //       const SnackBar(
  //         content: Text('No es Posible Seguir Restando'),
  //         backgroundColor: Colors.red,
  //       ),
  //     );
  //   }
  // }

  @override
  Widget build(BuildContext context) {

    final args =ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    numero =(args['price_unidad']);
    var i = double.parse(numero);
    numero = f.format(i);
    cantidad = args['cantidad']; 
    //cantidad.toString();
    //detal = args['detal'];
    //cuartoCaja = int.parse(args['cuartocaja']);
    cajat = int.parse(args['detal']);
    //_numeroProductos = _numeroProductos+cajat;
    //aqui se calcula cuanto cuesta cada cuartyo de caja
    //cuarto1 = (double.parse((args['price_caja']).toString())/cajat)*cuartoCaja;
    //cuarto2 = (double.parse((args['price_caja']).toString())/cajat)*(cuartoCaja*2);
    //cuarto3 = (double.parse((args['price_caja']).toString())/cajat)*(cuartoCaja*3);
    //nueva = double.parse(cuarto1);
    //nueva2 = nueva/4;
    /*if(cantidad < cuartoCaja){
      vbotoncaja = false;
    }*/

    return Scaffold(
      appBar: appBarDetalle(),
      body: bodyDetalle(args, rutaimg,numero,_radioValue,_radioValue2,_numeroProductos,_numeroProductos1),
    );
  }

  Widget appBarDetalle() {
    return AppBar(
      backgroundColor: HexColor("#0067A2"),
      title: Text("DETALLE DEL PRODUCTO"),
      centerTitle: true,
      leading: IconButton(
        padding: EdgeInsets.only(top: 10, left: 15, bottom: 10),
        iconSize: 15,
        icon: Icon(Icons.arrow_back_ios),
        color: HexColor("#FFFFFF"),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }

  Widget bodyDetalle(args, rutaimg,numero,_radioValue,_radioValue2,_numeroProductos,_numeroProductos1) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(top: 15),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              margin: EdgeInsets.only(bottom: 10),
              child: Text(
                args["name"],
                textAlign:TextAlign.center,
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
            ),
            Container(
              width: 350,
              height: 300,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40.0),
                ),
                elevation: 10,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Center(
                        child: ClipRRect(
                           borderRadius: BorderRadius.circular(40),
                          child: Column(
                            children: [
                              GestureDetector(
                                child: FadeInImage(
                                  width: 320,
                                  height: 280,
                                  image: NetworkImage("$rutaimg" + args["photo"]),
                                  placeholder:AssetImage('assets/img/loading.gif'),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    )
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: 200,
                    child: ListView(
                      physics: ClampingScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: [
                        Container(
                          width: 200,
                          height: 200,
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40.0),
                            ),
                            elevation: 10,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Container(
                                    padding: EdgeInsets.all(5),
                                    child: Center(
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(40),
                                        child: Column(
                                          children: [
                                            GestureDetector(
                                              child: FadeInImage(
                                                height: 180,
                                                width: 200,
                                                image: NetworkImage(
                                                    "$rutaimg" + args["photo"]),
                                                placeholder: AssetImage(
                                                    'assets/img/loading.gif'),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ))
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 200,
                          height: 200,
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40.0),
                            ),
                            elevation: 10,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Container(
                                    padding: EdgeInsets.all(5),
                                    child: Center(
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(40),
                                        child: Column(
                                          children: [
                                            GestureDetector(
                                            child: FadeInImage(
                                              width: 200,
                                              height: 180,
                                              image: NetworkImage("$rutaimg"+args["photo2"]),
                                              placeholder: AssetImage(
                                                'assets/img/loading.gif'
                                              ),
                                                fit: BoxFit.cover,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                )
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 200,
                          height: 200,
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40.0),
                            ),
                            elevation: 10,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Container(
                                    padding: EdgeInsets.all(5),
                                    child: Center(
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(40),
                                        child: Column(
                                          children: [
                                            GestureDetector(
                                              child: FadeInImage(
                                                height: 180,
                                                width: 200,
                                                image: NetworkImage("$rutaimg" +args["photo3"]),
                                                placeholder: AssetImage('assets/img/loading.gif'),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ))
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 200,
                          height: 200,
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40.0),
                            ),
                            elevation: 10,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Container(
                                    padding: EdgeInsets.all(5),
                                    child: Center(
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(40),
                                        child: Column(
                                          children: [
                                            GestureDetector(
                                              child: FadeInImage(
                                                height: 180,
                                                width: 200,
                                                image: NetworkImage(
                                                    "$rutaimg" + args["photo"]),
                                                placeholder: AssetImage(
                                                    'assets/img/loading.gif'),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ))
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Column(
              children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(horizontal: 7,vertical: 15),
                width: 400,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40.0),
                  ),
                  elevation: 10,
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(15),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: 10),
                              child: Text(args["name"],style: TextStyle(fontSize: 25,color: Colors.blue,fontWeight: FontWeight.bold) ,textAlign:TextAlign.center ),
                            ),
                            Container(
                              child: Text(args["description"],
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              height: 25,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Radio(
                                    value: 0,
                                    groupValue: _radioValue,
                                    activeColor: Colors.blue,
                                    onChanged: (value){
                                     _inputRadio(value);
                                     vunidad = true;
                                     mcaja = false;
                                     vcaja = false;
                                     vcuartos = false;
                                     mcuarto1 = false;
                                     mcuarto2 =false;
                                     mcuarto3 = false;
                                    //  boton1 =false;
                                    //  boton2=false;
                                    //  boton3=false;
                                    //  boton4=false;
                                     _inputRadio2(1);
                                    },
                                  ),
                                  Text('Producto por Unidad'),
                                  // Visibility(
                                  // visible: vbotoncaja,
                                  // child:
                                  // Row(
                                  // children: [   
                                  // Radio(
                                  //   value: 1,
                                  //   groupValue: _radioValue,
                                  //   activeColor: Colors.blue,
                                  //   onChanged: (value){
                                  //     _inputRadio(value);
                                  //     if(cantidad >= cuartoCaja){
                                  //      vcuartos = true;
                                  //      boton1 = true; 
                                  //     }
                                  //     if(cantidad>= cuartoCaja*2){
                                  //      boton2 = true;
                                  //     }
                                  //     if(cantidad>= cuartoCaja*3){
                                  //      boton3 = true;
                                  //     }
                                  //     if(cantidad>= cajat){
                                  //      boton4 = true;
                                  //     }
                                  //     vunidad = false;
                                  //     vcaja = true;
                                  //     vcuartos = true;
                                  //     mcuarto1 = true;
                                  //   },
                                  // ),
                                  // Text('Producto por Caja'),
                                  // ]
                                  // )
                                  // )
                                ],
                              ),
                            )
                          ],
                        ),      
                      ),
                      Visibility(//esto es para unidades
                      visible: vunidad,
                      child:
                      Row(
                        children: [
                          Container(      
                            margin: EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(
                              border: Border.all(width: 2, color: Colors.grey),
                              borderRadius: BorderRadius.circular(3),
                            ),
                            height: 42,
                            width: 150,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 4),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  IconButton(
                                    icon: Icon(Icons.remove),
                                    onPressed: () {
                                      _restar(_numeroProductos);
                                    },
                                  ),
                                  Expanded(
                                    child: Center(
                                      child: Text(
                                        _numeroProductos.toString()
                                      ),
                                    ),
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.add),
                                    onPressed: () {
                                      _sumar(_numeroProductos);
                                    },
                                  )
                                ],
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Text("\$."+" "+numero+" por unidad",style: TextStyle(fontSize: 20,color: Colors.blue,fontWeight: FontWeight.bold))
                          ),
                        ],
                      ),
                      ),
                      // Visibility(//este es para cajas
                      //   visible: vcuartos,
                      //   child:Column(
                      //     children:[
                      //     Visibility(
                      //     visible: vcuartos,
                      //     child:Column(
                      //       children: [
                      //       Container(
                      //             margin: EdgeInsets.only(top: 10),
                      //             height: 25,
                      //             child: Row(
                      //               mainAxisAlignment: MainAxisAlignment.center,
                      //               children: [
                      //                 Visibility(
                      //                 visible: boton1,  
                      //                 child:Row(
                      //                 children: [  
                      //                 Radio(
                      //                   value: 1,
                      //                   groupValue: _radioValue2,
                      //                   activeColor: Colors.blue,
                      //                   onChanged: (value){
                      //                   _inputRadio2(value);
                      //                   //vunidad = true;
                      //                   vcaja = true;
                      //                   mcuarto1 = true;
                      //                   mcaja =false;
                      //                   mcuarto2 = false;
                      //                   mcuarto3 =false;
                      //                   //print(args);
                      //                   },
                      //                 ),
                      //                 Text('1/4'),
                      //                 ]
                      //                 )
                      //                 ),
                      //                 Visibility(
                      //                 visible: boton2,  
                      //                 child:Row(
                      //                 children: [  
                      //                 Radio(
                      //                   value: 2,
                      //                   groupValue: _radioValue2,
                      //                   activeColor: Colors.blue,
                      //                   onChanged: (value){
                      //                   _inputRadio2(value);
                      //                   //vunidad = true;
                      //                   vcaja = true;
                      //                   mcuarto1 = false;
                      //                   mcaja =false;
                      //                   mcuarto2 = true;
                      //                   mcuarto3 =false;
                      //                   //print(args);
                      //                   },
                      //                 ),
                      //                 Text('2/4'),
                      //                 ]
                      //                 )
                      //                 ),
                      //                 Visibility(
                      //                 visible: boton3,  
                      //                 child:Row(
                      //                 children: [  
                      //                 Radio(
                      //                   value: 3,
                      //                   groupValue: _radioValue2,
                      //                   activeColor: Colors.blue,
                      //                   onChanged: (value){
                      //                   _inputRadio2(value);
                      //                   //vunidad = true;
                      //                   vcaja = true;
                      //                   mcuarto1 = false;
                      //                   mcaja =false;
                      //                   mcuarto2 = false;
                      //                   mcuarto3 =true;
                      //                   //print(args);
                      //                   },
                      //                 ),
                      //                 Text('3/4'),
                      //                 ]
                      //                 )
                      //                 ),
                      //                 Visibility(
                      //                 visible: boton4,  
                      //                 child:Row(
                      //                 children: [  
                      //                 Radio(
                      //                   value: 4,
                      //                   groupValue: _radioValue2,
                      //                   activeColor: Colors.blue,
                      //                   onChanged: (value){
                      //                   _inputRadio2(value);
                      //                     //_numeroProductos = cajat;
                      //                     //vunidad = false;
                      //                     vcaja = true;
                      //                     mcaja = true;
                      //                     mcuarto1 =false;
                      //                     mcuarto2 = false;
                      //                     mcuarto3 =false;
                      //                     //print(_numeroProductos);
                      //                   },
                      //                 ),
                      //                 Text('caja'),
                      //                 ]
                      //                 )
                      //                 ),
                      //               ],
                      //             ),
                      //           )  
                      //       ],
                      //     )


                      //     ),  
                      //       Visibility(
                      //       visible: vcaja,
                      //       child:
                      //         Container(
                      //         margin: EdgeInsets.only(top: 10),
                      //         //height: 25,
                      //         child :
                      //         Row(
                      //           mainAxisAlignment: MainAxisAlignment.center,
                      //           children:
                      //           [
                      //             Visibility(
                      //             visible:mcaja,//aqui
                      //             child:Row(
                      //               children:[ 
                      //             Container(      
                      //               margin: EdgeInsets.only(left: 10),
                      //               decoration: BoxDecoration(
                      //                 border: Border.all(width: 2, color: Colors.grey),
                      //                 borderRadius: BorderRadius.circular(3),
                      //               ),
                      //               height: 42,
                      //               width: 150,
                      //               child: Padding(
                      //                 padding: const EdgeInsets.symmetric(horizontal: 4),
                      //                 child: Row(
                      //                   mainAxisAlignment: MainAxisAlignment.center,
                      //                   children: <Widget>[
                      //                     IconButton(
                      //                       icon: Icon(Icons.remove),
                      //                       onPressed: () {
                      //                         _restar1(_numeroProductos1);
                      //                       },
                      //                     ),
                      //                     Expanded(
                      //                       child: Center(
                      //                         child: Text(
                      //                           (_numeroProductos1).toString()
                      //                         ),
                      //                       ),
                      //                     ),
                      //                     IconButton(
                      //                       icon: Icon(Icons.add),
                      //                       onPressed: () {
                      //                         //_numeroProductos=_numeroProductos+cajat;
                                              
                      //                         _sumar1(_numeroProductos1);
                                              
                      //                       },
                      //                     )
                      //                   ],
                      //                 ),
                      //               ),
                      //             ),
                      //             Container(
                      //               margin: EdgeInsets.only(left: 20),
                      //               child: Text("\$."+" "+numero+" por caja",style: TextStyle(fontSize: 20,color: Colors.blue,fontWeight: FontWeight.bold))
                      //             ),
                      //             ]//children
                      //             )
                      //             ),
                      //             Visibility(
                      //             visible: mcuarto1,
                      //             child:Row(
                      //               children: [
                      //                 Container(
                      //                   margin: EdgeInsets.only(left: 20),
                      //                   child: Text(cuarto1.toStringAsFixed(2)+'\$ por '+(cuartoCaja).toString()+' Unidades',style: TextStyle(fontSize: 20,color: Colors.blue,fontWeight: FontWeight.bold))
                      //                 ),
                      //               ],
                      //             )
                      //             ),
                      //                                               Visibility(
                      //             visible: mcuarto2,
                      //             child:Row(
                      //               children: [
                      //                 Container(
                      //                   margin: EdgeInsets.only(left: 20),
                      //                   child: Text(cuarto2.toStringAsFixed(2)+'\$ por '+(cuartoCaja*2).toString()+' Unidades',style: TextStyle(fontSize: 20,color: Colors.blue,fontWeight: FontWeight.bold))
                      //                 ),
                      //               ],
                      //             )
                      //             ),
                      //                                               Visibility(
                      //             visible: mcuarto3,
                      //             child:Row(
                      //               children: [
                      //                 Container(
                      //                   margin: EdgeInsets.only(left: 20),
                      //                   child: Text(cuarto3.toStringAsFixed(2)+'\$ por '+(cuartoCaja*3).toString()+' Unidades',style: TextStyle(fontSize: 20,color: Colors.blue,fontWeight: FontWeight.bold))
                      //                 ),
                      //               ],
                      //             )
                      //             )


                      //           ],
                      //         )),
                      //       ),
                      //     ]
                      //   )
                      // ), 
                      Container(
                        margin: EdgeInsets.only(top: 20,bottom: 15),
                        width: 300,
                        height: 50,
                        child: ElevatedButton(
                          child: Text(
                            'Agregar',
                            style: TextStyle(fontSize: 18),
                          ),
                          onPressed: () {
                            switch (_radioValue) {
                              case 0:
                                //print(_numeroProductos);
                                vitrinaprovider.postProductoDetalle(args,_numeroProductos,1);
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text('Producto Agregado'),
                                    backgroundColor: Colors.green,
                                  ),
                                );
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => Vitrina()),
                                );
                                break;
                              case 1:
                                switch (_radioValue2) {
                                  case 1:
                                    //print(cuartoCaja);
                                    vitrinaprovider.postProductoDetalle(args,cuartoCaja,2);
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text('Producto Agregado'),
                                        backgroundColor: Colors.green,
                                      ),
                                    );
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => Vitrina()),
                                    );
                                  break;
                                  case 2:
                                    //print(cuartoCaja*2);
                                    var cantidadp = cuartoCaja*2;
                                    vitrinaprovider.postProductoDetalle(args,cantidadp,2);
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text('Producto Agregado'),
                                        backgroundColor: Colors.green,
                                      ),
                                    );
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => Vitrina()),
                                    );
                                  break;
                                  case 3:
                                    //print(cuartoCaja*3);
                                    var cantidadp = cuartoCaja*3;
                                    vitrinaprovider.postProductoDetalle(args,cantidadp,2);
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text('Producto Agregado'),
                                        backgroundColor: Colors.green,
                                      ),
                                    );
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => Vitrina()),
                                    );
                                  break;
                                  case 4:
                                    //print(_numeroProductos1);
                                    vitrinaprovider.postProductoDetalle(args,_numeroProductos1,2);
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text('Producto Agregado'),
                                        backgroundColor: Colors.green,
                                      ),
                                    );
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => Vitrina()),
                                    );
                                  break;
                                  default:
                                  break;
                                }
                                
                                break;
                              default:
                                break;
                            }
                            /*vitrinaprovider.postProductoDetalle(args,_numeroProductos);
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  content: Text('Producto Agregado'),
                                  backgroundColor: Colors.green,
                                ),
                              );
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => Vitrina()),
                              );
                              */
                              //print('radiovalue '+_radioValue.toString());
                              //print('radiovalue2 '+_radioValue2.toString());
                          },
                        ), 
                      )
                    ],
                  ),
                ),
              )
              ],
            )
          ],
        ),
      ),
    );
  }

}
