import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:yegoapp/pages/entrega_mapa_page.dart';
import 'package:yegoapp/providers/entregas_pendientes_provider.dart';
import 'package:yegoapp/pages/entrega_inicio_page.dart';
import 'package:toast/toast.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';
import 'package:yegoapp/widget/mydrawer.dart';
//import 'package:toast/toast.dart';
//import 'package:flutter_slidable/flutter_slidable.dart';


class EntregaCodigo extends StatefulWidget {
  EntregaCodigo({Key key}) : super(key: key);

  @override
  _EntregaCodigoState createState() => _EntregaCodigoState();
}

class _EntregaCodigoState extends State<EntregaCodigo> {
  final prefs = PreferenciasUsuario();
  TextEditingController _inputcodigo =new TextEditingController();
  final despachoProvider = EntregasProvider();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  var orden;
  var telefono;
  var nombre;
  var apellido;
  var cedula;
  var estado;
  var municipio;
  var parroquia;
  var direccion;
    void initState() { 
    super.initState();
    //despachoProvider.despachos();
  }

  @override
  Widget build(BuildContext context) {
    final args =ModalRoute.of(context).settings.arguments;
    //ScrollController _direccion = ScrollController (debugLabel: "");
    return Scaffold(
      key: _scaffoldKey,
      drawer: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: HexColor('#0067A2').withOpacity(0.7),
          ),
          child: myDrawer(context),
        ),
      appBar: AppBar(
        backgroundColor: HexColor('#0067A2'),
        title: Text('Entregas'),
        actions: [
          Container(
            margin: EdgeInsets.only(left: 0),
            child: ClipRRect(
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, 'lista de entregas'),
                child: FadeInImage(
                  image: AssetImage('assets/img/4.png'),
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
        leading: IconButton(
          iconSize: 35,
          icon: Icon(Icons.menu),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
      ),
      body:FutureBuilder( 
      future:despachoProvider.direccion(args).then((value){
        orden = value['data'][0][0];
        estado = value['data'][0][1];
        municipio = value['data'][0][2];
        parroquia = value['data'][0][3];
        telefono = value['data'][0][4];
        nombre = value['data'][0][5];
        apellido = value['data'][0][6];
        cedula = value['data'][0][7];
        direccion = value['data'][0][8];
        print(direccion);
      }),
      builder: (BuildContext context, AsyncSnapshot snapshot){
      return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.all(20.0),
            children:<Widget>[
              Text("Orden: $orden"),
              Text('Cedula: $cedula'),
              Text('Nombre: $nombre'),
              Text('Apellido: $apellido'),
              Text('Telefono: $telefono'),
              Text('Direccion: $direccion, $municipio, $parroquia, $estado'),
            ],
          ),
          _crearInputCodigo(),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child:  ElevatedButton(
              child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 20.0),
                child: Text(
                  'Enviar',
                  style: TextStyle(
                    color: HexColor("FFFFFF"),
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
                ),
              ),
              style: ButtonStyle(
                elevation: MaterialStateProperty.all<double>(0.0),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0)))),
              onPressed: () async {
              // Validate devolverá true si el formulario es válido, o false si
              // el formulario no es válido.
              //if (_formKey.currentState.validate()) {
                  var resultado = await despachoProvider.verificarCod(args, _inputcodigo.text,prefs.id);
                  print(resultado['respuesta']); 
                  if(resultado['respuesta'] == false){
                  Toast.show('Porfavor Verifique el Codigo', context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER, backgroundColor: Colors.red);
                  }
                  else{
                    Toast.show('Codigo aceptado', context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER, backgroundColor: Colors.green);
                    Navigator.pushReplacementNamed(context, 'entrega_inicio');
                  }
                // Si el formulario es válido, muestre un snackbar. En el mundo real, a menudo
                // desea llamar a un servidor o guardar la información en una base de datos
                //Scaffold
                //.of(context)
                //.showSnackBar(SnackBar(content: Text('Processing Data')));
                //print('validando');
              //}
              }
          
            )
          ),
        ],
      ),
      );
      }
      ),
      /*ListView.builder(
        itemCount: 10,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Text("$index"),
          );
        },
      ), //_crearproductos(),*/


      bottomNavigationBar: _footer(context) 
    );
  }

  Drawer drawer() {
    return Drawer(
      child: ListView(
        children: <Widget>[
          Container(
            child: DrawerHeader(
              child: CircleAvatar(
                child: ClipOval(
                  child: Text('BF',style:TextStyle(fontSize: 50, fontWeight: FontWeight.bold)),
                ),
              ),
            ),
            //color: HexColor('#0067A2'),
          ),
          Container(
            //color: HexColor('#0067A2'),
            child: Column(
              children: List.generate(8, (int index) {
                return ListTile(
                  title: Text('opciones',style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold)),
                  leading: Icon(
                    Icons.info,
                    color: Colors.white,
                  ),
                );
              }),
            ),
          )
        ],
      ),
    );
  }
  _footer(BuildContext context) {
    var _currentPage = 0;
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: HexColor('#0067A2'),
        primaryColor: Colors.yellowAccent,
      ),
      child: BottomNavigationBar(
        unselectedItemColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.house, size: 30),
            label: 'lista',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search, size: 30),
            label: 'mapa',
          ),
        ],
        currentIndex: _currentPage,
        onTap: (int index) {
          setState(() {
            _currentPage = index;
            switch (_currentPage) {
              case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => EntregaInicio()),
                  );
                break;
              case 1:
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EntregaMapa()),
                );
                break;
              default:
            }
          });
        },
      ),
    );
  }

  _crearInputCodigo() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: _inputcodigo,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Codigo',
       labelText: 'Codigo',
       suffixIcon: Icon(Icons.lock,size: 30,),
       //icon: Icon(Icons.account_circle,size:40,),
      ),
      //validator: validator.validateName,
    );
  }



}