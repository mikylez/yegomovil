import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:yegoapp/pages/vitrina_page.dart';
import 'package:yegoapp/providers/vitrina_provider.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';

class CartPage extends StatefulWidget {
  CartPage({Key key}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  final prefs = PreferenciasUsuario();
  final vitrinaprovider = VitrinaProvider();
  var carritomodal;
  var flete;

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(child: CircularProgressIndicator()
            // new Row(
            // mainAxisSize: MainAxisSize.min,
            // children: [
            //   new CircularProgressIndicator(),
            //   new Text("cargando"),
            // ],
            //),
            );
      },
    );
    // new Future.delayed(new Duration(seconds: 3), () {
    //   Navigator.pop(context); //pop dialog
    //   //_login();
    // });
  }

  _spinkit() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return SpinKitRotatingCircle(
          color: Colors.white,
          size: 50.0,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final rutaimg = 'http://157.245.82.111/';
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              margin: EdgeInsets.only(right: 30),
              child: Text(
                'Carrito',
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
            ),
            Image.asset('assets/img/4.png'),
          ],
        ),
        backgroundColor: HexColor('#0067A2'),
      ),
      body: Container(
        padding: EdgeInsets.all(5),
        child: FutureBuilder(
          future: vitrinaprovider.getCarritoComprar(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            carritomodal = snapshot.data;
            if (snapshot.hasData) {
              return StaggeredGridView.countBuilder(
                staggeredTileBuilder: (int index) => StaggeredTile.fit(2),
                itemCount: snapshot.data["length"],
                crossAxisCount: 2,
                mainAxisSpacing: 5,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40.0),
                      ),
                      elevation: 5,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(10),
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(15),
                                    child: FadeInImage(
                                      width: 80,
                                      height: 80,
                                      image: NetworkImage("$rutaimg" +
                                          snapshot.data["cart"][index]
                                              ["photo"]),
                                      placeholder:
                                          AssetImage('assets/img/loading.gif'),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 15),
                                  width: 150,
                                  child: Column(
                                    children: [
                                      Container(
                                        child: Center(
                                          child: Text(
                                              snapshot.data["cart"][index]
                                                  ["name"],
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 15),
                                        child: Center(
                                          child: Text(
                                              '\$' +
                                                  " " +
                                                  snapshot.data["cart"][index]
                                                      ["price_unidad"],
                                              style: TextStyle(
                                                  color: Colors.blue,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      IconButton(
                                        icon: Icon(
                                          Icons.remove,
                                          color: Colors.red,
                                        ),
                                        onPressed: () async {
                                          CircularProgressIndicator();
                                          if (snapshot.data["cart"][index]
                                                  ["quantity"] ==
                                              1) {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(
                                              const SnackBar(
                                                content: Text(
                                                    'No es Posible Seguir Restando'),
                                                backgroundColor: Colors.red,
                                              ),
                                            );
                                          } else {
                                            var id = snapshot.data["cart"]
                                                [index]["id"];
                                            _onLoading();
                                            await vitrinaprovider
                                                .restarComprar(id);
                                            setState(() {});
                                            Navigator.pop(context);

                                            // Future.delayed(const Duration(milliseconds: 200), () {
                                            //   setState(() {
                                            //   });
                                            // });
                                          }
                                        },
                                      ),
                                      Container(
                                        child: Center(
                                          child: Text((snapshot.data["cart"]
                                                  [index]["quantity"])
                                              .toString()),
                                        ),
                                      ),
                                      IconButton(
                                        icon: Icon(
                                          Icons.add,
                                          color: Colors.green,
                                        ),
                                        onPressed: () async {
                                          _onLoading();
                                          var id = snapshot.data["cart"][index]
                                              ["id"];
                                          await vitrinaprovider
                                              .sumarComprar(id)
                                              .then((value) {
                                            if (value['error'] == 0) {
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(
                                                const SnackBar(
                                                  content: Text(
                                                      'Cantidad Maxima Alcanzada del Producto'),
                                                  backgroundColor: Colors.red,
                                                ),
                                              );
                                            } else {
                                              Future.delayed(
                                                  const Duration(
                                                      milliseconds: 200), () {
                                                setState(() {});
                                              });
                                            }
                                          });
                                          Navigator.pop(context);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 25),
                                  child: Center(
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.delete,
                                        size: 50,
                                        color: Colors.red,
                                      ),
                                      onPressed: () async {
                                        if (snapshot.data["length"] == 1) {
                                          var id = snapshot.data["cart"][index]
                                              ["id"];
                                          vitrinaprovider.eliminarcomprar(id);
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    Vitrina()),
                                          );
                                        } else {
                                          var id = snapshot.data["cart"][index]
                                              ["id"];
                                          _onLoading();
                                          await vitrinaprovider
                                              .eliminarcomprar(id);
                                          setState(() {});
                                          Navigator.pop(context);
                                        }
                                      },
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          RawMaterialButton(
            onPressed: () {
              _showSimpleModalDialog(context);
              /*
              vitrinaprovider.pedidocomprar().then((value) {
                if (value['resultado']==1) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text('Algunos de tu Productos no puede ser Procesado'),
                      backgroundColor: Colors.red,
                    ),
                  );
                } else {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Vitrina()),
                  );
                    ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text('Pedido Procesado'),
                      backgroundColor: Colors.green,
                    ),
                  );
                }
              });*/
            },
            elevation: 5,
            fillColor: Colors.blue,
            child: Text('ver total',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold)),
            padding: EdgeInsets.all(30),
            shape: CircleBorder(),
          ),
        ],
      ),
    );
  }

  _showSimpleModalDialog(context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          //var carritomodal
          var subtotal = 0.0;
          flete = carritomodal['flete'];
          if (flete == 0) {
            flete = carritomodal['flete'];
          } else {
            flete = carritomodal['flete'][0]['precio'];
          }
          // print(carritomodal['flete'][0]['precio']);
          // print(carritomodal['cart'][0]['unidad']);
          carritomodal['cart'].forEach((totales) {
            if (totales['unidad'] == '1') {
              // print(subtotal);
              subtotal = subtotal +
                  ((double.parse(totales['price_unidad'])) *
                      (double.parse(totales['quantity'])));
              //print(subtotal);
            } else {
              //print(subtotal);
              subtotal = subtotal +
                  ((double.parse(totales['price_caja']) /
                          double.parse(totales['detal'])) *
                      (double.parse(totales['quantity'])));
            }
          });
          var iva = subtotal * 0.16;
          var totalfinal = subtotal + iva + flete;
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              constraints: BoxConstraints(maxHeight: 350),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          text: 'subtotal: ' + subtotal.toString() + ' \$',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Colors.black,
                              wordSpacing: 1)),
                    ),
                    RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          text: 'iva: ' + iva.toString() + ' \$',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Colors.black,
                              wordSpacing: 1)),
                    ),
                    RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          text: 'flete: ' + flete.toString() + ' \$',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Colors.black,
                              wordSpacing: 1)),
                    ),
                    RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          text: 'total: ' + totalfinal.toString() + ' \$',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Colors.black,
                              wordSpacing: 1)),
                    ),
                    RawMaterialButton(
                      onPressed: () async {
                        //_showSimpleModalDialog(context);
                        /*Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => BuscarUsuarioVender()),
                      );*/
                        if (subtotal < 10) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text('el subtotal minimo son 10\$'),
                              backgroundColor: Colors.red,
                            ),
                          );
                          Navigator.pop(context);
                        } else {
                          _spinkit();
                          await vitrinaprovider.pedidocomprar(flete).then((value) {
                            if (value['resultado'] == 1) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  content: Text(
                                      'Algunos de tu Productos no puede ser Procesado'),
                                  backgroundColor: Colors.red,
                                ),
                              );
                            } else {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Vitrina()),
                              );
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  content: Text('Pedido Procesado'),
                                  backgroundColor: Colors.green,
                                ),
                              );
                            }
                          });
                        }
                      },
                      elevation: 5,
                      fillColor: Colors.blue,
                      child: Text('Procesar',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.bold)),
                      padding: EdgeInsets.all(30),
                      shape: CircleBorder(),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
