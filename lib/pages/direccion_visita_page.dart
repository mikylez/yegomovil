import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:toast/toast.dart';
import 'package:yegoapp/pages/vender_page.dart';
import 'package:yegoapp/pages/vitrina_page.dart';
//import 'package:yegoapp/pages/vitrina_page.dart';
import 'package:yegoapp/providers/aceptar_visita_provider.dart';
import 'package:yegoapp/providers/entregas_pendientes_provider.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';
import 'package:yegoapp/widget/mydrawer.dart';

import 'buscar_vender_page.dart';
import 'cuenta_vendedor_page.dart';

class DireccionVisita extends StatefulWidget {
  DireccionVisita({Key key}) : super(key: key);

  @override
  _DireccionVisitaState createState() => _DireccionVisitaState();
}

class _DireccionVisitaState extends State<DireccionVisita> {
  final prefs = PreferenciasUsuario();
  //TextEditingController _inputcodigo =new TextEditingController();
  final despachoProvider = EntregasProvider();
  final verdireccionprovider = AceptarVisitaProvider();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  var orden;
  var telefono;
  var nombre;
  var apellido;
  var cedula;
  var estado;
  var municipio;
  var parroquia;
  var direccion;
    void initState() { 
    super.initState();
    //despachoProvider.despachos();
  }


  @override

    Widget build(BuildContext context) {
    final args =ModalRoute.of(context).settings.arguments;
    //ScrollController _direccion = ScrollController (debugLabel: "");
    return Scaffold(
      key: _scaffoldKey,
      drawer: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: HexColor('#0067A2').withOpacity(0.7),
          ),
          child: myDrawer(context),
        ),
      appBar: AppBar(
        backgroundColor: HexColor('#0067A2'),
        title: Text('Entregas'),
        actions: [
          Container(
            margin: EdgeInsets.only(left: 0),
            child: ClipRRect(
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, 'lista de entregas'),
                child: FadeInImage(
                  image: AssetImage('assets/img/4.png'),
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
        leading: IconButton(
          iconSize: 35,
          icon: Icon(Icons.menu),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
      ),
      body:FutureBuilder( 
      future:verdireccionprovider.verDireccionVisita(args).then((value){
        //orden = value['data'][0][0];
        estado = value['informacion2'][3];
        municipio = value['informacion2'][4];
        parroquia = value['informacion2'][5];
        telefono = value['informacion2'][6];
        nombre = value['informacion2'][1];
        apellido = value['informacion2'][2];
        cedula = value['informacion2'][7];
        direccion = value['informacion2'][0];
        print(direccion);
      }),
      builder: (BuildContext context, AsyncSnapshot snapshot){
      return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.all(20.0),
            children:<Widget>[
              //Text("Orden: $orden"),
              Text('Cedula: $cedula'),
              Text('Nombre: $nombre'),
              Text('Apellido: $apellido'),
              Text('Telefono: $telefono'),
              Text('Direccion: $direccion, $municipio, $parroquia, $estado'),
            ],
          ),
          //_crearInputCodigo(),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child:  ElevatedButton(
              child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 20.0),
                child: Text(
                  'Terminar Visita',
                  style: TextStyle(
                    color: HexColor("FFFFFF"),
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
                ),
              ),
              style: ButtonStyle(
                elevation: MaterialStateProperty.all<double>(0.0),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0)))),
              onPressed: () async {
              // Validate devolverá true si el formulario es válido, o false si
              // el formulario no es válido.
              //if (_formKey.currentState.validate()) {
                  //print(args);
                  var resultado = await verdireccionprovider.visitaEnProceso(args);
                  print(resultado['respuesta']); 
                  if(resultado['respuesta'] == false||resultado['respuesta'] == null){
                  Toast.show('error', context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER, backgroundColor: Colors.red);
                  }
                  else{
                    Toast.show('Visita Terminada', context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER, backgroundColor: Colors.green);
                    Navigator.pushReplacementNamed(context, 'registro_de_visita');
                  }
                // Si el formulario es válido, muestre un snackbar. En el mundo real, a menudo
                // desea llamar a un servidor o guardar la información en una base de datos
                //Scaffold
                //.of(context)
                //.showSnackBar(SnackBar(content: Text('Processing Data')));
                //print('validando');
              //}
              }
          
            )
          ),
        ],
      ),
      );
      }
      ),
      /*ListView.builder(
        itemCount: 10,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Text("$index"),
          );
        },
      ), //_crearproductos(),*/


      bottomNavigationBar: _footer(context) 
    );
  }

  _footer(BuildContext context) {
  var _currentPage = 3;
  return Theme(
    data: Theme.of(context).copyWith(
      canvasColor: HexColor('#0067A2'),
      primaryColor: Colors.yellowAccent,
    ),
      child: BottomNavigationBar(
        unselectedItemColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.house, size: 30),
            label: 'Comprar',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search, size: 30),
            label: 'Buscar',
          ),
          /*BottomNavigationBarItem(
            icon: Icon(Icons.category, size: 30), 
            label: 'Categorías'
          ),*/
             BottomNavigationBarItem(
            icon: Icon(Icons.credit_card_rounded, size: 30), 
            label: 'Vender'
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle, size: 30), 
            label: 'Cuenta'
          ),
        ],
        currentIndex: _currentPage,
        onTap: (int index) {
          setState(() {
            _currentPage = index;
            switch (_currentPage) {
              case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Vitrina()),
                  );
                break;
              case 1:
                showSearch(context: context, delegate: DataSearchVender());
                break;
              case 2 :
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Vender()),
                );
                break;
              case 3:
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CuentaVendedor()),
                );
                break;
              default:
            }
          });
        },
      ),
    );
  }
  /*_crearInputCodigo() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: _inputcodigo,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Codigo',
       labelText: 'Codigo',
       suffixIcon: Icon(Icons.lock,size: 30,),
       //icon: Icon(Icons.account_circle,size:40,),
      ),
      //validator: validator.validateName,
    );
  }*/
  /*Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: HexColor('#0067A2'),
        title: Text('VISITAS PENDIENTES'),
        actions: [
          Container(
            margin: EdgeInsets.only(left: 0),
            child: ClipRRect(
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, ''),
                child: FadeInImage(
                  image: AssetImage('assets/img/4.png'),
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }*/
}
