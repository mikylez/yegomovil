import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:yegoapp/pages/vitrina_page.dart';
import 'package:yegoapp/providers/solicitar_visita_provider.dart';

class SolicitarVisita extends StatefulWidget {
  @override
  _SolicitarVisitaState createState() => _SolicitarVisitaState();
}

class _SolicitarVisitaState extends State<SolicitarVisita> {
  var variable;
  final solicitarvisitaprovider = SolicitarVisitaProvider();
  void initState() {
    super.initState();
    solicitarvisitaprovider.validacionVisita().then((value) {
      print(value);
      if (value["visita"] == false && value["visita2"] == false ||
          value["visita"] == false && value["visita2"] == true) {
        setState(() {
          variable = true;
        });
      } else {
        setState(() {
          variable = false;
        });
      }
      print(variable);
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarSolicitarVisita(),
      body: bodySolicitarVisita(variable),
    );
  }

  Widget appBarSolicitarVisita() {
    return AppBar(
      backgroundColor: HexColor("#0067A2"),
      title: Text("SOLICITAR"),
      centerTitle: true,
      leading: IconButton(
        padding: EdgeInsets.only(top: 10, left: 15, bottom: 10),
        iconSize: 15,
        icon: Icon(Icons.arrow_back_ios),
        color: HexColor("#FFFFFF"),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Vitrina()));
        },
      ),
    );
  }

  Widget bodySolicitarVisita(variable) {
    if (variable == true) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          child: Center(
              child: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Center(
                    child: Container(
                      width: 400.0,
                      decoration: BoxDecoration(
                        color: HexColor('FFFFFF'),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent, width: 5),
                      ),
                      height: 150,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(8.0),
                              child: Container(
                                child: Text(
                                  "Solicitar Colaborador Yego",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blue),
                                ),
                              ),
                            ),
                            Center(
                              child: Container(
                                padding: EdgeInsets.only(top: 15),
                                child: TextButton(
                                  onPressed: () {
                                    solicitarvisitaprovider.solicitar();
                                  },
                                  child: Container(
                                    width: 100.0,
                                    decoration: BoxDecoration(
                                        color: HexColor('#0067A2'),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    height: 50,
                                    child: Center(
                                        child: Text(
                                      "Enviar",
                                      style: TextStyle(
                                          fontSize: 20, color: Colors.white),
                                    )),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 50,
              ),
              Container(
                child: SingleChildScrollView(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Center(
                          child: Container(
                            width: 400.0,
                            decoration: BoxDecoration(
                              color: HexColor('FFFFFF'),
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: Colors.blueAccent, width: 5),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )),
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          child: Center(
            child: Column(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Center(
                      child: Container(
                        width: 400.0,
                        decoration: BoxDecoration(
                          color: HexColor('FFFFFF'),
                          borderRadius: BorderRadius.circular(10),
                          border:
                              Border.all(color: Colors.blueAccent, width: 5),
                        ),
                        height: 250,
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(8.0),
                                child: Container(
                                  child: Text(
                                    "USTED YA TIENE UNA VISITA REGISTRADA",
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.blue),
                                  ),
                                ),
                              ),
                              Center(
                                child: Container(
                                  padding: EdgeInsets.only(top: 15),
                                  child: TextButton(
                                    onPressed: null,
                                    child: Container(
                                      width: 100.0,
                                      decoration: BoxDecoration(
                                          color: Colors.black12,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      height: 50,
                                      child: Center(
                                          child: Text(
                                        "Enviar",
                                        style: TextStyle(
                                            fontSize: 20, color: Colors.blue),
                                      )),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 25),
                              Center(
                                child: Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Container(
                                    child: Text(
                                      "YEGO LE RECUERDA QUE SOLAMENTE PUEDE SOLICITAR UNA VISITA, DESPUÉS DE HABER COMPLETADO LA MISMA",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.blue),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Center(
                          child: Container(
                            width: 400.0,
                            decoration: BoxDecoration(
                              color: HexColor('FFFFFF'),
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: Colors.blueAccent, width: 5),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    //return Container();
  }
}
