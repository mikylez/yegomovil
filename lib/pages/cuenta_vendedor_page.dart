import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';
import 'package:show_up_animation/show_up_animation.dart';
import 'package:yegoapp/models/validacion_formularios.dart';
import 'package:yegoapp/pages/ofertas_page.dart';
import 'package:yegoapp/pages/vender_page.dart';
import 'package:yegoapp/pages/vitrina_page.dart';
//import 'package:yegoapp/pages/vitrina_vendedor_page.dart';
import 'package:yegoapp/providers/vitrina_provider.dart';
import 'buscar_vender_page.dart';
import 'cart_vender_page.dart';
//import 'categorias_vender_page.dart';

class CuentaVendedor extends StatefulWidget {
  @override
  _CuentaVendedorState createState() => _CuentaVendedorState();
}

class _CuentaVendedorState extends State<CuentaVendedor> {
  final vitrinaprovider =VitrinaProvider();
  final validator = ValidacionFormulario();
  final _formulario =GlobalKey<FormState>();
  TextEditingController _inputnombre =new TextEditingController();
  TextEditingController _inputapellido =new TextEditingController();
  TextEditingController _inputdocumento =new TextEditingController();
  TextEditingController _inputcorreo =new TextEditingController();
  TextEditingController _inputtelefono =new TextEditingController();
  TextEditingController _inputfecha =new TextEditingController();
  TextEditingController _inputdireccionfiscal =new TextEditingController();
  TextEditingController _inputdirecciondespacho =new TextEditingController();
  int _radioValue = 2;
  int _radioValueContribuyente = 2;
  String _opcionSelecionadaNacionalidad ='V';
  List<String> _nacionalidad = ['V','E','J'];

  var _estadoFiscal;
  var _municipiosFiscal;
  var _parroquiasFiscal;
  var _estadoDespacho ;
  var _municipiosDespacho;
  var _parroquiasDespacho;
  var _info;
  var datosDireccion;
  var lengthh;

  void _inputsexo(int value) {
    setState(() {
      _radioValue = value;
    });
  }
  // void _inputcontribuyente(int value) {
  //   setState(() {
  //     _radioValueContribuyente = value;
  //   });
  // }

  _loadEstadosMUnicipiosParroquia()async{
    var datosEstadosMunicipiosParroquia;
    datosEstadosMunicipiosParroquia =await vitrinaprovider.direccion();
    setState(() {
      datosDireccion =datosEstadosMunicipiosParroquia;
    });
  }

  void initState() {
    super.initState();
    _loadEstadosMUnicipiosParroquia();
      vitrinaprovider.getCarritoVitrina().then((value){
      setState(() {
        lengthh =value;
      });
    });
    vitrinaprovider.info().then((value){
      if (value!=null) {
        setState(() {
          _info =value;
          _inputnombre.text= _info["info"]["nombre_usuario"];
          _inputapellido.text =_info["info"]["apellido_usuario"];
          _inputdocumento.text =_info["info"]["documento"];
          _inputcorreo.text = _info["correo"];
          _inputtelefono.text=_info["info"]["telefono"];
          _inputfecha.text=_info["info"]["fecha_nacimiento"];
          _inputdireccionfiscal.text =_info["direccion_fiscal"][0]["direccion_usuario"];
          _inputdirecciondespacho.text =_info["direccion_despacho"][0]["direccion_usuario"];
          _opcionSelecionadaNacionalidad = _info["info"]["nacionalidad"];
          _estadoFiscal =_info["estado_fiscal"];
          _municipiosFiscal =_info["municipio_fiscal"];
          _parroquiasFiscal = _info["parroquia_fiscal"];
          _estadoDespacho =_info["estado_despacho"];
          _municipiosDespacho = _info["municipio_despacho"];
          _parroquiasDespacho =_info["parroquia_despacho"];
          if (_info["info"]["sexo"] == "M") {
            _radioValue = 0;
          }else{
            _radioValue = 1;
          }
          // if (_info["info"]["contribuyente"] == 0) {
          //   _radioValueContribuyente = 0;
          // }else{
          //   _radioValueContribuyente = 1;
          // }  
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset('assets/img/4.png'),
              Container(
                padding: EdgeInsets.only(right: 40),
                child: Text(
                  'Cuenta',
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
              ),
            Container(
              margin: EdgeInsets.only(right: 15),
              child: GestureDetector(
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: <Widget>[
                    Icon(
                      Icons.shopping_cart,
                      size: 36.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: CircleAvatar(
                        radius: 8,
                        backgroundColor: Colors.red,
                        foregroundColor: Colors.white,
                        child: Text(
                          lengthh.toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                onTap: (){
                    if (lengthh==0) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('Carrito Sin Productos'),
                        backgroundColor: Colors.red,
                      ),
                    );
                  }else{
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => CartVender()),
                    );
                  }
                },
              ),
            )
            ],
          ),
          backgroundColor: HexColor('#0067A2'),
        ),
        body: SingleChildScrollView(
          child: Form(
            key: _formulario,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  ShowUpAnimation(
                    //delayStart: Duration(seconds:1 ),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: _crearImputNombre(),
                  ),
                  SizedBox( height: 15),
                    ShowUpAnimation(
                    //delayStart: Duration(seconds: 2),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child: _crearImputApellido(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 2),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: _crearImputDocumento(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 3),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:  _crearImputCorreo(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 4),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:  _crearImputTefono(),
                  ),
                  SizedBox( height: 15),
                   ShowUpAnimation(
                    //delayStart: Duration(seconds: 5),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:  _crearImputFecha(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:_estados(),
                  ),
                  SizedBox( height: 15),
                    ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:_municipio(),
                  ),
                    SizedBox( height: 15),
                    ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:_parroquia(),
                  ),
                   SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:  _crearImputDireccionfiscal(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:_estadosDespacho(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:_municipioDespacho(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 6),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child:_parroquiaDespacho(),
                  ),
                  SizedBox( height: 15),
                    ShowUpAnimation(
                    //delayStart: Duration(seconds: 7),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: -0.5,
                    child:  _crearImputDirecciondespacho(),
                  ),
                  SizedBox( height: 15),
                  ShowUpAnimation(
                    //delayStart: Duration(seconds: 8),
                    animationDuration: Duration(seconds: 1),
                    curve: Curves.bounceInOut,
                    direction: Direction.horizontal,
                    offset: 0.5,
                    child: Row(
                      children: [
                        Icon(Icons.person,size: 40,color: Colors.grey,),
                        Radio(
                          value: 0,
                          groupValue: _radioValue,
                          activeColor: Colors.blue,
                          onChanged: (value){
                           _inputsexo(value);
                          },
                        ),
                        Text('Hombre'),
                        Radio(
                          value: 1,
                          groupValue: _radioValue,
                          activeColor: Colors.blue,
                          onChanged: (value){
                            _inputsexo(value);
                          },
                        ),
                        Text('Mujer'),
                      ],
                    ),
                  ),
                  // ShowUpAnimation(
                  //   //delayStart: Duration(seconds: 8),
                  //   animationDuration: Duration(seconds: 1),
                  //   curve: Curves.bounceInOut,
                  //   direction: Direction.horizontal,
                  //   offset: 0.5,
                  //   child: Row(
                  //     children: [
                  //       Icon(Icons.person,size: 40,color: Colors.grey,),
                  //       Text('Contribuyente Especial'),
                  //       Radio(
                  //         value: 1,
                  //         groupValue: _radioValueContribuyente,
                  //         activeColor: Colors.blue,
                  //         onChanged: (value){
                  //          _inputcontribuyente(value);
                  //         },
                  //       ),
                  //       Text('Si'),
                  //       Radio(
                  //         value: 0,
                  //         groupValue: _radioValueContribuyente,
                  //         activeColor: Colors.blue,
                  //         onChanged: (value){
                  //           _inputcontribuyente(value);
                  //         },
                  //       ),
                  //       Text('No'),
                  //     ],
                  //   ),
                  // ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ShowUpAnimation(
                        animationDuration: Duration(seconds:2),
                        curve: Curves.bounceInOut,
                        direction: Direction.horizontal,
                        offset: -0.5,
                        child: TextButton(
                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blue)),
                          child: Text('Guardar',style: TextStyle(color: Colors.white)),
                          onPressed: (){
                            if (_formulario.currentState.validate()){
                              if (_radioValue==2) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text('Selecccionar el Genero'),
                                    backgroundColor: Colors.red,
                                  ),
                                );
                              }
                              // else if(_radioValueContribuyente==2){
                              //   ScaffoldMessenger.of(context).showSnackBar(
                              //     const SnackBar(
                              //       content: Text('Selecccionar Si es Contribuyente Especial'),
                              //       backgroundColor: Colors.red,
                              //     ),
                              //   );
                              // }
                              else{
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text('Enviado'),
                                    backgroundColor: Colors.green,
                                  ),
                                );
                                vitrinaprovider.cuenta(_inputnombre.text,_inputapellido.text,_inputdocumento.text,_inputcorreo.text,_inputtelefono.text,_inputfecha.text,_inputdireccionfiscal.text,_inputdirecciondespacho.text,_radioValue,_opcionSelecionadaNacionalidad,_radioValueContribuyente,_estadoFiscal,_municipiosFiscal,_parroquiasFiscal,_estadoDespacho,_municipiosDespacho,_parroquiasDespacho);
                              }
                            }
                          },
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      bottomNavigationBar: _footer(context)
    );
  }

   _footer(BuildContext context) {
    var _currentPage = 4;
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: HexColor('#0067A2'),
        primaryColor: Colors.yellowAccent,
      ),
      child: BottomNavigationBar(
        unselectedItemColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.house, size: 30),
            label: 'Comprar',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search, size: 30),
            label: 'Buscar',
          ),
          /*BottomNavigationBarItem(
            icon: Icon(Icons.category, size: 30), 
            label: 'Categorías'
          ),*/
             BottomNavigationBarItem(
            icon: Icon(Icons.credit_card_rounded, size: 30), 
            label: 'Vender'
          ),
            BottomNavigationBarItem(
            icon: Icon(Icons.card_giftcard, size: 30), 
            label: 'Ofertas'
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle, size: 30), 
            label: 'Cuenta'
          ),
        ],
        currentIndex: _currentPage,
        onTap: (int index) {
          setState(() {
            _currentPage = index;
            switch (_currentPage) {
              case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Vitrina()),
                  );
                break;
              case 1:
                showSearch(context: context, delegate: DataSearchVender());
                break;
              case 2 :
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Vender()),
                );
                break;
                case 3:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Ofertas()),
                  );
                break;
              case 4:
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CuentaVendedor()),
                );
                break;
              default:
            }
          });
        },
      ),
    );
  }
  _crearImputNombre() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: _inputnombre,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Nombre',
       labelText: 'Nombre',
       suffixIcon: Icon(Icons.account_circle,size: 30,),
       icon: Icon(Icons.account_circle,size:40,),
      ),
      validator: validator.validateName,
    );
  }
  _crearImputApellido() {
    return TextFormField(
       autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: _inputapellido,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Apellido',
       labelText: 'Apellido',
       suffixIcon: Icon(Icons.account_circle,size: 30,),
       icon: Icon(Icons.account_circle,size:40,),
      ),
         validator: validator.validateApellido,
    );
  }
  _crearImputCorreo() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.emailAddress,
      controller: _inputcorreo,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Correo',
       labelText: 'Correo',
       suffixIcon: Icon(Icons.email,size: 30,),
       icon: Icon(Icons.email,size:40,),
      ),
        validator: validator.validateEmail,
    );
  }

  _crearImputTefono() {
      return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.number,
      controller: _inputtelefono,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Telefono',
       labelText: ' Telefono',
       suffixIcon: Icon(Icons.phone,size: 30,),
       icon: Icon(Icons.phone,size:40,),
      ),
        inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
          validator: (value){
        if (value.isEmpty) {
          return 'El Telefono es Obligatorio';
        }
        return null;
      },
    );
  }
    List<DropdownMenuItem<String>> getOpcionesDropdown(){
    List<DropdownMenuItem<String>> _lista= [];
    _nacionalidad.forEach((nacionalida) {
      _lista.add(DropdownMenuItem(
        child: Text(nacionalida),
        value: nacionalida,
      ));
    });
    return _lista;
  }

  _crearImputDocumento() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      autofocus: false,
      keyboardType: TextInputType.number,
      controller: _inputdocumento,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Documento',
       labelText: ' Documento',
       prefixIcon:Container(
         padding: EdgeInsets.only(left: 15),
         child: DropdownButton(
          underline: Container(
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide.none)
            )
          ),
          value: _opcionSelecionadaNacionalidad,
          items: getOpcionesDropdown(),
          onChanged: (opt){
            setState(() {
              _opcionSelecionadaNacionalidad =opt;
            });
          },
        ),
       ),
       icon:Icon(Icons.account_box,size: 40,) ,
       suffixIcon: Icon(Icons.account_box,size: 30,),
      ),
      inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
          validator: (value){
        if (value.isEmpty) {
          return 'El Documento es Obligatorio';
        }
        return null;
      },
    );
  }

  _crearImputFecha() {
    return TextFormField(
    autofocus: false,
    controller: _inputfecha,
    decoration: InputDecoration(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      hintText: 'Fecha de Nacimiento',
      labelText: 'Fecha de Nacimiento',
      suffixIcon: Icon(Icons.calendar_today,size: 30),
      icon: Icon(Icons.calendar_today,size:40),
    ),
     onTap: (){
       FocusScope.of(context).requestFocus(new FocusNode());
       _selectDate(context);
     },
        validator: (value){
        if (value.isEmpty) {
          return 'La Fecha es Obligatoria';
        }
        return null;
      },
    );
  }

  _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(1920),
      lastDate:  new DateTime(2025),
    );
    if (picked!=null) {
      setState(() {
        String fecha = DateFormat('dd-MM-yyyy').format(picked);
        _inputfecha.text = fecha;
      });
    }
  }

  _estados(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_estadoFiscal,
          decoration: InputDecoration(
            labelText: 'Estado Direccion Fiscal',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[0]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
              value:map,
            );
          })?.toList() ?? [],
          onChanged: (value){
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _estadoFiscal=value;
            });
          },
        ),
      );
    }else{
      return CircularProgressIndicator();
    }     
  }

  _municipio(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_municipiosFiscal,
          decoration: InputDecoration(
            labelText: 'Municipio Direccion Fiscal',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[1]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
              value: map,
            );
          })?.toList() ?? [],
              onChanged: (value){
                FocusScope.of(context).requestFocus(new FocusNode());
                setState(() {
                  _municipiosFiscal=value;
                });
              },
        ),
      );
    }else{
      return CircularProgressIndicator();
    }  
  }

  _parroquia(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_parroquiasFiscal,
          decoration: InputDecoration(
            labelText: 'Parroquia',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[2]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
              value: map,
            );
          })?.toList() ?? [],
          onChanged: (value){
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _parroquiasFiscal=value;
            });
          },
        ),
      );
    }else{
      return CircularProgressIndicator();
    }      
  }
  _crearImputDireccionfiscal() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      maxLines: null,
      keyboardType: TextInputType.multiline,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      controller: _inputdireccionfiscal,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Direccion Fiscal',
       labelText: 'Direccion Fiscal',
       icon: Icon(Icons.location_on,size:40,),
       suffixIcon: Icon(Icons.location_on,size: 30,),
      ),
         //validator: validator.validateDireccionFiscal,
    );
  }

  _crearImputDirecciondespacho() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      maxLines: null,
      keyboardType: TextInputType.multiline,
      autofocus: false,
      textCapitalization: TextCapitalization.sentences,
      controller: _inputdirecciondespacho,
      decoration: InputDecoration(
       border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
       ),
       hintText: 'Direccion de Despacho',
       labelText: 'Direccion de Despacho',
       icon: Icon(Icons.location_on,size:40,),
       suffixIcon: Icon(Icons.location_on,size: 30,),
      ),
      //validator: validator.validateDireccionDespacho,
    );
  }

  _estadosDespacho(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_estadoDespacho,
          decoration: InputDecoration(
            labelText: 'Estado Direccion de Despacho',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[0]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
              value: map,
            );
          })?.toList() ?? [],
          onChanged: (value){
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _estadoDespacho=value;
            });
          },
        ),
          );
     }else{
       return CircularProgressIndicator();
     }
      
  }

  _municipioDespacho(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_municipiosDespacho,
          decoration: InputDecoration(
            labelText: 'Municipio Direccion de Despacho',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[1]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
              value: map,
            );
          })?.toList() ?? [],
          onChanged: (value){
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _municipiosDespacho=value;
            });
          },
        ),
      );
    }else{
      return CircularProgressIndicator();
    }  
  }

  _parroquiaDespacho(){
    if (datosDireccion!=null) {
      return Container( 
        child: DropdownButtonFormField(
          autofocus: false,
          value:_parroquiasDespacho,
          decoration: InputDecoration(
            labelText: 'Parroquia Direccion de Despacho',
            icon:Icon(Icons.location_on,size: 40,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          items:datosDireccion[2]?.map<DropdownMenuItem>((map){
            return DropdownMenuItem(
              child: Text(map),
               value: map,
            );
          })?.toList() ?? [],
          onChanged: ( value){
            FocusScope.of(context).requestFocus(new FocusNode());
            setState(() {
              _parroquiasDespacho=value;
            });
          },
            ),
          );
    }else{
      return CircularProgressIndicator();
    }
  }
}