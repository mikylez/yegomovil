import 'dart:convert';

import 'package:flutter/material.dart';
//import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';
import 'package:loadmore/loadmore.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:yegoapp/carrito_preferences/carrito_preferences.dart';
import 'package:yegoapp/providers/vitrina_provider.dart';
import 'package:yegoapp/widget/mydrawer.dart';
import 'buscar_vender_page.dart';
import 'cart_vender_page.dart';

var lengthh;

class VitrinaDistribuidor extends StatefulWidget {
  
  @override
  _VitrinaDistribuidorState createState() => _VitrinaDistribuidorState();
}

class _VitrinaDistribuidorState extends State<VitrinaDistribuidor> {
  final vitrinaprovider = VitrinaProvider();
  final cart = CarritoPreferences();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int get count => list.length;
  List<dynamic> list = [];
  var page =1;
  final rutaimg = 'http://157.245.82.111/';
  final f = NumberFormat("#,##0.00", "en_US");
  var numero;
  var cantidad;
  var maxPorductos;

  void initState() {
    super.initState();
    product();
    vitrinaprovider.getCarritoVitrina().then((value) {
      setState(() {
        lengthh = value;
      });
    });
    //list.addAll(List.generate(30, (v) => v));
  }

  product() {
    vitrinaprovider.productos(page).then((value)  => {    
      setState(() {
        list = value['Inventories'];
        page +=1;
        maxPorductos = value['maxPorductos'].length;
      })
    });
  }
  // void load() {
  //   vitrinaprovider.productos(page).then((value) => {
  //     setState(() {
  //       page +=1;
  //       list.addAll(value['Inventories']);
  //       //list.addAll(List.generate(10, (v) => v)); 
  //     })    
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        drawer: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: HexColor('#0067A2').withOpacity(0.7),
          ),
          child: myDrawer(context),
        ),
        appBar: AppBar(
          backgroundColor: HexColor('#0067A2'),
          actions: [
            Container(
              margin: EdgeInsets.only(right: 80),
              child: ClipRRect(
                child: GestureDetector(
                  onTap: () => Navigator.pushNamed(context, 'vitrina_vendedor'),
                  child: FadeInImage(
                    image: AssetImage('assets/img/4.png'),
                    placeholder: AssetImage('assets/img/no-image.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10,right: 15),
              child: GestureDetector(
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: <Widget>[
                    Icon(
                      Icons.shopping_cart,
                      size: 36.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: CircleAvatar(
                        radius: 8,
                        backgroundColor: Colors.red,
                        foregroundColor: Colors.white,
                        child: Text(((){
                          if (cart.carrito== 0) {
                            return '0';
                          }else{
                            return jsonDecode(cart.carrito).length.toString();
                          }
                        })(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                onTap: (){
                  if (cart.carrito== 0) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('Carrito Sin Productos'),
                        backgroundColor: Colors.red,
                      ),
                    );
                  }else{
                       Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => CartVender()),
                    );
                  }    
                }
              ),
            )
          ],
          leading: IconButton(
            iconSize: 35,
            icon: Icon(Icons.menu),
            onPressed: () => _scaffoldKey.currentState.openDrawer(),
          ),
        ),
           body: Center(
        child: list.length == 0 ? CircularProgressIndicator() : 
        Container(
        child: RefreshIndicator(
          child: LoadMore(
            isFinish: count >= maxPorductos,
            onLoadMore: _loadMore,
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                  numero =(list[index]["price_unidad"]);
                  var i = double.parse(numero);
                  numero = f.format(i);
                  cantidad = (list[index]["cantidad"]).toString();
                  if(list[index]["cantidad"]>0 && list[index]["estatus"]==1){
                  return Container(
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40.0),
                      ),
                      elevation: 5,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.all(5),
                              child: Center(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: Column(
                                    children: [
                                      GestureDetector(
                                        child: FadeInImage.memoryNetwork(
                                          placeholder: kTransparentImage,
                                          height: 180,
                                          width: 180,
                                          image: ("$rutaimg" +list[index]["photo"]),
                                        ),
                                      // // //   /*CachedNetworkImage(
                                      // // //     imageUrl:("$rutaimg" +
                                      // // //               snapshot.data["Inventories"]
                                      // // //               [index]["photo"]),//"http://via.placeholder.com/350x150",
                                      // // //     //placeholder: (context, url) => CircularProgressIndicator(),
                                      // // //     progressIndicatorBuilder: (context, url, downloadProgress) => 
                                      // // //     CircularProgressIndicator(value: downloadProgress.progress),
                                      // // //     errorWidget: (context, url, error) => Icon(Icons.error)
                                      // // //     //errorWidget: (context, url, error) => Icon(Icons.error),
                                      // // //   ),*/
                                      // // //   /*FadeInImage(
                                      // // //     height: 180,
                                      // // //     width: 180,
                                      // // //     image: NetworkImage("$rutaimg" +
                                      // // //         snapshot.data["Inventories"]
                                      // // //             [index]["photo"]),
                                      // // //     placeholder: AssetImage(
                                      // // //         'assets/img/loading.gif'),
                                      // // //     fit: BoxFit.fill,
                                      // // //   ),*/
                                        onTap: () => Navigator.pushNamed(
                                          context, 'detalle_vender',
                                          arguments: list[index]
                                        ),
                                      ),
                                      Container(
                                        child: ListTile(
                                          title: Text(
                                            list[index]
                                                ["name"],
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          subtitle: Text(numero + " " + "\$--"+cantidad+" Unidades Disponibles",
                                              style: TextStyle(
                                                  color: Colors.blue,
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ))
                        ],
                      ),
                    ),
                  );}
                  else{
                    return Container();
                  }
                  /* return Container(
                    padding: EdgeInsets.all(5),
                    height: 300,
                    child: Center(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(40),
                        child: Column(
                          children: [
                            GestureDetector(
                              child: FadeInImage(
                                height: 180,
                                image: NetworkImage("$rutaimg"+snapshot.data["Inventories"][index]["photo"]),
                                placeholder: AssetImage('assets/img/loading.gif'),
                                fit: BoxFit.cover,
                              ),
                              onTap: ()=>Navigator.pushNamed(context, 'detalle_producto',arguments: snapshot.data["Inventories"][index]),
                            ),
                              Container(
                                child: ListTile(
                                  title: Text(snapshot.data["Inventories"][index]["name"],style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.bold),
                                    //overflow: TextOverflow.ellipsis,
                                  ),
                                  subtitle: Text(snapshot.data["Inventories"][index]["price"],style: TextStyle(color: Colors.blue, fontSize: 18)),
                                ),
                              ),
                            Expanded(
                              child: Container(
                                width: double.infinity,
                                child: ElevatedButton(
                                  child: Text(
                                      'Agregar',
                                    style: TextStyle(fontSize: 18),
                                  ),
                                  onPressed: () {},
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  );*/
              },
              itemCount: count,
            ),
            whenEmptyLoad: false,
            delegate: DefaultLoadMoreDelegate(),
            textBuilder: DefaultLoadMoreTextBuilder.english,
          ),
          onRefresh: _refresh,
        ),
      ),
        
      ),
      bottomNavigationBar: _footer(context)
    );
  }
  Future<bool> _loadMore() async {
    //await Future.delayed(Duration(seconds: 0, milliseconds: 5000));
    //load();
    await vitrinaprovider.productos(page).then((value) => {
      setState(() {
        page +=1;
        list.addAll(value['Inventories']);
        //list.addAll(List.generate(10, (v) => v)); 
      })    
    });
    return true;
  }

  Future<void> _refresh() async {
    //await Future.delayed(Duration(seconds: 0, milliseconds: 5000));
    list.clear();
    page =1;
    //load();
    await vitrinaprovider.productos(page).then((value) => {
      setState(() {
        page +=1;
        list.addAll(value['Inventories']);
        //list.addAll(List.generate(10, (v) => v)); 
      })    
    });
  }
  
  _footer(BuildContext context) {
    var _currentPage = 0;
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: HexColor('#0067A2'),
      ),
      child: BottomNavigationBar(
        unselectedItemColor: Colors.white,
        selectedItemColor: Colors.yellowAccent,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.house, size: 30),
            label: 'Vender',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search, size: 30),
            label: 'Buscar',
          ),
          /*BottomNavigationBarItem(
            icon: Icon(Icons.category, size: 30), 
            label: 'Categorías'
          ),*/
           /* BottomNavigationBarItem(
            icon: Icon(Icons.credit_card_rounded, size: 30), 
            label: 'Vender'
          ),*/
          /*BottomNavigationBarItem(
            icon: Icon(Icons.account_circle, size: 30), 
            label: 'Cuenta'
          ),*/
        ],
        currentIndex: _currentPage,
        onTap: (int index) {
          setState(() {
            _currentPage = index;
            switch (_currentPage) {
              case 0:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => VitrinaDistribuidor()),
                  );
                break;
              case 1:
                showSearch(context: context, delegate: DataSearchVender());
                break;
              /*case 2:
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CategoriasVender()),
                );
                break;*/
              /*case 2 :
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => VitrinaDistribuidor()),
                );
                break;*/
              /*case 4:
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CuentaVendedor()),
                );
                break;*/
              default:
            }
          });
        },
      ),
    );
  }

  // _crearproductos() {
  //   final rutaimg = 'http://157.245.82.111/';
  //   final f = NumberFormat("#,##0.00", "en_US");
  //   var numero ;
  //   return Container(
  //     child: FutureBuilder(
  //       //future: vitrinaprovider.productos(),
  //       builder:(BuildContext context, AsyncSnapshot snapshot ){
  //         if (snapshot.hasData) {
  //           return Container(
  //             margin: EdgeInsets.symmetric(horizontal: 10),
  //             child: StaggeredGridView.countBuilder(
  //               staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
  //               itemCount: snapshot.data["Inventories"].length,
  //               crossAxisCount: 2,
  //               mainAxisSpacing: 4,
  //               crossAxisSpacing: 2,
  //               itemBuilder: (BuildContext context, int index){ 
  //                 numero =(snapshot.data["Inventories"][index]["price_unidad_credito"]);
  //                 var i = double.parse(numero);
  //                 numero = f.format(i);
  //                 return Container(
  //                   child: Column(
  //                     mainAxisSize: MainAxisSize.min,
  //                     children: [
  //                       Card(
  //                         shape: RoundedRectangleBorder(
  //                           borderRadius: BorderRadius.circular(40.0),
  //                         ),
  //                         elevation: 10,
  //                         child: Column(
  //                           mainAxisSize: MainAxisSize.min,
  //                           children: <Widget>[
  //                             Container(
  //                               padding: EdgeInsets.all(5),
  //                               child: Center(
  //                                 child: ClipRRect(
  //                                   borderRadius: BorderRadius.circular(40),
  //                                   child: Column(
  //                                     children: [
  //                                       GestureDetector(
  //                                         child: FadeInImage(
  //                                           height: 180,
  //                                           width: 180,
  //                                           image: NetworkImage("$rutaimg"+snapshot.data["Inventories"][index]["photo"]),
  //                                           placeholder: AssetImage('assets/img/loading.gif'),
  //                                           fit: BoxFit.fill,
  //                                         ),
  //                                         onTap: ()=>Navigator.pushNamed(context,'detalle_vender',arguments: snapshot.data["Inventories"][index]),
  //                                       ),
  //                                       Container(
  //                                         child: ListTile(
  //                                           title: Text(snapshot.data["Inventories"][index]["name"],style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),
  //                                             overflow: TextOverflow.ellipsis,
  //                                           ),
  //                                           subtitle: Text(numero+" "+"\$",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold)),
  //                                         ),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                 ),
  //                               )
  //                             )
  //                           ],
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 );
  //               },
  //             ),
  //           );
  //         }
  //         return Center(
  //           child: CircularProgressIndicator(),
  //         );
  //       },
  //     ),
  //   );
  // }

  Drawer drawer() {
    return Drawer(
      child: ListView(
        children: <Widget>[
          Container(
            child: DrawerHeader(
              child: CircleAvatar(
                child: ClipOval(
                  child: Text('BF',style:TextStyle(fontSize: 50, fontWeight: FontWeight.bold)),
                ),
              ),
            ),
            //color: HexColor('#0067A2'),
          ),
          Container(
            //color: HexColor('#0067A2'),
            child: Column(
              children: List.generate(8, (int index) {
                return ListTile(
                  title: Text('opciones',style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold)),
                  leading: Icon(
                    Icons.info,
                    color: Colors.white,
                  ),
                );
              }),
            ),
          )
        ],
      ),
    );
  }

}