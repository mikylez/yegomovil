import 'package:flutter/material.dart';
import 'package:yegoapp/providers/vitrina_provider.dart';

class DataSearch  extends SearchDelegate{
  final vitrinaprovider = VitrinaProvider();
  @override
  String get searchFieldLabel => 'Buscar...';
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.close),
        onPressed: (){
          query = '';
        },
      )
    ];  
  }
  
    @override
    Widget buildLeading(BuildContext context) {
      return IconButton(
          icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow,
            progress: transitionAnimation,
          ),
          onPressed: (){
            close(context, null);
          },
      );
    }
    @override
    Widget buildResults(BuildContext context) {
      return Container();
    }
    @override
    Widget buildSuggestions(BuildContext context) {
      final rutaimg = 'http://157.245.82.111/';
    if (query.isEmpty) {
      return Container();
    }
    return FutureBuilder(
      future: vitrinaprovider.buscar(query),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          final productos = snapshot.data;
            return ListView(
              children: productos.map<Widget>((productos){
                return ListTile(
                  leading: FadeInImage(
                    image: NetworkImage("$rutaimg"+productos["photo"]),
                    placeholder: AssetImage('assets/img/loading.gif'),
                    width: 50,
                    fit: BoxFit.contain,
                  ),
                  title: Text(productos["name"]),
                  subtitle: Text(productos["name"]),
                  onTap: (){
                    close(context, null);
                    Navigator.pushNamed(context, 'detalle_producto',arguments:productos);
                  },
                );
              }).toList()
            );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}