import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:yegoapp/bloc/provider.dart';
import 'package:yegoapp/pages/cambio_a_vendedor_page.dart';
import 'package:yegoapp/providers/user_provider.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';

Widget myDrawer(BuildContext context) {
  final usuarioProvider = new UsuarioProvider();
  final _pref = new PreferenciasUsuario();
  bool usuario;
  bool vendedor;

  if (_pref.idTipo == 2) {
    vendedor = true;
  } else {
    vendedor = false;
  }
  if (_pref.idTipo == 1) {
    usuario = true;
  } else {
    usuario = false;
  }

  //final authService = Provider.of<AuthService>(context, listen: false);
  return Drawer(
    // column holds all the widgets in the drawer
    child: Column(
      children: <Widget>[
        Expanded(
          // ListView contains a group of widgets that scroll inside the drawer
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                child: CircleAvatar(
                  child: Icon(
                    Icons.add_a_photo,
                    color: Colors.white,
                    size: 55.0,
                    /*Text('YEGO',
                        style:
                            TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),*/
                  ),
                ),
              ),
              Visibility(
                visible: usuario,
                child: Column(
                  children: [
                    TextButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CambioAVendedor()),
                        );
                      },
                      child: Center(
                        child: Text(
                          "¿Ser Vendedor Yego?",
                          style: TextStyle(fontSize: 15, color: Colors.white),
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        /*Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CambioAVendedor()),
                  );*/
                        Navigator.pushNamed(context, 'solicitar_visita');
                      },
                      child: Center(
                        child: Text(
                          "Solicitar Visita",
                          style: TextStyle(fontSize: 15, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                  visible: vendedor,
                  child: Column(children: [
                    TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, 'aceptar_visita');
                      },
                      child: Center(
                        child: Text(
                          "Solicitudes de Visita",
                          style: TextStyle(fontSize: 15, color: Colors.white),
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, 'registro_de_visita');
                      },
                      child: Center(
                        child: Text(
                          "Visitas Pendientes",
                          style: TextStyle(fontSize: 15, color: Colors.white),
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, 'registrar_clientes');
                      },
                      child: Center(
                        child: Text(
                          "Registrar Clientes",
                          style: TextStyle(fontSize: 15, color: Colors.white),
                        ),
                      ),
                    )
                  ])),
            ],
          ),
        ),
        // This container holds the align
        Container(
            // This align moves the children to the bottom
            child: Align(
                alignment: FractionalOffset.bottomCenter,
                // This container holds all the children that will be aligned
                // on the bottom and should not scroll with the above ListView
                child: Container(
                    child: Column(
                  children: <Widget>[
                    Divider(),
                    TextButton.icon(
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        //minimumSize: ,
                        //backgroundColor: Colors.teal,
                        //onSurface: Colors.grey,
                      ),
                      icon: Icon(Icons.exit_to_app),
                      label: Text("Salir",
                          style: TextStyle(fontSize: 15, color: Colors.white)),
                      onPressed: () {
                        usuarioProvider.logout();
                        /*Navigator.of(context)
                        .pushNamedAndRemoveUntil('login', (Route<dynamic> route) => false);*/
                        Navigator.pushReplacementNamed(context, 'login');
                      },
                    ),
                  ],
                ))))
      ],
    ),
  );

  /*Drawer(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            child: DrawerHeader(
              child: CircleAvatar(
                child: Icon(
                  Icons.add_a_photo,
                  color: Colors.white,
                  size: 55.0,
                  /*Text('YEGO',
                      style:
                          TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),*/
                ),
              ),
            ),
            //color: HexColor('#0067A2'),
          ),
          Container(
            //color: HexColor('#0067A2'),
            child: Column(
              children: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CambioAVendedor()),
                  );
                },
                child: Center(
                  child: 
                  Text(
                    "¿QUIERES SER YEGO VENDEDOR?",
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
              ),
            ]),
          ),
          Expanded(
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child:
                TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CambioAVendedor()),
                  );
                },
                child: Center(
                  child: 
                  Text(
                    "salir",
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
              ),
              ),
          )
        ],
      ),
    );*/
}
