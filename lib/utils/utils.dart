import 'package:flutter/material.dart';


void mostarAlerta(BuildContext context, String mensaje){

showDialog(
  context: context,
  builder: (context) {
    return AlertDialog(
      title: Text('Alerta'),
      content: Text(mensaje),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.of(context).pop(), 
          child: Text('ok'))
      ],

    );
  }
  );

}