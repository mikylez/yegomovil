import 'package:shared_preferences/shared_preferences.dart';

class CarritoPreferences {

  static final CarritoPreferences _carrito = new CarritoPreferences._internal();

  factory CarritoPreferences() {
    return _carrito;
  }
  
  CarritoPreferences._internal();

  SharedPreferences _cart ;

  initPrefs() async {
    this._cart = await SharedPreferences.getInstance();
    //await _cart.clear();
  }

  removetPrefs() async {
    this._cart = await SharedPreferences.getInstance();
    await _cart.remove('cart');
  }

  eliminarPrefs() async {
    this._cart = await SharedPreferences.getInstance();
    await _cart.remove('cart');
   
  }

  get carrito{
    return _cart.getString('cart') ?? 0;
  }

  set carrito( value ) {
    
   _cart.setString('cart', value);
  }

}