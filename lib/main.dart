import 'package:flutter/material.dart';
import 'package:yegoapp/bloc/provider.dart';
import 'package:yegoapp/carrito_preferences/carrito_preferences.dart';
import 'package:yegoapp/routes/routes.dart';
import 'package:yegoapp/user_preferences/preferencias_usuario.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();
  final cart = CarritoPreferences();
  await prefs.initPrefs();
  await cart.initPrefs();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prefs = new PreferenciasUsuario();
    print(prefs.token);
    print(prefs.id);
    print(prefs.idTipo);

    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'yegomovil',
        initialRoute: 'login',
        routes: routes(),
        theme: ThemeData(
          primaryColor: Colors.blue,
        ),
      ),
    );
  }
}
